import 'dart:convert';

import 'package:capto/models/VariationPodo.dart';
import 'package:capto/models/combopojo.dart';
import 'package:capto/models/preModelPojo.dart';
import 'package:capto/models/taxPojo.dart';

class DishOrderPojo {
  String? dishid,
      dishname,
      qty,tempQty,
      priceperdish,
      price,
      orderdetailid,
      status,
      prefid,
      prenm,
      discount,
      dis_per;
  String? cusineid, dishtpe, description, dishimage, hsn_no, preflag;
  String? combo_flag,
      dish_comment,
      sold_by,
      purchased_unit_id,
      purchased_unit_name,
      used_unit_id,
      used_unit_name,
      default_sale_weight;
  String? unitid,
      unitname,
      weight,
      sort_nm,
      price_without_tax,
      priceper_without_tax,
      tax_amt,
      tot_tax;
  String? offer,
      tot_disc,
      tm,
      allow_open_price,
      package_flag,
      pack_id,
      packname,
      max_qty;
  String? item_type, service_duration, buffer_duration, tsysid, item_cmt_id;
  bool? isnew = true;
  List<TaxData>? tax_data = [],tempTax=[];
  List<Pre>? pre = [];
  List<ComboItem>? combo_list = [], package_item = [];
  List<VariationPodo>? varPojoList = [];
  Map? tempPrice;

  DishOrderPojo({
      this.dishid,
      this.dishname,
      this.qty,
      this.priceperdish,
      this.price,
      this.orderdetailid,
      this.status,
      this.prefid,
      this.prenm,
      this.discount,
      this.dis_per,
      this.cusineid,
      this.dishtpe,
      this.description,
      this.dishimage,
      this.hsn_no,
      this.preflag,
      this.combo_flag,
      this.dish_comment,
      this.sold_by,
      this.purchased_unit_id,
      this.purchased_unit_name,
      this.used_unit_id,
      this.used_unit_name,
      this.default_sale_weight,
      this.unitid,
      this.unitname,
      this.weight,
      this.sort_nm,
      this.price_without_tax,
      this.priceper_without_tax,
      this.tax_amt,
      this.tot_tax,
      this.offer,
      this.tot_disc,
      this.tm,
      this.allow_open_price,
      this.package_flag,
      this.pack_id,
      this.packname,
      this.max_qty,
      this.item_type,
      this.service_duration,
      this.buffer_duration,
      this.tsysid,
      this.item_cmt_id,
      this.isnew=true,
      this.tax_data,this.tempTax,this.tempPrice,
      this.pre,
      this.combo_list,
      this.package_item,this.tempQty,
      this.varPojoList});

  DishOrderPojo.fromJson(Map<String, dynamic> json) {
    dishid=json['dishid'];
    dishname= json["dishname"];
    qty=json["qty"] ;
    if(json["max_qty"]!=null)
    max_qty=json["max_qty"];
    priceperdish=json["priceperdish"]  ;
    price=json["price"] ;
    orderdetailid=json["orderdetailid"] ;
    status=json["status"];
    prefid=json["prefid"] ;
    prenm=json["prenm"] ;
    discount=json["discount"];
    tot_disc=json["tot_discount"] ;
    if(json["offer"]!=null)
      offer=json["offer"];
    dish_comment=json["comment"] ;
    cusineid=json["cusineid"] ;
    dishtpe=json["dishtpe"];
    description=json["description"] ;
    dishimage=json["dishimage"];
    preflag=json["preflag"] ;
    dish_comment=json["dish_comment"] ;

    isnew=json["isnew"] ;

    sold_by=json["sold_by"];
    if(json["weight"]!=null)
      weight=json["weight"];
    if(json["sort_name"]!=null)
      sort_nm=json["sort_name"];
    if(json["unit_nm"]!=null)
      unitname=json["unit_nm"];
    if(json["unit_id"]!=null)
      unitid=json["unit_id"];
    if(json["p_id"]!=null)
      purchased_unit_id=json["p_id"];
    if(json["p_nm"]!=null)
      purchased_unit_name=json["p_nm"] ;
    if(json["u_id"]!=null)
      used_unit_id=json["u_id"];
    if(json["u_nm"]!=null)
      used_unit_name=json["u_nm"] ;

    tot_tax=json["tot_tax"];
    tax_amt=json["tax_amt"];
    priceper_without_tax=json["per_wt"] ;
    price_without_tax=json["p_wt"] ;
    varPojoList = [];
    if (json['pre_array']!=null) {
      json['pre_array'].forEach((v) {
        varPojoList!.add(new VariationPodo.fromJson(v));
      });
    }
    pre=[];
    if ( json['pre']!=null) {
      json['pre'].forEach((v) {
        pre!.add(new Pre.fromJson(v));
      });
    }
    tax_data=[];
    if (tax_data != null && tax_data!.isNotEmpty) {
      json['taxdata'].forEach((v) {
        tax_data!.add(new TaxData.fromLocalJson(v));
      });
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["dishid"] = dishid;
    data["dishname"] = dishname;
    data["qty"] = qty;
    data["max_qty"] = (max_qty!=null)?max_qty:'';
    data["priceperdish"] = priceperdish;
    data["price"] = price;
    data["orderdetailid"] = orderdetailid;
    data["status"] = status;
    data["prefid"] = prefid;
    data["prenm"] = prenm;
    data["discount"] = discount;
    data["tot_discount"] = (tot_disc!=null)?tot_disc:'0';
    data["offer"] = offer;
    data["comment"] = dish_comment;
    data["cusineid"] = cusineid;
    data["dishtpe"] = dishtpe;
    data["description"] = description;
    data["dishimage"] = dishimage;
    data["preflag"] = preflag;
    data["dish_comment"] = dish_comment;

    data["isnew"] = true;

    data["sold_by"] = sold_by;
    data["weight"] = weight;
    data["sort_name"] = sort_nm;
    data["unit_nm"] = unitname;
    data["unit_id"] = unitid;
    data["p_id"] = purchased_unit_id;
    data["p_nm"] = purchased_unit_name;
    data["u_id"] = used_unit_id;
    data["u_nm"] = used_unit_name;

    data["tot_tax"] = tot_tax;
    data["tax_amt"] = tax_amt;
    data["per_wt"] = priceper_without_tax;
    data["p_wt"] = price_without_tax;
    if (varPojoList != null && varPojoList!.isNotEmpty) {
      data['pre_array'] = this.varPojoList!.map((v) => v.toJson()).toList();
    }
    if (pre != null && pre!.isNotEmpty) {
      data['pre'] = this.pre!.map((v) => v.toJson()).toList();
    }
    if (tax_data != null && tax_data!.isNotEmpty) {
      data['taxdata'] = this.tax_data!.map((v) => v.toLocalJson()).toList();
    }

    return data;
  }
}
