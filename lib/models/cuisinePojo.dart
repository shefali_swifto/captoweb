import 'package:capto/models/dishPojo.dart';

class CuisinePojo {
  String? cuisineId,cuisineName,cuisineImage,itemPricing,taxId,itemCount;
  List<DishPojo>? dishData=[];

  CuisinePojo(
      {this.cuisineId,
        this.cuisineName,
        this.cuisineImage,
        this.itemPricing,
        this.taxId,this.dishData,
        this.itemCount});

  CuisinePojo.fromJson(Map<String, dynamic> json) {
    cuisineId = json['cuisine_id'].toString();
    cuisineName = json['cuisine_name'];
    cuisineImage = json['cuisine_image'];
    itemPricing = json['item_pricing'];
    taxId = json['tax_id'];
    itemCount = (json.containsKey('item_count'))?json['item_count'].toString():'0';
    if (json['dish_data'] != null) {
      dishData =[];
      json['dish_data'].forEach((v) {
        dishData!.add(new DishPojo.fromJson(v));
      });
      itemCount=dishData!.length.toString();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cuisine_id'] = this.cuisineId;
    data['cuisine_name'] = this.cuisineName;
    data['cuisine_image'] = this.cuisineImage;
    data['item_pricing'] = this.itemPricing;
    data['tax_id'] = this.taxId;
    data['item_count'] = this.itemCount;
    if (this.dishData != null) {
      data['dish_data'] =
          this.dishData!.map((v) => v.toJson()).toList();
    }
    return data;
  }

}