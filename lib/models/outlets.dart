import 'dart:convert';

import 'package:capto/util/constant.dart';

Outlets outletsFromJson(String str) => Outlets.fromJson(json.decode(str));

String outletsToJson(Outlets data) => json.encode(data.toJson());

Outlet outletFromJson(String str) => Outlet.fromJson(json.decode(str));

String outletToJson(Outlet data) => json.encode(data.toJson());

class Outlets {
  int? success;
  String? message;
  List<Outlet>? outlets;

  Outlets({
    this.success,
    this.message,
    this.outlets,
  });

  factory Outlets.fromJson(Map<String, dynamic> json) => Outlets(
        success: json["success"],
        message: json["message"],
        outlets:
            List<Outlet>.from(json["outlets"].map((x) => Outlet.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "outlets": List<dynamic>.from(outlets!.map((x) => x.toJson())),
      };
}

class Outlet {
  int? success, id;
  String? message, name, description;
  String? address;
  String? phno, countryCode, countryName;
  String? logo;
  String? logoUrl;
  String? email;
  String? restUniqueId, brand_id;
  String? outletOpeningTiming;
  String? outletClosingTiming;
  String? longitude;
  String? latitude;
  String? outletCurrentStatus;
  String? distance;
  String? decimalValue;
  String? isRoundingOn;
  String? roundingId;
  String? showItemPriceWithoutTax;
  String? intervalNumber;
  String? roundingRule;
  int? roundingTag, adv_order_duration;
  String? deliveryCharges, advance_order;
  String? delivery_available, capto_delivery_status, currency;
  double? minimum_total_bill = 0.0,
      minimum_bill_for_delivery_charge = 0.0,
      packaging_charge = 0.0;
  String? note, pack_charge_type, cod;
  String? razorpayEnable, razorpayKeyId, razorpayKeySecret, razorpayCurrency;
  String? vivawalletEnable,
      vivawalletPublicKey,
      vivawalletMerchantId,
      vivawalletApiKey;
  String? paypalEnable, paypalEmail;
  String? stripeEnable, stripePublishableKey, stripeSecretKey;
  String? mpesaEnable, mpesaConsumerKey, mpesaConsumerSecret;
  String? captoDineinAskPin;

  Outlet(
      {this.success,
      this.message,
      this.id,
      this.name,
      this.address,
      this.phno,
      this.countryCode,
      this.countryName,
      this.logo,
      this.logoUrl,
      this.email,
      this.restUniqueId,
      this.brand_id,
      this.outletOpeningTiming,
      this.outletClosingTiming,
      this.longitude,
      this.latitude,
      this.outletCurrentStatus,
      this.distance,
      this.decimalValue,
      this.isRoundingOn,
      this.roundingId,
      this.showItemPriceWithoutTax,
      this.intervalNumber,
      this.roundingRule,
      this.roundingTag,
      this.deliveryCharges,
      this.adv_order_duration,
      this.advance_order,
      this.description,
      this.delivery_available,
      this.capto_delivery_status,
      this.minimum_total_bill,
      this.minimum_bill_for_delivery_charge,
      this.currency,
      this.packaging_charge,
      this.note,
      this.pack_charge_type,
      this.razorpayCurrency,
      this.razorpayEnable,
      this.razorpayKeyId,
      this.razorpayKeySecret,
      this.vivawalletEnable,
      this.vivawalletPublicKey,
      this.vivawalletMerchantId,
      this.vivawalletApiKey,
      this.stripeEnable,
      this.stripeSecretKey,
      this.stripePublishableKey,
      this.mpesaEnable,
      this.mpesaConsumerSecret,
      this.mpesaConsumerKey,
      this.paypalEnable,
      this.paypalEmail,
      this.captoDineinAskPin,
      this.cod});

  factory Outlet.fromJson(Map<String, dynamic> json) {
    try {
      return Outlet(
          message: json["message"],
          name: json["name"],
          address: json["address"],
          phno: json["phno"],
          countryCode: json['country_code'],
          countryName: json['country_name'],
          logo: json["logo"],
          logoUrl: json["logo_url"],
          email: json["email"],
          restUniqueId: json["rest_unique_id"],
          brand_id: json["brand_id"],
          outletOpeningTiming: json["outlet_opening_timing"],
          outletClosingTiming: json["outlet_closing_timing"],
          longitude: json["longitude"],
          latitude: json["latitude"],
          outletCurrentStatus: json["outlet_current_status"],
          distance: json["distance"].toString(),
          decimalValue: json["decimal_value"].toString(),
          isRoundingOn: json["is_rounding_on"],
          roundingId: json["rounding_id"],
          showItemPriceWithoutTax: json["show_item_price_without_tax"],
          intervalNumber: json["interval_number"],
          roundingRule: json["rounding_rule"],
          deliveryCharges: json["delivery_charges"].toString(),
          advance_order: json["advance_order"],
          delivery_available: json["delivery_available"],
          capto_delivery_status: json["capto_delivery_status"],
          minimum_bill_for_delivery_charge:
              double.parse(json['minimum_bill_for_delivery_charge'].toString()),
          minimum_total_bill:
              double.parse(json['minimum_total_bill'].toString()),
          currency: json['currency'],
          pack_charge_type: json['pack_charge_type'],
          packaging_charge: double.parse(json['packaging_charge'].toString()),
          note: json['note'],
          description: json['description'],
          cod: json.containsKey('cash_on_delivery_pickup')
              ? json['cash_on_delivery_pickup'].toString()
              : null,
          razorpayCurrency: json.containsKey('razorpay_currency')
              ? json['razorpay_currency'].toString()
              : '',
          razorpayEnable: json.containsKey('razorpay_enable')
              ? json['razorpay_enable'].toString()
              : '',
          razorpayKeyId: json.containsKey('razorpay_key_id')
              ? json['razorpay_key_id'].toString()
              : null,
          razorpayKeySecret: json.containsKey('razorpay_key_secret')
              ? json['razorpay_key_secret'].toString()
              : null,
          vivawalletEnable: json.containsKey('vivawallet_enable')
              ? json['vivawallet_enable'].toString()
              : '',
          vivawalletApiKey: json.containsKey('vivawallet_api_key')
              ? json['vivawallet_api_key'].toString()
              : null,
          vivawalletMerchantId: json.containsKey('vivawallet_merchant_id')
              ? json['vivawallet_merchant_id'].toString()
              : null,
          vivawalletPublicKey: json.containsKey('vivawallet_public_key')
              ? json['vivawallet_public_key'].toString()
              : null,
          paypalEnable: json.containsKey('paypal_enable')
              ? json['paypal_enable'].toString()
              : '',
          paypalEmail: json.containsKey('paypal_email')
              ? json['paypal_email'].toString()
              : null,
          stripeEnable: json.containsKey('stripe_enable')
              ? json['stripe_enable'].toString()
              : '',
          stripePublishableKey: json.containsKey('stripe_publishable_key')
              ? json['stripe_publishable_key'].toString()
              : null,
          stripeSecretKey: json.containsKey('stripe_secret_key')
              ? json['stripe_secret_key'].toString()
              : null,
          mpesaEnable: json.containsKey('mpesa_enable')
              ? json['mpesa_enable'].toString()
              : '',
          mpesaConsumerKey: json.containsKey('mpesa_consumer_key')
              ? json['mpesa_consumer_key'].toString()
              : null,
          mpesaConsumerSecret: json.containsKey('mpesa_consumer_secret')
              ? json['mpesa_consumer_secret'].toString()
              : null,
          success: json["success"] ?? 0,
          id: json["id"] ?? 0,
          roundingTag: json["rounding_tag"] ?? 0,
          adv_order_duration: json["adv_order_duration"] ?? 0,
          captoDineinAskPin: (json.containsKey('capto_dinein_ask_pin'))
              ? json['capto_dinein_ask_pin']
              : 'enable');
    } catch (e) {
      Constant.showLog(e);
      return Outlet();
    }
  }

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "id": id,
        "name": name,
        "address": address,
        "phno": phno,
        "country_code": countryCode,
        "country_name": countryName,
        "logo": logo,
        "logo_url": logoUrl,
        "email": email,
        "rest_unique_id": restUniqueId,
        "brand_id": brand_id,
        "outlet_opening_timing": outletOpeningTiming,
        "outlet_closing_timing": outletClosingTiming,
        "longitude": longitude,
        "latitude": latitude,
        "outlet_current_status": outletCurrentStatus,
        "distance": distance,
        "decimal_value": decimalValue,
        "is_rounding_on": isRoundingOn,
        "rounding_id": roundingId,
        "show_item_price_without_tax": showItemPriceWithoutTax,
        "interval_number": intervalNumber,
        "rounding_rule": roundingRule,
        "rounding_tag": roundingTag,
        "delivery_charges": deliveryCharges,
        "adv_order_duration": adv_order_duration,
        "advance_order": advance_order,
        "capto_delivery_status": capto_delivery_status,
        "delivery_available": delivery_available,
        "minimum_total_bill": minimum_total_bill,
        "minimum_bill_for_delivery_charge": minimum_bill_for_delivery_charge,
        "currency": currency,
        "note": note,
        "packaging_charge": packaging_charge,
        "pack_charge_type": pack_charge_type,
        "description": description,
        "cash_on_delivery_pickup": cod,
        "razorpay_currency": razorpayCurrency,
        "razorpay_enable": razorpayEnable,
        "razorpay_key_id": razorpayKeyId,
        "razorpay_key_secret": razorpayKeySecret,
        "vivawallet_enable": vivawalletEnable,
        "vivawallet_api_key": vivawalletApiKey,
        "vivawallet_merchant_id": vivawalletMerchantId,
        "vivawallet_public_key": vivawalletPublicKey,
        "paypal_enable": paypalEnable,
        "paypal_email": paypalEmail,
        "stripe_enable": stripeEnable,
        "stripe_publishable_key": stripePublishableKey,
        "stripe_secret_key": stripeSecretKey,
        "mpesa_enable": mpesaEnable,
        "mpesa_consumer_key": mpesaConsumerKey,
        "mpesa_consumer_secret": mpesaConsumerSecret,
        "capto_dinein_ask_pin": captoDineinAskPin
      };
}
