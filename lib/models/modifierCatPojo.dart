
class ModifierCat {
  String? catId,catName;
  int? modifierSelection;
  int? minSelection=0,tot_sel=0;

  ModifierCat(
      {this.catId, this.catName, this.modifierSelection, this.minSelection=0});

  ModifierCat.fromJson(Map<String, dynamic> json) {
    catId = json['cat_id'].toString();
    catName = json['cat_name'];
    modifierSelection = int.parse(json['modifier_selection'].toString());
    minSelection = json['min_selection'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cat_id'] = this.catId;
    data['cat_name'] = this.catName;
    data['modifier_selection'] = this.modifierSelection;
    data['min_selection'] = this.minSelection;
    return data;
  }

}