class VariationPodo{
  String? id,name,amount,amount_wt,quantity;

  VariationPodo({this.id, this.name, this.amount, this.amount_wt, this.quantity});

  VariationPodo.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    amount_wt = json['amount_wt'];
    id = json['id'];
    name = json['name'];
    if(json.containsKey('quantity'))
      quantity = json['quantity'];
    if(quantity==null || quantity!.isEmpty)
      quantity='1';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['amount_wt'] = this.amount_wt;
    data['id'] = this.id;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    return data;
  }
}