import 'package:capto/DBOrder.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppModel with ChangeNotifier {

  Map<String, dynamic>? appConfig;
  bool? darkTheme = false,isFirstTime=true,isLoading=false;
  int value = 0,dval=2,currentTab=0;
  String exp_grand='0.0';

  AppModel() {
    getConfig();
  }

  Future<bool> getConfig() async {
    isLoading=true;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if(prefs.containsKey(KeyConstant.key_theme))
        darkTheme = prefs.getBool(KeyConstant.key_theme);
      if(prefs.containsKey(KeyConstant.key_firstTime))
        isFirstTime = prefs.getBool(KeyConstant.key_firstTime);
      int cnt =await DBProvider().getCount();
      if (cnt > 0) {
        await DBProvider().getOrderDetail();
      }
      isLoading=false;
      notifyListeners();
      return true;
  }

  void updateFirstTime(bool firstTime) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      isFirstTime = firstTime;
      await prefs.setBool(KeyConstant.key_firstTime, firstTime);
      notifyListeners();
    } catch (e) {}
  }

  void updateTheme(bool theme) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      darkTheme = theme;
      await prefs.setBool(KeyConstant.key_theme, theme);
      notifyListeners();
    } catch (e) {}
  }

  void increment() {
    if(Constant.decimal_value!=null && Constant.decimal_value.isNotEmpty)
      dval=int.parse(Constant.decimal_value);
    if(Constant.dishorederlist.isNotEmpty){
      value=Constant.dishorederlist.map<int>((m) => int.parse(m.qty!)).reduce((a,b )=>a+b);
      exp_grand=Constant.dishorederlist
          .map<double>((m) => int.parse(m.qty!) * double.parse(m.price!))
          .reduce((a, b) => a + b).toStringAsFixed(dval);
    }else {
      value = 0;
      exp_grand=0.0.toStringAsFixed(dval);
    }
    if(Constant.placeitemlist.isNotEmpty)
      value=value+Constant.placeitemlist
          .map<int>(
              (m) => int.parse(m.qty!))
          .reduce((a, b) => a + b);
    notifyListeners();
  }

  void changeTab(int page){
    currentTab=page;
  }

}

class App {
  Map<String, dynamic> appConfig;

  App(this.appConfig);
}
