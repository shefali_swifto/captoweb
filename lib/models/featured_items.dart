import 'dart:convert';

List<FeaturedItems> featuredItemsFromJson(String str) => new List<FeaturedItems>.from(json.decode(str).map((x) => FeaturedItems.fromJson(x)));

String? featuredItemsToJson(List<FeaturedItems> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class FeaturedItems {
    int? dishid;
    int? cusineid;
    String? dishname;
    String? dishimage;
    String? dishimageUrl;
    String? hsnNo;
    String? soldBy;
    String? type;
    String? description;
    int? purchasedUnitId;
    String? purchasedUnitName;
    int? usedUnitId;
    String? usedUnitName;
    String? defaultSaleValue;
    String? price;
    String? priceWithoutTax;
    String? taxAmt;
    int? inStock;
    int? lowStock;
    List<dynamic>? taxData;
    String? preflag;
    String? comboFlag;

    FeaturedItems({
        this.dishid,
        this.cusineid,
        this.dishname,
        this.dishimage,
        this.dishimageUrl,
        this.hsnNo,
        this.soldBy,
        this.type,
        this.description,
        this.purchasedUnitId,
        this.purchasedUnitName,
        this.usedUnitId,
        this.usedUnitName,
        this.defaultSaleValue,
        this.price,
        this.priceWithoutTax,
        this.taxAmt,
        this.inStock,
        this.lowStock,
        this.taxData,
        this.preflag,
        this.comboFlag,
    });

    factory FeaturedItems.fromJson(Map<String?, dynamic> json) => new FeaturedItems(
        dishid: json["dishid"],
        cusineid: json["cusineid"],
        dishname: json["dishname"],
        dishimage: json["dishimage"],
        dishimageUrl: json["dishimage_url"],
        hsnNo: json["hsn_no"],
        soldBy: json["sold_by"],
        type: json["type"],
        description: json["description"],
        purchasedUnitId: json["purchased_unit_id"],
        purchasedUnitName: json["purchased_unit_name"],
        usedUnitId: json["used_unit_id"],
        usedUnitName: json["used_unit_name"],
        defaultSaleValue: json["default_sale_value"],
        price: json["price"],
        priceWithoutTax: json["price_without_tax"],
        taxAmt: json["tax_amt"],
        inStock: json["in_stock"],
        lowStock: json["low_stock"],
        taxData: new List<dynamic>.from(json["tax_data"].map((x) => x)),
        preflag: json["preflag"],
        comboFlag: json["combo_flag"],
    );

    Map<String?, dynamic> toJson() => {
        "dishid": dishid,
        "cusineid": cusineid,
        "dishname": dishname,
        "dishimage": dishimage,
        "dishimage_url": dishimageUrl,
        "hsn_no": hsnNo,
        "sold_by": soldBy,
        "type": type,
        "description": description,
        "purchased_unit_id": purchasedUnitId,
        "purchased_unit_name": purchasedUnitName,
        "used_unit_id": usedUnitId,
        "used_unit_name": usedUnitName,
        "default_sale_value": defaultSaleValue,
        "price": price,
        "price_without_tax": priceWithoutTax,
        "tax_amt": taxAmt,
        "in_stock": inStock,
        "low_stock": lowStock,
        "tax_data": new List<dynamic>.from(taxData!.map((x) => x)),
        "preflag": preflag,
        "combo_flag": comboFlag,
    };
}