import 'VariationPodo.dart';

class DishOrderPodo{
  String? dishid,dishname,qty,priceperdish,price,orderdetailid,discount;
  String? cusineid;
  String? dishtype;
  String? description;
  String? dishimage;
  String? preflag,preid;
  String? combo_flag;
  String? dish_comment;
  String? sold_by;
  String? price_without_tax,priceper_without_tax,tax_amt,tot_tax;
  List? tax_data = null;
  List? pre;
  List? combo_list;
  String? offer,tot_disc;
  List<VariationPodo>? varPojoList;
  String? brandid,rest_id,rest_uniq_id,rest_name,rest_address,rest_img;
  bool isNew;

  DishOrderPodo({
      this.dishid,
      this.dishname,
      this.qty,
      this.priceperdish,
      this.price,
      this.orderdetailid,
      this.discount,
      this.cusineid,
      this.dishtype,
      this.description,
      this.dishimage,
      this.preflag,
      this.preid,
      this.combo_flag,
      this.dish_comment,
      this.sold_by,
      this.price_without_tax,
      this.priceper_without_tax,
      this.tax_amt,
      this.tot_tax,
      this.tax_data,
      this.pre,
      this.combo_list,
      this.offer,
      this.tot_disc,
      this.varPojoList,
      this.brandid,
      this.rest_id,
      this.rest_uniq_id,
      this.rest_name,
      this.rest_address,
      this.rest_img,this.isNew=true});
}