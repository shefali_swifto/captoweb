class TaxData {
  String? id,text,value,taxAmount;
  String tax_value='0';

  TaxData({this.id, this.text, this.value, this.taxAmount,this.tax_value='0'});

  TaxData.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    text = json['text'].toString();
    value = json['value'].toString();
    taxAmount = json['tax_amount'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['value'] = this.value;
    data['tax_amount'] = this.taxAmount;
    return data;
  }

  TaxData.fromLocalJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    text = json['txt'].toString();
    value = json['val'].toString();
    taxAmount = json['tax_amount'].toString();
    tax_value=json['tax_value'].toString();
  }

  Map<String, dynamic> toLocalJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['txt'] = this.text;
    data['val'] = this.value;
    data['tax_amount'] = this.taxAmount;
    data['tax_value']=this.tax_value;
    return data;
  }

}