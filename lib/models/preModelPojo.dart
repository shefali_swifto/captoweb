class Pre {
  String? preid,pretype,prenm,preprice,type;
  bool? isCheck=false;
  int qty=1;

  Pre({this.preid, this.pretype, this.prenm, this.preprice,this.qty=1});

  Pre.fromJson(Map<String, dynamic> json) {
    preid = json['preid'].toString();
    prenm = json['prenm'];
    preprice = json['preprice'].toString();

    if(json.containsKey('quantity'))
      qty=json['quantity'];

    if(json.containsKey('type')){
      type=json['type'].toString();
      pretype=json['pretype'].toString();
    }else{
      pretype = json['cat_id'].toString();
      if(json.containsKey('pretype'))
        type=json['pretype'].toString();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['preid'] = this.preid;
    data['pretype'] = this.pretype;
    data['prenm'] = this.prenm;
    data['preprice'] = this.preprice;
    data['type'] = this.type;
    data['quantity']=this.qty;
    return data;
  }

}