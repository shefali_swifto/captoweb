import 'dart:convert';

import 'package:capto/models/modifierCatPojo.dart';
import 'package:capto/models/preModelPojo.dart';

List<OutletMenu> outletMenuFromJson(String str) => List<OutletMenu>.from(json.decode(str).map((x) => OutletMenu.fromJson(x)));

String? outletMenuToJson(List<OutletMenu> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OutletMenu {
    int? cuisineId;
    String? cuisineName;
    String? cuisineImage;
    String? itemPricing;
    String? taxId;
    int? cuisineDishCount;
    List<CuisineDishArray>? cuisineDishArray;

    OutletMenu({
        this.cuisineId,
        this.cuisineName,
        this.cuisineImage,
        this.itemPricing,
        this.taxId,
        this.cuisineDishCount,
        this.cuisineDishArray,
    });

    factory OutletMenu.fromJson(Map<String?, dynamic> json) => OutletMenu(
        cuisineId: json["cuisine_id"],
        cuisineName: json["cuisine_name"],
        cuisineImage: json["cuisine_image"],
        itemPricing: json["item_pricing"],
        taxId: json["tax_id"],
        cuisineDishCount: json["cuisine_dish_count"],
        cuisineDishArray: json["cuisine_dish_array"] == null ? null : List<CuisineDishArray>.from(json["cuisine_dish_array"].map((x) => CuisineDishArray.fromJson(x))),
    );

    Map<String?, dynamic> toJson() => {
        "cuisine_id": cuisineId,
        "cuisine_name": cuisineName,
        "cuisine_image": cuisineImage,
        "item_pricing": itemPricing,
        "tax_id": taxId,
        "cuisine_dish_count": cuisineDishCount,
        "cuisine_dish_array": cuisineDishArray == null ? null : List<dynamic>.from(cuisineDishArray!.map((x) => x.toJson())),
    };
}

class CuisineDishArray {
    int? dishid;
    int? cusineid;
    String? dishname;
    String? dishimage;
    String? dishimageUrl;
    String? hsnNo;
    String? soldBy;
    String? type;
    String? description;
    int? purchasedUnitId;
    String? purchasedUnitName;
    int? usedUnitId;
    String? usedUnitName;
    String? defaultSaleValue;
    String? price;
    String? priceWithoutTax;
    String? taxAmt;
    double? inStock;
    int? lowStock;
    List<TaxDatum>? taxData;
    String? preflag;
    List<Pre>? pre;
    List<ModifierCat>? modifierCat;
    String? comboFlag;

    CuisineDishArray({
        this.dishid,
        this.cusineid,
        this.dishname,
        this.dishimage,
        this.dishimageUrl,
        this.hsnNo,
        this.soldBy,
        this.type,
        this.description,
        this.purchasedUnitId,
        this.purchasedUnitName,
        this.usedUnitId,
        this.usedUnitName,
        this.defaultSaleValue,
        this.price,
        this.priceWithoutTax,
        this.taxAmt,
        this.inStock,
        this.lowStock,
        this.taxData,
        this.preflag,
        this.pre,
        this.modifierCat,
        this.comboFlag,
    });

    factory CuisineDishArray.fromJson(Map<String?, dynamic> json) => CuisineDishArray(
        dishid: json["dishid"],
        cusineid: json["cusineid"],
        dishname: json["dishname"],
        dishimage: json["dishimage"],
        dishimageUrl: json["dishimage_url"],
        hsnNo: json["hsn_no"],
        soldBy: json["sold_by"],
        type: json["type"],
        description: json["description"],
        purchasedUnitId: json["purchased_unit_id"],
        purchasedUnitName: json["purchased_unit_name"],
        usedUnitId: json["used_unit_id"],
        usedUnitName: json["used_unit_name"],
        defaultSaleValue: json["default_sale_value"],
        price: json["price"],
        priceWithoutTax: json["price_without_tax"],
        taxAmt: json["tax_amt"],
        inStock: json["in_stock"] == null ? null : json["in_stock"].todouble(),
        lowStock: json["low_stock"],
        taxData: json["tax_data"] == null ? null : List<TaxDatum>.from(json["tax_data"].map((x) => TaxDatum.fromJson(x))),
        preflag: json["preflag"],
        pre: json["pre"] == null ? null : List<Pre>.from(json["pre"].map((x) => Pre.fromJson(x))),
        modifierCat: json["modifier_cat"] == null ? null : List<ModifierCat>.from(json["modifier_cat"].map((x) => ModifierCat.fromJson(x))),
        comboFlag: json["combo_flag"],
    );

    Map<String?, dynamic> toJson() => {
        "dishid": dishid,
        "cusineid": cusineid,
        "dishname": dishname,
        "dishimage": dishimage,
        "dishimage_url": dishimageUrl,
        "hsn_no": hsnNo,
        "sold_by": soldBy,
        "type": type,
        "description": description,
        "purchased_unit_id": purchasedUnitId,
        "purchased_unit_name": purchasedUnitName,
        "used_unit_id": usedUnitId,
        "used_unit_name": usedUnitName,
        "default_sale_value": defaultSaleValue,
        "price": price,
        "price_without_tax": priceWithoutTax,
        "tax_amt": taxAmt,
        "in_stock": inStock,
        "low_stock": lowStock,
        "tax_data": taxData == null ? null : List<dynamic>.from(taxData!.map((x) => x.toJson())),
        "preflag": preflag,
        "pre": pre == null ? null : List<dynamic>.from(pre!.map((x) => x.toJson())),
        "modifier_cat": modifierCat == null ? null : List<dynamic>.from(modifierCat!.map((x) => x.toJson())),
        "combo_flag": comboFlag,
    };
}



class TaxDatum {
    int? id;
    
    String? text;
    String? value;
    String? taxAmount;

    TaxDatum({
        this.id,
        this.text,
        this.value,
        this.taxAmount,
    });

    factory TaxDatum.fromJson(Map<String?, dynamic> json) =>  TaxDatum(
        id: json["id"],
        text: json["text"],
        value: json["value"],
        taxAmount: json["tax_amount"],
    );

    Map<String?, dynamic> toJson() => {
        "id": id,
        "text":text,
        "value": value,
        "tax_amount": taxAmount,
    };
}