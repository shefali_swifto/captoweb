class ComboItem {
  String? itemId,itemName,qty,dishId;

  ComboItem({this.itemId, this.itemName, this.qty});

  ComboItem.fromJson(Map<String, dynamic> json) {
    itemId = json['item_id'].toString();
    itemName = json['item_name'];
    qty = json['qty'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['item_id'] = this.itemId;
    data['item_name'] = this.itemName;
    data['qty'] = this.qty;
    return data;
  }


}