import 'dart:convert';

import 'package:capto/models/combopojo.dart';
import 'package:capto/models/modifierCatPojo.dart';
import 'package:capto/models/preModelPojo.dart';
import 'package:capto/models/taxPojo.dart';


class DishPojo {

  String? dishid,cusineid,dishname,dishimage,dishimageUrl,colorCode,costing_price;
  String? soldBy,barcodeNo,batchNo,prodctNo,hsnNo,itemType,type;
  String? timePeriod,serviceDuration,bufferDuration,description,compositeItemTrackStock;
  String? allowOpenPrice,bestSellingItem,purchasedUnitId,purchasedUnitName,usedUnitId;
  String? usedUnitName,defaultSaleValue,price,priceWithoutTax,taxAmt,inStock,lowStock;
  String? preflag,comboFlag,packageFlag,offers,discountAmount;
  List<TaxData>? taxData=[];
  List<Pre>? pre=[];
  List<ModifierCat>? modifierCat=[];
  List<ComboItem>? comboItem=[],packageItem=[];
  List<InventoryItem>? inventoryItem=[];

  DishPojo(
      {this.dishid,
        this.cusineid,
        this.dishname,
        this.dishimage,
        this.dishimageUrl,
        this.colorCode,
        this.soldBy,
        this.barcodeNo,
        this.batchNo,
        this.prodctNo,
        this.hsnNo,
        this.itemType,
        this.type,
        this.timePeriod,
        this.serviceDuration,
        this.bufferDuration,
        this.description,
        this.compositeItemTrackStock,
        this.allowOpenPrice,
        this.bestSellingItem,
        this.purchasedUnitId,
        this.purchasedUnitName,
        this.usedUnitId,
        this.usedUnitName,
        this.defaultSaleValue,
        this.price,
        this.priceWithoutTax,
        this.taxAmt,this.offers,
        this.inStock,this.discountAmount,
        this.lowStock,this.packageItem,
        this.taxData,this.pre,this.modifierCat,
        this.inventoryItem,
        this.preflag,
        this.comboFlag,
        this.packageFlag});

  DishPojo.fromJson(Map<String, dynamic> json) {
    dishid = json['dishid'].toString();
    cusineid = json['cusineid'].toString();
    dishname = json['dishname'];
    dishimage = json['dishimage'];
    dishimageUrl = json['dishimage_url'];
    colorCode = json['color_code'];
    soldBy = json['sold_by'];
    barcodeNo = json['barcode_no'].toString();
    batchNo = json['batch_no'].toString();
    prodctNo = json['prodct_no'].toString();
    hsnNo = json['hsn_no'].toString();
    itemType = json['item_type'];
    type = (json.containsKey('type'))?json['type']:'';
    timePeriod = json['time_period'].toString();
    serviceDuration = (json.containsKey('service_duration'))?json['service_duration'].toString():'0';
    bufferDuration = (json.containsKey('buffer_duration'))?json['buffer_duration'].toString():'0';
    description = json['description'];
    compositeItemTrackStock = json['composite_item_track_stock'].toString();
    allowOpenPrice = json['allow_open_price'].toString();
    bestSellingItem = json['best_selling_item'].toString();
    purchasedUnitId = json['purchased_unit_id'].toString();
    purchasedUnitName = json['purchased_unit_name'];
    usedUnitId = json['used_unit_id'].toString();
    usedUnitName = json['used_unit_name'];
    defaultSaleValue = json['default_sale_value'].toString();
    price = json['price'].toString();
    priceWithoutTax = json['price_without_tax'].toString();
    taxAmt = (json.containsKey('tax_amt'))?json['tax_amt'].toString():'0';
    inStock = json['in_stock'].toString();
    lowStock = json['low_stock'].toString();
    if (json['tax_data'] != null) {
      taxData = [];
      json['tax_data'].forEach((v) {
        taxData!.add(new TaxData.fromJson(v));
      });
    }
    if (json['inventory_item'] != null) {
      inventoryItem =[];
      json['inventory_item'].forEach((v) {
        inventoryItem!.add(new InventoryItem.fromJson(v));
      });
    }
    if (json['combo_item'] != null) {
      comboItem = [];
      json['combo_item'].forEach((v) {
        comboItem!.add(new ComboItem.fromJson(v));
      });
    }
    if (json['package_item'] != null) {
      packageItem = [];
      json['package_item'].forEach((v) {
        packageItem!.add(new ComboItem.fromJson(v));
      });
    }
    if(json['pre']!=null){
      pre=[];
      json['pre'].forEach((v){
        pre!.add(new Pre.fromJson(v));
      });
    }
    if(json['modifier_cat']!=null){
      modifierCat=[];
      json['modifier_cat'].forEach((v){
        modifierCat!.add(new ModifierCat.fromJson(v));
      });
    }
    preflag = (json.containsKey('preflag'))?json['preflag'].toString():'false';
    comboFlag = (json.containsKey('combo_flag'))?json['combo_flag'].toString():'false';
    packageFlag = (json.containsKey('package_flag'))?json['package_flag'].toString():'false';
    packageItem=json['package_item'];
    offers = (json.containsKey('offers'))?json['offers'].toString():'';
    discountAmount = (json.containsKey('discount_amount'))?json['discount_amount'].toString():'';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dishid'] = this.dishid;
    data['cusineid'] = this.cusineid;
    data['dishname'] = this.dishname;
    data['dishimage'] = this.dishimage;
    data['dishimage_url'] = this.dishimageUrl;
    data['color_code'] = this.colorCode;
    data['sold_by'] = this.soldBy;
    data['barcode_no'] = this.barcodeNo;
    data['batch_no'] = this.batchNo;
    data['prodct_no'] = this.prodctNo;
    data['hsn_no'] = this.hsnNo;
    data['item_type'] = this.itemType;
    data['type'] = this.type;
    data['time_period'] = this.timePeriod;
    data['service_duration'] = this.serviceDuration;
    data['buffer_duration'] = this.bufferDuration;
    data['description'] = this.description;
    data['composite_item_track_stock'] = this.compositeItemTrackStock;
    data['allow_open_price'] = this.allowOpenPrice;
    data['best_selling_item'] = this.bestSellingItem;
    data['purchased_unit_id'] = this.purchasedUnitId;
    data['purchased_unit_name'] = this.purchasedUnitName;
    data['used_unit_id'] = this.usedUnitId;
    data['used_unit_name'] = this.usedUnitName;
    data['default_sale_value'] = this.defaultSaleValue;
    data['price'] = this.price;
    data['price_without_tax'] = this.priceWithoutTax;
    data['tax_amt'] = this.taxAmt;
    data['in_stock'] = this.inStock;
    data['low_stock'] = this.lowStock;
    if (this.taxData != null) {
      data['tax_data'] = this.taxData!.map((v) => v.toJson()).toList();
    }
    if (this.inventoryItem != null) {
      data['inventory_item'] =
          this.inventoryItem!.map((v) => v.toJson()).toList();
    }
    if (this.comboItem != null) {
      data['combo_item'] = this.comboItem!.map((v) => v.toJson()).toList();
    }
    if (this.packageItem != null) {
      data['package_item'] = this.packageItem!.map((v) => v.toJson()).toList();
    }
    if(this.modifierCat!=null){
      data['modifier_cat']=this.modifierCat!.map((e) => e.toJson()).toList();
    }
    data['preflag'] = this.preflag;
    data['combo_flag'] = this.comboFlag;
    data['package_flag'] = this.packageFlag;
    data['offers'] = this.offers;
    data['discount_amount'] = this.discountAmount;
    return data;
  }

}

class InventoryItem {
  String? stockItemId,qty,stockItemName;

  InventoryItem({this.stockItemId, this.qty, this.stockItemName});

  InventoryItem.fromJson(Map<String, dynamic> json) {
    stockItemId = json['stock_item_id'].toString();
    qty = json['qty'].toString();
    stockItemName = json['stock_item_name'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['stock_item_id'] = this.stockItemId;
    data['qty'] = this.qty;
    data['stock_item_name'] = this.stockItemName;
    return data;
  }
}