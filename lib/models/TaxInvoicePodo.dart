class TaxInvoicePodo{
  String? id,name,per,type,per_total,amount_total;

  TaxInvoicePodo({this.id, this.name, this.per, this.type, this.per_total,
      this.amount_total});

  factory TaxInvoicePodo.fromJson(Map<String, dynamic> json) => new TaxInvoicePodo(
    id: json["id"],
    name: json["name"],
    per: json["per"],
    type: json["type"],
    per_total: json["per_total"],
    amount_total: json["amount_total"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "per": per,
    "type": type,
    "per_total": per_total,
    "amount_total": amount_total,
  };
}