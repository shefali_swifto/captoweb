import 'dart:convert';

List<Categories> categoriesFromJson(String str) => new List<Categories>.from(json.decode(str).map((x) => Categories.fromJson(x)));

String categoriesToJson(List<Categories> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Categories {
    int? cuisineId;
    String? cuisineName;
    String? cuisineImage;
    String? itemPricing;
    String? taxId;
    int? cuisineDishCount;

    Categories({
        this.cuisineId,
        this.cuisineName,
        this.cuisineImage,
        this.itemPricing,
        this.taxId,
        this.cuisineDishCount,
    });

    factory Categories.fromJson(Map<String, dynamic> json) => new Categories(
        cuisineId: json["cuisine_id"],
        cuisineName: json["cuisine_name"],
        cuisineImage: json["cuisine_image"],
        itemPricing: json["item_pricing"],
        taxId: json["tax_id"],
        cuisineDishCount: json["cuisine_dish_count"],
    );

    Map<String, dynamic> toJson() => {
        "cuisine_id": cuisineId,
        "cuisine_name": cuisineName,
        "cuisine_image": cuisineImage,
        "item_pricing": itemPricing,
        "tax_id": taxId,
        "cuisine_dish_count": cuisineDishCount,
    };
}
