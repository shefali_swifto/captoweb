import 'dart:convert';
import 'dart:io';

import 'package:capto/routes/route_helper.dart';
import 'package:capto/screens/loginScreen.dart';
import 'package:capto/util/checkConnection.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
// import 'package:client_information/client_information.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

Future<Map> checkPhone(String phone) async {
  try {
    String url = Constant.base_url + "capto_check_phone_no";
    Constant.showLog("===Check Phone===");
    Constant.showLog(url);
    Map reqdata = {'country_code': Constant.country_code, 'phone_no': phone};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("check Phone Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> registerUser(String phone, String email, String name) async {
  try {
    Constant.showLog("===Register===");
    String url = Constant.base_url + "capto_register_customer";
    Constant.showLog(url);

    Map reqdata = {
      'country_code': Constant.country_code,
      'phone_no': phone,
      'email': email,
      'password': '',
      'name': name,
      'login_type': 'phone',
      'country': Constant.country,
      'city': '',
      'facebook_id': '',
      'google_id': ''
    };

    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("Register Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

// Future updateFCM() async {
//   try {
//     String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
//     String deviceid = '';
//     ClientInformation info;
//     try {
//       info = await ClientInformation.fetch();
//       deviceid=info.deviceId;
//     } on PlatformException {
//       Constant.showLog('Failed to get client information');
//     }
//     String devicetype = Platform.operatingSystem;
//     String url = Constant.base_url + "capto_customer_update_fcm";
//     Constant.showLog("===Update FCM===");
//     Constant.showLog(url);
//
//     Map reqdata = {
//       'cust_id': custid,
//       'device_id': deviceid,
//       'device_type': devicetype
//     };
//     bool conn = await isConnection();
//     // if (conn) {
//     //   FirebaseMessaging.instance.getToken().then((token)async {
//     //     reqdata['fcm_id']=token;
//     //     Constant.showLog(reqdata);
//     //     final response = await http.post(
//     //       Uri.parse(url),
//     //       body: reqdata,
//     //     );
//     //     Constant.showLog("update fcm result");
//     //     Constant.showLog(response.body);
//     //   });
//     // } else {
//     //   Map res = {
//     //     'success': Constant.api_no_net,
//     //     'message': Constant.api_no_net_msg
//     //   };
//     //   Constant.showLog(res);
//     // }
//   } catch (e) {
//     Constant.showLog("error");
//     Constant.showLog(e);
//     Map res = {
//       'success': Constant.api_catch_error,
//       'message': Constant.api_catch_error_msg
//     };
//     Constant.showLog(res);
//   }
// }

Future<Map> getProfile() async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Get Profile===");
    String url = Constant.base_url + "capto_get_cust_data";
    Constant.showLog(url);

    Map reqdata = {'cust_id': custid};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("get profile Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getUserPoints(String restid) async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Get Points===");
    String url = Constant.base_url + "capto_get_cust_points";
    Constant.showLog(url);

    Map reqdata = {'cust_id': custid, 'restaurant_id': restid};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("get points");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> updateProfile(String nm, String city, String country, String code,
    String ph, String mail) async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Update Profile===");
    String url = Constant.base_url + "capto_update_info_customer";
    Constant.showLog(url);

    Map reqdata = {
      'cust_id': custid,
      'name': nm,
      'city': city,
      'country': country,
      'country_code': code,
      'phone_no': ph,
      'email': mail
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("update profile Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> updateAddress(String addressJson) async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Update Address===");
    String url = Constant.base_url + "capto_update_customer_address";
    Constant.showLog(url);

    Map reqdata = {
      'cust_id': custid,
      'address': addressJson,
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("update address Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getNotifications() async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog('get notification list');
    String url = Constant.base_url + "capto_get_notification";
    Constant.showLog(url);

    Map reqdata = {'cust_id': (custid!=null && custid.isNotEmpty)?custid:'0'};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("===Notification Result===");
      Constant.showLog(response.body);
      List list = json.decode(response.body);
      if (list == null) list = [];
      return {
        'success':1,
        'data':list
      };
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future logout(BuildContext context) async {
  await KeyConstant.saveString(KeyConstant.key_user_id, '');
  await KeyConstant.saveString(KeyConstant.key_user_name, '');
  await KeyConstant.saveString(KeyConstant.key_user_email, '');
  await KeyConstant.saveString(KeyConstant.key_user_phone, '');
  await KeyConstant.saveString(KeyConstant.key_user_address, '');
  Get.offNamed(RouteHelper.getSignInRoute());

}
