import 'dart:convert';

import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/util/checkConnection.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:http/http.dart' as http;

Future<Map> getOrderList(String type, int page) async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Get Order List===");
    String url = Constant.base_url + "capto_get_customer_orders";
    Constant.showLog(url);

    Map reqdata = {'cust_id': custid, 'page_no': page.toString(), 'type': type};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
        body: reqdata,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      );
      Constant.showLog("===Get Order List Result===");
      Constant.showLog(response.body);
      List list = json.decode(response.body);
      if (list == null) list = [];
      return {'success': 1, 'data': list};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getOrderDetail(
    String order_id, String restid, String runiqid,String brandid) async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Get Order Detail===");
    String url = Constant.base_url + "get_customer_order_detail";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'cust_id': custid,
      'order_id': order_id,
      'restaurant_id': restid,
      'rest_unique_id': runiqid
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
        body: reqdata,
        headers: {"brand-id": brandid}
      );
      Constant.showLog("get Result Order Detail");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getReOrderDetail(
    String order_id, String restid, String runiqid,String brandid) async {
  try {
    String? custid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    Constant.showLog("===Get ReOrder Detail===");
    String url = Constant.base_url + "reorder_details";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'cust_id': custid,
      'order_id': order_id,
      'restaurant_id': restid,
      'rest_unique_id': runiqid
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
        body: reqdata,
        headers: {"brand-id": brandid}
      );
      Constant.showLog("get Result Order Detail");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> cancelOrder(String order_id, String restid, String runiqid,String brandid) async {
  try {
    Constant.showLog("===Cancel Order===");
    String url = Constant.base_url + "capto_customer_online_order_update_status";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'new_state': '4',
      'order_id': order_id,
      'restaurant_id': restid,
      'rest_unique_id': runiqid
    };

    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
        headers: {"brand-id": brandid},
      );
      Constant.showLog("get Result Order Detail");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> initTransaction(Map reqdata,String brandid) async {
  try {
    bool conn = await isConnection();
    if (conn) {
      String url = Constant.base_url + "init_transaction_online_payment_order";//"init_transaction_capto_app";
      Constant.showLog("===Init Transaction===");
      Constant.showLog(url);
      Constant.showLog("brandid:"+brandid);
      Constant.showLog(reqdata);
      final response = await http.post(
        Uri.parse(url),
        body: jsonEncode(reqdata),
        headers: {"brand-id": brandid,'Content-Type': 'application/json'},
      );
      Constant.showLog("===Init Transaction Response===");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> updatePayment(String order_id,String transid,String status,String rid,String ruid,String brandid) async {
  try {
    Constant.showLog("===Payment Order Update===");
    String url = Constant.base_url + "update_payment_customer_app";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'restaurant_id':rid,
      'rest_unique_id':ruid,
      'order_id': order_id,
      'txn_id': transid,
      'payment_status': status,
    };

    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
        headers: {"brand-id": brandid},
      );
      Constant.showLog("Payment Order Update Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<dynamic> createCheckoutSession(List<DishOrderPodo>  productItems)async{
  Uri url=Uri.parse("https://api.stripe.com/v1/checkout/sessions");
  List lineItems=[];
  productItems.forEach((element) {
    lineItems.add({
      'price':element.price!,
      'quantity':element.qty!
    });
  });
  Map req={
    'payment_method_types':['card'],
    'line_items':lineItems,
    'mode':'payment',
    'success_url':'https://success.com/{CHECKOUT_SESSION_ID}',
    'cancel_url':'https://cancel.com'
  };
  final response=await http.post(url,
  body:'success_url=http://checkout.stripe.dev/success&mode=payment&line_items=$lineItems',//jsonEncode(req),//
  headers: {
    'Authorization':'Bearer sk_test_IQu8hrsbjLe2f8VQajN1bI8H008LuSdmHO',
    'Content-Type':'application/x-www-form-urlencoded'
  });
Constant.showLog(response.body);
  return json.decode(response.body)["id"];
}

