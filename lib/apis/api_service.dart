import 'dart:convert';
import 'dart:developer';

import 'package:capto/util/checkConnection.dart';
import 'package:capto/util/constant.dart';
import 'package:http/http.dart' as http;

Future<dynamic> apiPostService(String api,Map reqData) async {
  try {
    String url=Constant.base_url+api;
    Constant.showLog("===call api===");
    Constant.showLog(url);

    Constant.showLog(reqData);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqData,
      );
      Constant.showLog("Api Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message':Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message':Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<dynamic> apiPostHeaderService(String api,Map reqData) async {
  try {
    String url=Constant.base_url+api;
    String brandid=Constant.shopData!.brand_id!;
    Constant.showLog("===call api==="+brandid);
    Constant.showLog(url);
    Constant.showLog(reqData);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqData,
          headers: {'brand-id':brandid}
      );
      Constant.showLog(url);
      // Constant.showLog("Api Result");
      // Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message':Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
    };
    return res;
  }
}

Future<dynamic> apiObjPostHeaderService(String api,Map reqData) async {
  try {
    String url=Constant.base_url+api;
    String brandid=Constant.shopData!.brand_id!;
    Constant.showLog("===call api==="+brandid);
    Constant.showLog(url);
    log(json.encode(reqData));
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: json.encode(reqData),
          headers: {'brand-id':brandid,"Content-Type": "application/json"}
      );
      Constant.showLog("Api Result");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message':Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message':Constant.api_catch_error_msg
    };
    return res;
  }
}
