import 'dart:convert';

import 'package:capto/models/outlets.dart';
import 'package:capto/util/checkConnection.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:http/http.dart' as http;

Future<Map> fetchNearbyOutlets(String lat, String lng, String zipcode) async {
  try {
    String url = Constant.base_url + "capto_get_near_by_outlets";
    Constant.showLog('inside fetch outlets');
    Constant.showLog(url);
    String? uid = await KeyConstant.retriveString(KeyConstant.key_user_id);
    String? c = await KeyConstant.retriveString(KeyConstant.key_country);
    String? t = await KeyConstant.retriveString(KeyConstant.key_type);
    Map reqdata = {
      'latitude': lat,
      'longitude': lng,
      'cust_id': (uid==null)?'':uid,
      'zip_code': zipcode,
      'type': t,
      'country': (c==null)?'':c
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("===Get Results");
      Constant.showLog(response.body);
      Map d = json.decode(response.body);
      return d;
    } else {
      Map outlet = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return outlet;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map outlet = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return outlet;
  }
}

Future<Map> searchOutlet(String str) async {
  try {
    String url = Constant.base_url + "search_capto_outlet";
    Constant.showLog('search outlets');
    Constant.showLog(url);
    Map reqdata = {'search_str': str};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("===Get Results");
      Constant.showLog(response.body);
      Map d = json.decode(response.body);
      return d;
    } else {
      Map outlet = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return outlet;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map outlet = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return outlet;
  }
}

Future<Outlet> getOutletDetail(String outletid, String brandid) async {
  try {
    String? zcode = await KeyConstant.retriveString(KeyConstant.key_zipcode);
    String url = Constant.base_url + "get_outlet_details";
    Constant.showLog('get outlet detail');
    Constant.showLog(url);
    Constant.showLog("brandid:" + brandid);
    Map reqdata = {'outlet_id': outletid};
    if (zcode != null && zcode.isNotEmpty) reqdata['zip_code'] = zcode;
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      Constant.showLog("===Get Results");
      Constant.showLog(response.body);
      Map d = json.decode(response.body);
      if (d['success'] == 1) {
        return outletFromJson(response.body);
      } else {
        Outlet outlet = new Outlet();
        outlet.success = d['success'];
        outlet.message = d['message'];
        return outlet;
      }
    } else {
      Outlet outlet = new Outlet();
      outlet.success = Constant.api_no_net;
      outlet.message = Constant.api_no_net_msg;
      return outlet;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Outlet outlet = new Outlet();
    outlet.success = Constant.api_catch_error;
    outlet.message = Constant.api_catch_error_msg;
    return outlet;
  }
}

Future<Outlet> getQROutletDetail(String str) async {
  try {
    String url = Constant.base_url + "get_outlet_details_using_kiosk_str";
    Constant.showLog('get outlet detail');
    Constant.showLog(url);
    Map reqdata = {'kiosk_str': str};
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
        Uri.parse(url),
        body: reqdata,
      );
      Constant.showLog("===Get Results");
      Map d = json.decode(response.body);
      Constant.showLog(json.encode(d));
      if (d['success'] == 1) {
        return outletFromJson(response.body);
      } else {
        Outlet outlet = new Outlet();
        outlet.success = d['success'];
        outlet.message = d['message'];
        return outlet;
      }
    } else {
      Outlet outlet = new Outlet();
      outlet.success = Constant.api_no_net;
      outlet.message = Constant.api_no_net_msg;
      return outlet;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Outlet outlet = new Outlet();
    outlet.success = Constant.api_catch_error;
    outlet.message = Constant.api_catch_error_msg;
    return outlet;
  }
}

Future<Map> getAds() async {
  try {
    String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
    String? runid =
        await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
    String? brandid = await KeyConstant.retriveString(KeyConstant.key_brand_id);
    String url = Constant.base_url + "get_ads";
    Constant.showLog('---Get Ads---');
    Constant.showLog(url);
    Constant.showLog("brandid:" + brandid!);
    Map reqdata = {
      'rest_unique_id': runid,
      'restaurant_id': rid,
    };
    bool conn = await isConnection();
    if (conn) {
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      Constant.showLog("Get slide show Imgs");
      Constant.showLog(response.body);
      List list = json.decode(response.body);
      if (list == null) list = [];
      return {'success': 1, 'data': list};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> fetchFeaturedItems() async {
  try {
    Constant.showLog("Get Feature Items");
    String url = Constant.base_url + "get_recommended_dishes";
    Constant.showLog(url);
    String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
    String? runid =
        await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
    String? brandid = await KeyConstant.retriveString(KeyConstant.key_brand_id);
    Constant.showLog("brandid:" + brandid!);
    Map reqdata = {
      'rest_unique_id': runid,
      'restaurant_id': rid,
    };
    bool conn = await isConnection();
    if (conn) {
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      List list = json.decode(response.body);
      if (list == null) list = [];
      Constant.showLog("Feature Items");
      Constant.showLog(list);
      return {'success': 1, 'data': list};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getAllCuisine() async {
  try {
    Constant.showLog('inside fetch all cuisine');
    String url = Constant.base_url + "get_cuisine_by_outlet";
    Constant.showLog(url);
    String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
    String? runid = await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
    String? brandid = await KeyConstant.retriveString(KeyConstant.key_brand_id);
    Map reqdata = {
      'rest_unique_id': runid,
      'restaurant_id': rid,
    };
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqdata,
          headers: {"brand-id":brandid!}
      );
      List list = json.decode(response.body);
      if (list == null) list = [];
      Constant.showLog(list);
      return {'success': 1, 'data': list};
    } else {
      return  {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getAllItems() async {
  try {
    Constant.showLog('inside fetch all items');
    String url = Constant.base_url + "get_all_dish_by_outlet";
    Constant.showLog(url);
    String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
    String? runid =
        await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
    String? brandid = await KeyConstant.retriveString(KeyConstant.key_brand_id);
    Constant.showLog("brandid:" + brandid!);
    Map reqdata = {
      'rest_unique_id': runid,
      'restaurant_id': rid,
    };
    bool conn = await isConnection();
    if (conn) {
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      List list = json.decode(response.body);
      if (list == null) list = [];
      return {'success': 1, 'data': list};
    } else {
      return {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getItemsByCusine(String cuisineId) async {
  try {
    Constant.showLog('inside fetch items by cuisine');
    String url = Constant.base_url + "get_dish_by_cuisine_by_outlet";
    Constant.showLog(url);
    String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
    String? runid = await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
    String? brandid = await KeyConstant.retriveString(KeyConstant.key_brand_id);
    Map reqdata = {
      'rest_unique_id': runid,
      'restaurant_id': rid,
      'cuisine_id':cuisineId
    };
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqdata,
          headers: {"brand-id": brandid!}
      );
      Constant.showLog(response.body);
      List list = json.decode(response.body);
      if (list == null) list =[];
      return {'success': 1, 'data': list};
    } else {
      return {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> fetchOutletMenu() async {
  try {
    bool conn = await isConnection();
    if (conn) {
      Constant.showLog('inside fetch outlet Menuu');
      String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
      String? runid =
          await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
      String? brandid =
          await KeyConstant.retriveString(KeyConstant.key_brand_id);
      String? url = Constant.base_url +
          "get_dish_by_cuisine_and_recommended_dishes"; //"get_dish_by_cuisine";//
      Constant.showLog(url);
      Constant.showLog("brandid:" + brandid!);
      Map reqdata = {
        'rest_unique_id': runid,
        'restaurant_id': rid,
      };
      Constant.showLog(reqdata);
      Constant.showLog('brand-id:' + brandid);
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      List list = json.decode(response.body);
      if (list == null) list = [];
      Constant.showLog("Outlet Menu");
      Constant.showLog(list);
      return {'success': 1, 'data': list};
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getItemDetail(
    String rest_id, String runiqid, String item_id, String brandid) async {
  try {
    Constant.showLog('===Get Dish Detail===');
    String url = Constant.base_url + "get_dish_detail";
    Constant.showLog(url);
    Constant.showLog("brandid:" + brandid);
    Map reqdata = {
      'rest_unique_id': runiqid,
      'restaurant_id': rest_id,
      'dish_id': item_id
    };
    bool conn = await isConnection();
    if (conn) {
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      Constant.showLog("===Get Order Detail Result===");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getInfo() async {
  try {
    String url = Constant.base_url + "get_web_settings";
    Constant.showLog('Get Info');
    Constant.showLog(url);
    String? brandid = await KeyConstant.retriveString(KeyConstant.key_brand_id);
    Constant.showLog("brandid:" + brandid!);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(Uri.parse(url), headers: {"brand-id": brandid});
      Constant.showLog("===Get Results Info===");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> getOffers(String couponcode, String brandid) async {
  try {
    bool conn = await isConnection();
    if (conn) {
      Constant.showLog('get offers');
      String? rid = await KeyConstant.retriveString(KeyConstant.key_rest_id);
      String? runid =
          await KeyConstant.retriveString(KeyConstant.key_rest_uniq_id);
      String url = Constant.base_url + "get_bill_amt_offer_data";
      Constant.showLog(url);
      Map reqdata = {
        'rest_unique_id': runid,
        'restaurant_id': rid,
        'coupon_code': couponcode
      };
      Constant.showLog(reqdata);
      final response =
          await http.post(Uri.parse(url), body: reqdata, headers: {"brand-id": brandid});
      Constant.showLog("Offer list");
      Constant.showLog(response.body);
      try {
        List list = json.decode(response.body);
        if (list == null) list = [];
        return {'success': 1, 'data': list};
      } catch (e) {
        return json.decode(response.body);
      }
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<dynamic> searchLocation(String text) async {
  try {
    bool conn = await isConnection();
    if (conn) {
      Constant.showLog('search location');
      //String SEARCH_LOCATION_URI = '/api/v1/config/place-api-autocomplete';
      String url =Constant.base_url+'place_api_autocomplete?search_text=$text';
      Constant.showLog(url);
      //Constant.showLog(reqdata);
      final response = await http.get(Uri.parse(url));
      Constant.showLog("search location result");
      Constant.showLog(response.body);
      try {
        dynamic list = json.decode(response.body);

        return list;
      } catch (e) {
        return json.decode(response.body);
      }
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}
