import 'dart:convert';

import 'package:capto/apis/api_service.dart';
import 'package:capto/util/checkConnection.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:http/http.dart' as http;

Future<Map> createTblPin(
    String tblid, String restid, String runiqid,String brandid) async {
  try {
    Constant.showLog("===Generate table pin===");
    String url = Constant.base_url + "generate_table_pin";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'table_id': tblid,
      'restaurant_id': restid,
      'rest_unique_id': runiqid
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqdata,
          headers: {"brand-id": brandid}
      );
      Constant.showLog("generate table pin response");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> checkPin(
    String tblid, String restid, String runiqid,String brandid,String pin) async {
  try {
    Constant.showLog("===Check pin===");
    String url = Constant.base_url + "check_table_pin";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'table_id': tblid,
      'restaurant_id': restid,
      'rest_unique_id': runiqid,
      'pin':pin
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqdata,
          headers: {"brand-id": brandid}
      );
      Constant.showLog("Check pin response");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> fetchOrder(
    String tblid, String restid, String runiqid,String brandid) async {
  try {
    Constant.showLog("===Get table order detail===");
    String url = Constant.base_url + "get_table_current_order";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'table_id': tblid,
      'restaurant_id': restid,
      'rest_unique_id': runiqid
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqdata,
          headers: {"brand-id": brandid}
      );
      Constant.showLog("Get table order detail response");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> startOrder(
    String tblid, String restid, String runiqid,String brandid,String nnp) async {
  try {
    Constant.showLog("===Start Order===");
    String url = Constant.base_url + "capto_start_new_order";
    Constant.showLog(url);
    Constant.showLog("brandid:"+brandid);
    Map reqdata = {
      'table_id': tblid,
      'restaurant_id': restid,
      'rest_unique_id': runiqid,
      'no_of_people':nnp,
      'platform_type':'capto_app_web',
      'order_timestamp':DateTime.now().millisecondsSinceEpoch.toString(),
    };
    Constant.showLog(reqdata);
    bool conn = await isConnection();
    if (conn) {
      final response = await http.post(
          Uri.parse(url),
          body: reqdata,
          headers: {"brand-id": brandid}
      );
      Constant.showLog("Start order response");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<Map> updateOrder(Map reqdata,String brandid) async {
  try {
    bool conn = await isConnection();
    if (conn) {
      String url = Constant.base_url + "capto_finedine_update_order";
      Constant.showLog("===Update Order===");
      Constant.showLog(url);
      Constant.showLog("brandid:"+brandid);
      Constant.showLog(reqdata);
      final response = await http.post(
        Uri.parse(url),
        body: jsonEncode(reqdata),
        headers: {"brand-id": brandid,'Content-Type': 'application/json'},
      );
      Constant.showLog("===Update Order Response===");
      Constant.showLog(response.body);
      return json.decode(response.body);
    } else {
      Map res = {
        'success': Constant.api_no_net,
        'message': Constant.api_no_net_msg
      };
      return res;
    }
  } catch (e) {
    Constant.showLog("error");
    Constant.showLog(e);
    Map res = {
      'success': Constant.api_catch_error,
      'message': Constant.api_catch_error_msg
    };
    return res;
  }
}

Future<dynamic> getCategoryItems()async{
  String apiName="get_cuisine_by_dish_array";
  String  restId=Constant.shopData!.id.toString();
  String runiqId=Constant.shopData!.restUniqueId!;
  Map req={
    'restaurant_id':restId,
    'rest_unique_id':runiqId
  };
  dynamic res= await apiPostHeaderService(apiName,req);
  return res;
}