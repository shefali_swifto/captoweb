import 'dart:async';
import 'dart:convert';

import 'package:capto/localdb/order.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/VariationPodo.dart';
import 'package:capto/util/constant.dart';
import 'package:hive/hive.dart';

class DBProvider {
  static final String DBName = "DBPOSCapto.db";
  static final int DBVersion = 1;
  static final String TBL = "tblorder";
  static final String KEY_ID = "id";
  static final String KEY_DISH_ID = "dish_id";
  static final String KEY_NAME = "dnm";
  static final String KEY_DIMG = "dimg";
  static final String KEY_DISHTYPE = "type";
  static final String KEY_QTY = "qty";
  static final String KEY_PRICE_PER_DISH = "priceperdish";
  static final String KEY_PRICE = "price";
  static final String KEY_DIS = "discount";
  static final String KEY_COMMENT = "dishcmt";

  static final String KEY_SOLDBY = "sold_by";
  static final String KEY_TOTTAX = "tot_tax";
  static final String KEY_TAXAMT = "tax_amt";

  static final String KEY_WT = "price_without_tax";
  static final String KEY_PER_WT = "priceper_without_tax";
  static final String KEY_TAX = "taxdata";
  static final String KEY_OFFER = "offer";
  static final String KEY_TOT_DIS = "disamount";
  static final String KEY_PRE_ID = "pre_id";
  static final String KEY_PRE_FLAG = "pre_flag";
  static final String KEY_VAR_ARRAY = "var_array";
  static final String KEY_TIME = "add_time";

  static final String KEY_BRAND_ID = "brand_id";
  static final String KEY_REST_ID = "rest_id";
  static final String KEY_REST_NAME = "rest_name";
  static final String KEY_REST_UNIQ_ID = "rest_uniq_id";
  static final String KEY_ADDRESS = "rest_address";
  static final String KEY_IMG = "rest_img";

  //Add To cart
  Future<int> addOrderDetail(DishOrderPodo data) async {
    List jtax = [];
    if (data.tax_data != null && data.tax_data!.isNotEmpty) {
      for (Map t in data.tax_data!) {
        Map j = {
          "id": t['id'],
          "text": t['text'],
          "value": t['value'],
          "tax_amount": t['tax_amount']
        };
        jtax.add(j);
      }
    }
    List jVar = [];
    if (data.varPojoList != null && data.varPojoList!.isNotEmpty) {
      for (VariationPodo p in data.varPojoList!) {
        Map preObj = {
          "id": p.id,
          "name": p.name,
          "amount": p.amount,
          "amount_wt": p.amount_wt,
          "quantity":p.quantity
        };

        jVar.add(preObj);
      }
    }
    String tot_disc = "0";
    if (data.tot_disc != null && data.tot_disc!.isNotEmpty) {
      tot_disc = data.tot_disc!;
    } else
      tot_disc = "0";
    String preid = "0", var_array = "", tax_array = "";
    if (jVar != null && jVar.isNotEmpty) {
      preid = data.preid!;
      var_array = json.encode(jVar);
    }
    if (jtax != null && jtax.isNotEmpty) tax_array = json.encode(jtax);
    Order orderData = Order(
        id: DateTime.now().microsecond.toString(),
        dish_id: data.dishid!,
        dnm: data.dishname,
        dimg: data.dishimage,
        qty: data.qty,
        priceperdish: data.priceperdish,
        price: data.price,
        price_without_tax: data.price_without_tax,
        priceper_without_tax: data.priceper_without_tax,
        pre_flag: data.preflag,
        pre_id: preid,
        var_array: var_array,
        discount: data.discount,
        dishcmt: data.dish_comment,
        type: data.dishtype,
        sold_by: data.sold_by,
        tot_tax: data.tot_tax,
        tax_amt: data.tax_amt,
        taxdata: tax_array,
        offer: data.offer,
        disamount: tot_disc,
        add_time: DateTime.now().toString(),
        brand_id: data.brandid,
        rest_id: data.rest_id,
        rest_name: data.rest_name,
        rest_uniq_id: data.rest_uniq_id,
        rest_address: data.rest_address,
        rest_img: data.rest_img);
//     Constant.showLog('add order');
// Constant.showLog(orderData.rest_id.toString()+" "+orderData.brand_id.toString());
    await Hive.box(TBL).add(orderData);

    return int.parse(orderData.id.toString());
  }

  updateItem(int index, DishOrderPodo data) async {
    List jVar = [];
    if (data.varPojoList != null && data.varPojoList!.isNotEmpty) {
      for (VariationPodo p in data.varPojoList!) {
        Map preObj = {
          "id": p.id,
          "name": p.name,
          "amount": p.amount,
          "amount_wt": p.amount_wt,
          "quantity":p.quantity
        };

        jVar.add(preObj);
      }
    }
    String preid = "0", var_array = "";
    if (jVar != null && jVar.isNotEmpty) {
      preid = data.preid!;
      var_array = json.encode(jVar);
    }

    Order o = await Hive.box(TBL).getAt(index);
    o.var_array = var_array;
    o.pre_id = preid;
    o.qty = data.qty;
    o.price = data.price;
    o.price_without_tax = data.price_without_tax;
    await Hive.box(TBL).putAt(index, o);
  }

  Future<int> getCount() async {
    return await Hive.box(TBL).values.length;
  }

  Future<int> getNoOcurr(String dishid) async {
    return Constant.dishorederlist.where((element) => element.dishid==dishid).toList().length;
  }

  Future updateQty(int index, String qty, String id) async {
    Order o = await Hive.box(TBL).getAt(index);
    o.qty = qty;
    await Hive.box(TBL).putAt(index, o);
  }

  removeItemById(int index, String id) async {
    await Hive.box(TBL).deleteAt(index);
  }

  Future getOrderDetail() async {
    // Constant.showLog("------------");
    List<DishOrderPodo> dishorederlist11 = [];
    List<String> selidlist11 = [];
    final res = await Hive.box(TBL);
    res.watch();
  //  Constant.showLog(res.length);
    if (res.length > 0) {
      Constant.selidlist.clear();
      Constant.dishorederlist.clear();
      for (int i = 0; i < res.length; i++) {
        DishOrderPodo podo =  DishOrderPodo();
        Order m = res.getAt(i);
        podo.dishname = m.dnm;
        podo.dishid = m.dish_id;
        podo.dishimage = m.dimg;
        podo.dishtype = m.type;

        podo.price = m.price;
        podo.priceperdish = m.priceperdish;
        podo.price_without_tax = m.price_without_tax;
        podo.priceper_without_tax = m.priceper_without_tax;
        podo.dish_comment = m.dishcmt;
        podo.orderdetailid = m.id;
        podo.discount = m.discount;
        podo.qty = m.qty;
        podo.preid = m.pre_id;
        podo.preflag = m.pre_flag;
        String? var_array = m.var_array;
        if (var_array != null && var_array.isNotEmpty) {
          List vars = json.decode(var_array);
          List<VariationPodo> vlist = [];
          for (Map v in vars) {
            VariationPodo vp = new VariationPodo();
            vp.id = v['id'].toString();
            vp.name = v['name'].toString();
            vp.amount = v['amount'].toString();
            vp.amount_wt = v['amount_wt'].toString();
            vp.quantity=v['quantity'];
            vlist.add(vp);
          }
          podo.varPojoList = vlist;
        }
        podo.sold_by = m.sold_by;
        podo.tax_amt = m.tax_amt;
        podo.tot_tax = m.tot_tax;
        podo.offer = m.offer;
        podo.tot_disc = m.disamount;
        String? tax_array = m.taxdata;
        if (tax_array != null && tax_array.isNotEmpty) {
          podo.tax_data = json.decode(tax_array);
        }
        podo.rest_img = m.rest_img;
        podo.rest_address = m.rest_address;
        podo.rest_uniq_id = m.rest_uniq_id;
        podo.rest_name = m.rest_name;
        podo.rest_id = m.rest_id;
        podo.brandid = m.brand_id;

        dishorederlist11.add(podo);
        selidlist11.add(podo.dishid!);
      }
    }
     Constant.dishorederlist=List.from(dishorederlist11);
     Constant.selidlist=List.from(selidlist11);
   // Constant.showLog("------------"+dishorederlist11.length.toString());
  }

  deleteAll() async {
    await Hive.box(TBL).clear();
  }
}
