import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SwiftoTxtField extends StatelessWidget {
  TextEditingController _controller;
  String? hintTxt, prefixTxt;
  TextInputAction txtAction;
  TextInputType txtInputType;
  bool isMultiline;
  int? maxLen;

  SwiftoTxtField(this._controller, this.hintTxt, this.txtAction,
      this.txtInputType, this.isMultiline, this.maxLen, this.prefixTxt);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 10),
        child: TextField(
          keyboardType: txtInputType,
          textInputAction: txtAction,
          decoration: InputDecoration(
              focusedBorder: new UnderlineInputBorder(
                  borderSide:
                      new BorderSide(color: Theme.of(context).colorScheme.secondary)),
              prefixText: (prefixTxt!=null)?prefixTxt!+" ":prefixTxt,
              labelText: hintTxt,
              labelStyle: TextStyle(color: Theme.of(context).hintColor),
              counterText: ""),
          controller: _controller,
          minLines: 1,
          maxLines: (isMultiline) ? 5 : 1,
          maxLength: maxLen,
        ));
  }
}
