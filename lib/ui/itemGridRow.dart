import 'package:capto/ui/swiftoQty.dart';
import 'package:capto/util/constant.dart';
import 'package:flutter/material.dart';

class GridProduct extends StatelessWidget {
  int? decimal_val;
  Map? data;
  String? restid, isPriceWT,outletStatus;
  final Function? clickAdd, clickInc, clickDec,clickView;

  GridProduct(
      {Key? key,
        required this.outletStatus,
      required this.restid,
      required this.isPriceWT,
      required this.data,
      required this.clickAdd,
      required this.clickInc,
      required this.clickDec,
        required this.clickView})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Constant.decimal_value != null && Constant.decimal_value.isNotEmpty)
      decimal_val = int.parse(Constant.decimal_value);
    else
      decimal_val = 2;
    String img = "";
    if (data!.containsKey("dishimage_url")) {
      img = data!['dishimage_url'];
    } else if (data!.containsKey("img")) img = data!['img'];
    String price = "";
    if (data!.containsKey("price"))
      price = data!['price'];
    else
      price = '0';
    if (isPriceWT == 'true' && data!.containsKey('price_without_tax'))
      price = data!['price_without_tax'];
    String nm = "";
    if (data!.containsKey("dishname"))
      nm = data!['dishname'];
    else if (data!.containsKey("name")) nm = data!['name'];
    String qty = "0", dish_type = "", dtimg = "";
    if (data!.containsKey("type")) {
      dish_type = data!['type'];
      if (dish_type == 'Veg')
        dtimg = "assets/veg.png";
      else if (dish_type == 'NonVeg') dtimg = "assets/non_veg.png";
    }
    bool diffoutlet = false;
    if (Constant.selidlist != null && Constant.selidlist.length > 0) {
      if (Constant.dishorederlist[0].rest_id != restid) diffoutlet = true;
      if (!diffoutlet &&
          Constant.selidlist.contains(data!['dishid'].toString())) {
        Constant.dishorederlist.forEach((data1) {
          if (data1.dishid == data!['dishid'].toString()) {
            int q = int.parse(data1.qty!) + int.parse(qty);
            qty = q.toString();
          }
        });
      }
    }
    if (qty == "0") qty = "1";
    return Container(
      child: InkWell(
        child: ListView(
          shrinkWrap: true,
          primary: false,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
              //color: Colors.pink[200],
              padding: EdgeInsets.only(top: 5, bottom: 5),
              child: Text(
                nm.trim(),
                style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w600,
                ),
                maxLines: 2,
              ),
            ),
            Container(
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        Constant.currency +
                            double.parse(price).toStringAsFixed(decimal_val!),
                        style: TextStyle(
                          fontSize: 13.0,
                        ),
                      ),
                      flex: 1,
                    ),
                    (outletStatus!=null && outletStatus=='close')?SizedBox():SwiftoQty(
                        true,
                        (!diffoutlet &&
                            Constant.selidlist != null &&
                            Constant.selidlist
                                .contains(data!['dishid'].toString())),
                        qty,
                        clickInc,
                        clickDec,
                        clickAdd),
                  ]),
            )
          ],
        ),
        onTap: ()=>clickView!(),
      ),
    );
  }
}
