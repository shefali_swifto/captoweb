import 'package:capto/models/app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IconBadge extends StatefulWidget {
  Color? clr;

  IconBadge({Key? key, this.clr}) : super(key: key);

  @override
  _IconBadgeState createState() => _IconBadgeState();
}

class _IconBadgeState extends State<IconBadge> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 3,right: 5),
            child: Image.asset('assets/ic_cart.png', color: widget.clr,height: 20,)),
        Positioned(
          right: 0.0,
          top: 0,
          child: Container(
              padding: const EdgeInsets.all(1),
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(6),
              ),
              constraints: const BoxConstraints(
                minWidth: 13,
                minHeight: 13,
              ),
              child: Text(
                context.watch<AppModel>().value.toString(),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 8,
                ),
                textAlign: TextAlign.center,
              )),
        ),
      ],
    );
  }
}
