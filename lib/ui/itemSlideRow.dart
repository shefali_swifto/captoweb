
import 'package:capto/util/constant.dart';
import 'package:flutter/material.dart';

class SliderItem extends StatelessWidget {

  final String img;

  SliderItem(
      {Key? key,
      required this.img,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
   // Constant.showLog(img);
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 1.0,
      child: Container(
        height: MediaQuery.of(context).size.height / 3.2,
        width: MediaQuery.of(context).size.width,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5.0),
          child:
         Image.network(
           "$img",
           fit: BoxFit.cover,
         ),
          // CachedNetworkImage(
          //   fit: BoxFit.cover,
          //   imageUrl: "$img",
          //   placeholder: (context, url) => Image.asset(
          //     'assets/loading.gif',
          //     fit: BoxFit.cover,
          //   ),
          // ),
        ),
      ),
    );
  }
}
