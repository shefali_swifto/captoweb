import 'package:capto/ui/swiftoQty.dart';
import 'package:capto/util/constant.dart';
import 'package:flutter/material.dart';
import 'package:image_network/image_network.dart';

class ListProduct extends StatefulWidget {
  int? decimal_val = 2;
  Map? data;
  String? restid, isPriceWT, outletStatus;
  bool isLast = false, isCaptoWeb = true;
  final Function? clickAdd, clickInc, clickDec, clickView;

  ListProduct({
    Key? key,
    required this.outletStatus,
    required this.restid,
    required this.isPriceWT,
    required this.decimal_val,
    required this.data,
    required this.clickAdd,
    required this.clickInc,
    required this.clickDec,
    required this.clickView,
    required this.isLast,
    this.isCaptoWeb= true,
  }) : super(key: key);

  @override
  _ListProductState createState() => _ListProductState();
}

//
class _ListProductState extends State<ListProduct> {
  bool isExpanded = false;

  void toggleExpanded() {
    setState(() {
      isExpanded = !isExpanded;
    });
  }

  @override
  Widget build(BuildContext context) {
    String price = "", pricewt = "";
    price = widget.data!['price'];
    pricewt = widget.data!['price_without_tax'];
    String nm = "";
    if (widget.data!.containsKey("dishname")) nm = widget.data!['dishname'];
    String qty = "0", dishType = "", img = "";
    if (widget.data!.containsKey("type")) {
      dishType = widget.data!['type'];
      if (dishType == 'Veg')
        img = "assets/veg.png";
      else if (dishType == 'NonVeg') img = "assets/non_veg.png";
    }
    String dimg = "";
    if (widget.data!.containsKey("dishimage_url")) {
      dimg = widget.data!['dishimage_url'];
    }
    bool diffoutlet = false;
    if (Constant.selidlist != null && Constant.selidlist.length > 0) {
      if (Constant.dishorederlist[0].rest_id != widget.restid)
        diffoutlet = true;
      if (!diffoutlet &&
          Constant.selidlist.contains(widget.data!['dishid'].toString())) {
        Constant.dishorederlist.forEach((data1) {
          if (data1.dishid == widget.data!['dishid'].toString()) {
            int q = int.parse(data1.qty!) + int.parse(qty);
            qty = q.toString();
          }
        });
      }
    }

    if (qty == "0") qty = "1";
    bool isAdded = (!diffoutlet &&
        Constant.selidlist != null &&
        Constant.selidlist.contains(widget.data!['dishid'].toString()));
    int addedQty = 0;
    if (!widget.isCaptoWeb && Constant.placeitemlist.isNotEmpty) {
      addedQty = Constant.placeitemlist
          .map<int>((m) => (m.dishid == widget.data!['dishid'].toString())
              ? int.parse(m.qty!)
              : 0)
          .reduce((a, b) => a + b);
    }

    return
      // InkWell(
      // onTap: () => widget.clickView!(),
      // child:
      Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 2, bottom: 2, right: 1),
            padding: const EdgeInsets.only(top: 6, bottom: 6),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                nm,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                (Constant.shopData != null &&
                                        Constant.shopData!
                                                .showItemPriceWithoutTax !=
                                            null &&
                                        Constant.shopData!
                                                .showItemPriceWithoutTax ==
                                            'true')
                                    ? Constant.currency +
                                        double.parse(pricewt).toStringAsFixed(
                                            widget.decimal_val!)
                                    : Constant.currency +
                                        double.parse(price).toStringAsFixed(
                                            widget.decimal_val!),
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                ),
                              ),
                              const SizedBox(
                                height: 3,
                              ),
                              if (widget.data!.containsKey('description'))
                                buildDescriptionSection(context, widget.data)
                              else
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 0.77,
                                )
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      if (dimg.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              if (addedQty > 0)
                                Text('Placed Qty:$addedQty'),
                              Stack(
                                alignment: Alignment.topCenter,
                                children: <Widget>[
                                  ImageNetwork(
                                    image: dimg,
                                    height: 130,
                                    width: 137,
                                    duration: 1500,
                                    curve: Curves.easeIn,
                                    onPointer: true,
                                    debugPrint: false,
                                    fullScreen: false,
                                    fitAndroidIos: BoxFit.cover,
                                    fitWeb: BoxFitWeb.cover,
                                    borderRadius: BorderRadius.circular(8),
                                    onLoading: const CircularProgressIndicator(
                                      color: Colors.indigoAccent,
                                    ),
                                    onError: Image.asset(
                                      'assets/ic_error.png',
                                      height: 30,
                                      color: Colors.red,
                                    ),
                                    onTap: () {
                                     // debugPrint("©gabriel_patrick_souza");
                                    },
                                  ),
                                  Container(
                                    height: 144,
                                  ),
                                  (widget.outletStatus != null &&
                                          widget.outletStatus == 'close')
                                      ? const SizedBox()
                                      : Positioned(
                                          bottom: 0,
                                          child: SwiftoQty(
                                            true,
                                            isAdded,
                                            qty,
                                            widget.clickInc,
                                            widget.clickDec,
                                            widget.clickAdd,
                                          ),
                                        ),
                                ],
                              ),
                              if (widget.data!['preflag']
                                  .toString()
                                  .toLowerCase() ==
                                  'true' &&
                                  !isAdded)
                                const Text(
                                  '*customizable',
                                  style: TextStyle(fontSize: 12),
                                ),
                            ],
                          ),
                        )
                      else
                        Container(
                          height: 90,
                          alignment: Alignment.center,
                          padding: const EdgeInsets.only(
                            right: 20,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                width: 10,
                              ),
                              if (addedQty > 0)
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 8,
                                  ),
                                  child: Text(
                                    'Placed Qty:${addedQty}',
                                  ),
                                ),
                              (widget.outletStatus != null &&
                                      widget.outletStatus == 'close')
                                  ? const SizedBox()
                                  : SwiftoQty(
                                    true,
                                    isAdded,
                                    qty,
                                    widget.clickInc,
                                    widget.clickDec,
                                    widget.clickAdd,
                                  ),
                              if (widget.data!['preflag']
                                          .toString()
                                          .toLowerCase() ==
                                      'true' &&
                                  !isAdded)
                                const Padding(
                                  padding: EdgeInsets.only(top: 4, bottom: 4),
                                  child: Text(
                                    '*customizable',
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                            ],
                          ),
                        ),
                    ],
                  ),
                  flex: 1,
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
          if (!widget.isLast)
            const Divider(
              height: 1,
            )
        ],
      );
    //)
  }

  Widget buildDescriptionSection(BuildContext context, Map? data) {
    String description = data!['description'].toString();
    bool isLongDescription = description.trim().length > 0;
    return isLongDescription
        ? Container(
            width: MediaQuery.of(context).size.width / 0.77,
            margin: const EdgeInsets.only(top: 5),
            child: isExpanded
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(description),
                      GestureDetector(
                        onTap: toggleExpanded,
                        child: const Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: Text(
                            'Show less...',
                            style: TextStyle(
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        description,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      GestureDetector(
                        onTap: toggleExpanded,
                        child: const Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: Text(
                            'Show more...',
                            style: TextStyle(
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
          )
        : SizedBox(
            width: MediaQuery.of(context).size.width / 0.77,
          );
  }
}
