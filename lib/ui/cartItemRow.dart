
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/VariationPodo.dart';
import 'package:capto/ui/swiftoQty.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';

class CartItems extends StatelessWidget {
  DishOrderPodo data;
  final Function clickInc, clickDec, clickRemove, clickUpdate;
  bool isEditable;
  String currency;

  CartItems(
      {Key? key,
        required this.currency,
      required this.data,
      required this.clickInc,
      required this.clickDec,
      required this.clickRemove,
      required this.clickUpdate,
      required this.isEditable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int dval = 2;
    if (Constant.decimal_value != null && Constant.decimal_value.isNotEmpty)
      dval = int.parse(Constant.decimal_value);
    String price = data.price!,pricewt=data.price_without_tax!;
    if (Constant.shopData != null &&
        Constant.shopData!.showItemPriceWithoutTax != null &&
        Constant.shopData!.showItemPriceWithoutTax == 'true')
      price = data.price_without_tax!;
    String nm = data.dishname!;
    String qty = data.qty!;

    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: InkWell(
        onTap: ()=>clickUpdate(),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                (isEditable)
                    ? Container(
                  margin: const EdgeInsets.only(right: 7),
                      child: InkWell(
                          onTap: ()=>clickRemove(),
                          child:  CircleAvatar(
                              backgroundColor:defaultClr,
                              child: Padding(
                                padding: const EdgeInsets.all(1.0),
                                child: CircleAvatar(
                                  backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                                  child: Image.asset('assets/ic_minus.png',color: defaultClr,width: 15,),
                                ),
                              )),
                        ),
                    )
                    : const SizedBox(
                        height: 0,
                        width: 30,
                      ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        nm,
                        style: const TextStyle(
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      const SizedBox(height: 5.0),
                      Text(
                        currency + double.parse(price).toStringAsFixed(dval),
                        style: const TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.w900,
                          color: defaultClr,
                        ),
                      ),
                      if(data.varPojoList!=null && data.varPojoList!.isNotEmpty)
                        Container(
                          margin: const EdgeInsets.only(top: 5),
                          child: ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: data.varPojoList!.length,
                              itemBuilder: (BuildContext context,int index){
                                VariationPodo vp=data.varPojoList![index];
                                return Text(vp.name!+" - "+currency+double.parse(vp.amount!).toStringAsFixed(dval));
                              }),
                        ),
                    ],
                  ),
                  flex: 1,
                ),
                SwiftoQty(isEditable,true,qty,clickInc,clickDec,null),
              ],
            ),
            const Divider()
          ],
        ),
      ),
    );

  }
}
