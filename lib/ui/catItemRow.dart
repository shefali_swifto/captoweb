import 'package:capto/DBOrder.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/screens/dialogLayout/variationDialog.dart';
import 'package:capto/ui/itemListRow.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class CategoryItemRow extends StatefulWidget {
  Map om;
  Outlet currentOutlet;
  Function? cartClick, refreshClick;
  bool isCaptoWeb = false;

  CategoryItemRow({
    Key? key,
    required this.om,
    required this.currentOutlet,
    required this.cartClick,
    required this.refreshClick,
    this.isCaptoWeb= false,
  }) : super(key: key);

  @override
  _CategoryItemRowState createState() => _CategoryItemRowState();
}

class _CategoryItemRowState extends State<CategoryItemRow> {

  void addClick(Map featuredItems) {
    if (Constant.selidlist != null && Constant.selidlist.length > 0) {
      if (Constant.dishorederlist[0].rest_id ==
              widget.currentOutlet.id.toString() &&
          Constant.dishorederlist[0].brandid == widget.currentOutlet.brand_id) {
        _checkVariation(featuredItems);
      } else {
        _showAlert(featuredItems);
      }
    } else {
      _checkVariation(featuredItems);
    }
  }

  Future incClick(Map featuredItems) async {
    if (Constant.selidlist != null) {
      if (featuredItems.containsKey("preflag") &&
          featuredItems['preflag'] == 'true') {
        dynamic r = showDialog(
          context: context,
          builder: (_) => DialogVariation(
            item: featuredItems,
            currentOutlet: widget.currentOutlet,
            isUpdate: false,
            updatePOS: 0,
          ),
        );
        if (r != null && r is bool) {
          if (r) {
            Provider.of<AppModel>(context, listen: false).increment();
            widget.refreshClick!();
            if (mounted) {
              setState(() {});
            }
          }
        }
      } else {
        int pos =
            Constant.selidlist.indexOf(featuredItems['dishid'].toString());
        DishOrderPodo d = Constant.dishorederlist[pos];
        int q = int.parse(d.qty.toString()) + 1;
        d.qty = q.toString();
        await DBProvider().updateQty(pos, d.qty!, d.orderdetailid!);
        Constant.dishorederlist[pos] = d;
        Provider.of<AppModel>(context, listen: false).increment();
        widget.refreshClick!();
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  Future decClick(Map featuredItems) async {
    if (Constant.selidlist != null) {
      if (featuredItems.containsKey("preflag") &&
          featuredItems['preflag'] == 'true') {
        CustomUI.customAlert(
            "",
            "This item has multiple customizations added. Proceed to cart to remove item",
            context);
      } else {
        int pos =
            Constant.selidlist.indexOf(featuredItems['dishid'].toString());
        DishOrderPodo d = Constant.dishorederlist[pos];
        int q = int.parse(d.qty.toString()) - 1;
        if (q != 0) {
          d.qty = q.toString();
          await DBProvider().updateQty(pos, d.qty!, d.orderdetailid!);
          Constant.dishorederlist[pos] = d;
        } else {
          await DBProvider().removeItemById(pos, d.orderdetailid!);
          Constant.dishorederlist.removeAt(pos);
          Constant.selidlist.removeAt(pos);
        }
        Provider.of<AppModel>(context, listen: false).increment();
        widget.refreshClick!();
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    int ccnt = 0;
    List dishList = [];
    if (widget.om.containsKey('dish_data')) {
      ccnt = widget.om['dish_data'].length;
      dishList = widget.om['dish_data'];
    }
    return (ccnt > 0)
        ? ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: dishList.length,
            itemBuilder: (BuildContext context, int index) {
              Map featuredItems = dishList[index];
              return (featuredItems['sold_by'] == 'weight')
                  ? const SizedBox()
                  : Column(
                      children: [
                        ListProduct(
                            outletStatus: widget
                                .currentOutlet.outletCurrentStatus!,
                            restid: widget.currentOutlet.id.toString(),
                            isPriceWT: widget
                                .currentOutlet.showItemPriceWithoutTax!,
                            decimal_val: int.parse(
                                widget.currentOutlet.decimalValue!),
                            data: featuredItems,
                            clickAdd: () => addClick(featuredItems),
                            clickInc: () => incClick(featuredItems),
                            clickDec: () => decClick(featuredItems),
                            clickView: () async {
                              if (featuredItems['preflag'].toString() ==
                                  'false') {
                                dynamic r = await Get.toNamed(
                                  RouteHelper.getItemDetailRoute(
                                    {
                                      'item': featuredItems,
                                      'isPriceWT': widget.currentOutlet
                                          .showItemPriceWithoutTax!,
                                      'restid': widget.currentOutlet.id
                                          .toString(),
                                      'outletStatus': widget
                                          .currentOutlet
                                          .outletCurrentStatus!,
                                      'currentOutlet':
                                          widget.currentOutlet.toJson(),
                                    },
                                  ),
                                );
                                if (r != null && r == 'cartTab')
                                  widget.cartClick!();
                              } else {
                                _checkVariation(featuredItems);
                              }
                            },
                            isLast: (ccnt - 1 == index),
                            isCaptoWeb: widget.isCaptoWeb)
                      ],
                    );
            },
          )
        : const SizedBox();
  }

  _checkVariation(Map featuredItems) async {
    if (featuredItems.containsKey("preflag") &&
        featuredItems['preflag'] == 'true') {
      bool d_result = await openBottomSheetDialog(context, DialogVariation(
              item: featuredItems,
              currentOutlet: widget.currentOutlet,
              isUpdate: false,
              updatePOS: 0,
            ));
      // showDialog(
      //   context: context,
      //   builder: (_) => DialogVariation(
      //     item: featuredItems,
      //     currentOutlet: widget.currentOutlet,
      //     isUpdate: false,
      //     updatePOS: 0,
      //   ),
      // );

      if (d_result) {
        Provider.of<AppModel>(context, listen: false).increment();
        widget.refreshClick!();
        if (mounted) {
          setState(() {});
        }
      }
    } else {
      addItemToCart(featuredItems);
    }
  }

  Future addItemToCart(Map featuredItems) async {
    DishOrderPodo d = new DishOrderPodo();
    d.dishid = featuredItems['dishid'].toString();
    d.cusineid = featuredItems['cusineid'].toString();
    d.dishname = featuredItems['dishname'];
    d.dishtype = featuredItems['type'];
    d.description = featuredItems['description'];
    d.price = featuredItems['price'].toString();
    d.priceperdish = featuredItems['price'].toString();
    d.dishimage = featuredItems['dishimage_url'];
    d.preflag = featuredItems['preflag'].toString();
    d.sold_by = featuredItems['sold_by'].toString();
    d.priceper_without_tax = featuredItems['price_without_tax'].toString();
    d.price_without_tax = featuredItems['price_without_tax'].toString();
    d.tax_amt = featuredItems['tax_amt'].toString();
    d.offer = featuredItems['offers'].toString();
    d.discount = featuredItems['discount_amount'].toString();
    d.combo_flag = featuredItems['combo_flag'].toString();
    d.qty = "1";
    d.brandid = Constant.shopData!.brand_id.toString();
    d.rest_id = Constant.shopData!.id.toString();
    d.rest_uniq_id = Constant.shopData!.restUniqueId.toString();
    d.rest_name = Constant.shopData!.name.toString();
    d.rest_img = Constant.shopData!.logo.toString();
    d.rest_address = Constant.shopData!.address.toString();
    List tax_list = [];
    if (featuredItems.containsKey('tax_data')) {
      tax_list = featuredItems['tax_data'];
      if (tax_list != null && tax_list.length > 0) {
        double tot_tax = tax_list
            .map<double>((m) => double.parse(m['value']))
            .reduce((a, b) => a + b);
        d.tot_tax = tot_tax.toString();
      } else {
        d.tot_tax = '0';
      }
    } else {
      d.tot_tax = '0';
    }
    d.tax_data = tax_list;
    d.pre = featuredItems['pre'];
    d.preid = "";
    d.dish_comment = "";

    int r = await DBProvider().addOrderDetail(d);
    d.orderdetailid = r.toString();
    Constant.selidlist.add(featuredItems['dishid'].toString());
    Constant.dishorederlist.add(d);
    Provider.of<AppModel>(context, listen: false).increment();
    widget.refreshClick!();
    if (mounted) {
      setState(() {});
    }
  }

  void _showAlert(Map featuredItems) {
    CustomUI.customAlertDialog(
        "Remove Item?",
        "Your cart contains items from " +
            Constant.dishorederlist[0].rest_name! +
            ". Do you want to discard the " +
            "selection and add items from " +
            widget.currentOutlet.name! +
            "?", () async {
      await DBProvider().deleteAll();
      if (mounted) {
        setState(() {
          Constant.dishorederlist.clear();
          Constant.selidlist.clear();
          Get.back();
          _checkVariation(featuredItems);
        });
      }
    }, () {
      Get.back();
    }, context);
  }
}
