import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomUI {
  static SnackBar showToast(String msg, BuildContext context) {
    return SnackBar(
      content: Text(
        msg,
        // style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      //backgroundColor: Theme.of(context).colorScheme.secondary,
    );
  }

  static showProgress(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).colorScheme.secondary)),
          );
        });
  }

  static Widget CustProgress(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
          valueColor:
              AlwaysStoppedAnimation<Color>(Theme.of(context).colorScheme.secondary)),
    );
  }

  static hideProgress(BuildContext context) {
    Get.back();
  }

  static customAlert(String title, String msg, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: const EdgeInsets.all(10),
          contentPadding: const EdgeInsets.all(10),
          title: Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Text(msg),
          actions: <Widget>[
            TextButton(
              style: flatButtonStyle,
              onPressed: () {
                Get.back();
              },
              child: const Text('OK',style: TextStyle(color: BTNTXTCLR),),
            )
          ],
        );
      },
    );
  }

  static customAlertDialog(String title, String msg, Function posClick,
      Function negClick, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
          contentPadding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
          title: Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Text(msg),
          actions: <Widget>[
            TextButton(
                style: flatButtonStyle,
                onPressed:()=>negClick(),
                child: const Text('No',style: TextStyle(color: BTNTXTCLR),)),
            TextButton(
                style: flatButtonStyle,
                onPressed: ()=>posClick(),
                child: const Text('Yes',style: TextStyle(color: BTNTXTCLR),)),
          ],
        );
      },
    );
  }
}

Future<dynamic> openBottomSheetDialog(
    BuildContext context, Widget contentUI) async {
  dynamic r = await showModalBottomSheet(
      context: context,
      barrierColor: (Theme.of(context).brightness == Brightness.dark)
          ? Colors.white54
          : Colors.black.withOpacity(0.5),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      builder: (BuildContext context) {
        return contentUI;
      });
  return r;
}

// No Internet Connection
class NoInternetDialog extends StatefulWidget {
  final Function? retryClick;
  NoInternetDialog({Key? key, this.retryClick});

  @override
  _NoInternetDialogState createState() => _NoInternetDialogState();
}

class _NoInternetDialogState extends State<NoInternetDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Text(Constant.api_no_net_msg),
            const SizedBox(
              height: 20,
            ),
            SwiftoBtn('Try Again', widget.retryClick)
          ],
        ),
      ),
    ));
  }
}
