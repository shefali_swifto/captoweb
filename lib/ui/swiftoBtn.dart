import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';

class SwiftoBtn extends StatelessWidget {
  String? btnText;
  Function? btnClick;

  SwiftoBtn(this.btnText,this.btnClick);

  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: flatButtonStyle,
      onPressed:()=> btnClick!(),
      child:  Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width/2,
        child: Text(
          btnText!,
          style: TextStyle(color: BTNTXTCLR),
        ),
      )
    );
  }
}

class SwiftoBtn1 extends StatelessWidget {
  String btnText;
  Function btnClick;

  SwiftoBtn1(this.btnText,this.btnClick);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: flatButtonStyle,
      onPressed:()=> btnClick(),
      child: Text(
        btnText,
        style: TextStyle(color: BTNTXTCLR),
      ),
    );
  }
}
