import 'package:capto/models/modifierCatPojo.dart';
import 'package:capto/models/preModelPojo.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';


class PreUI extends StatefulWidget {
  double fontsize = 18;
  double pricesize = 16;
  List<ModifierCat> precatlist = [];
  List<Pre> prelist = [];
  Function updateQty, selPre;
  PreUI(
      {Key? key,
        required this.precatlist,
        required this.prelist,
        required this.updateQty,
        required this.selPre})
      : super(key: key);

  @override
  _PreUIState createState() => _PreUIState();
}

class _PreUIState extends State<PreUI> {

  @override
  Widget build(BuildContext context) {

    return ListView.builder(
        shrinkWrap: true,
        itemCount: widget.precatlist.length,
        itemBuilder: (BuildContext context, int index) {
          String preCat = widget.precatlist[index].catName.toString();
          if (widget.precatlist[index].modifierSelection != null &&
              widget.precatlist[index].modifierSelection! > 0)
            preCat = preCat +
                '(' +
                'Max' +
                ' ${widget.precatlist[index].modifierSelection} ' +
                'Allowed' +
                ')';
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                preCat,
                style: TextStyle(
                    fontSize: widget.fontsize, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 8,
              ),
              prefUI(widget.precatlist[index])
            ],
          );
        });
  }

  Widget prefUI(ModifierCat cat) {
    List<Pre> preList = widget.prelist
        .where((element) => element.pretype == cat.catId.toString())
        .toList();
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: preList.length,
          itemBuilder: (BuildContext context, int index) {
            Pre pre = preList[index];
            String prePrice =
                Constant.currency +
                    double.parse(pre.preprice.toString()).toStringAsFixed(int.parse(Constant.decimal_value));
            if (cat.tot_sel == 0 && pre.type == 'Single' && index == 0) {
              Future.delayed(new Duration(milliseconds: 5), () {
                widget.selPre(pre,
                    cat.modifierSelection); //select default first if type=single
              });
            }
            return Row(
              children: [
                IconButton(
                    onPressed: () => widget.selPre(pre, cat.modifierSelection),
                    icon: Image.asset(
                      (pre.isCheck!)
                          ? 'assets/checkbox.png'
                          : 'assets/checkbox_unselect.png',
                      height: 25,
                      color: (pre.isCheck!)
                          ? mainClr
                          : Theme.of(context).iconTheme.color,
                    )),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      widget.selPre(pre, cat.modifierSelection);
                    },
                    child: Text(
                      pre.prenm! ,
                      style: TextStyle(fontSize: widget.fontsize),
                    ),
                  ),
                  flex: 1,

                ),
                Container(
                  margin: const EdgeInsets.only(right: 10.0),
                  child: Text(
                    prePrice,
                    style: TextStyle(fontSize: widget.pricesize),
                  ),
                ),
              ],
            );
          }),
    );
  }
}
