import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';

class SwiftoQty extends StatelessWidget {
  bool isEditable, isAdded;
  String qty;
  Function? ClickInc, ClickDec, clickAdd;

  SwiftoQty(
    this.isEditable,
    this.isAdded,
    this.qty,
    this.ClickInc,
    this.ClickDec,
    this.clickAdd,
  );

  @override
  Widget build(BuildContext context) {
    return (isEditable)
        ? (isAdded)
            ? Container(
                width: 100,
                height: 40,
                constraints: BoxConstraints(minWidth: 100),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    border: Border.all(
                      color: defaultClr,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(5))),
                padding: const EdgeInsets.all(1),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => ClickDec!(),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                              'assets/ic_minus.png',
                              color: defaultClr,
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        padding: const EdgeInsets.only(top: 2, bottom: 4),
                        child: Text(
                          qty,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => ClickInc!(),
                          hoverColor: Theme.of(context).primaryColor,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                              'assets/ic_add.png',
                              color: defaultClr,
                            ),
                          )),
                    ),
                  ],
                ),
              )
            : Container(
                  width: 100,
                  height: 40,
                constraints: const BoxConstraints(minWidth: 100),
                child: TextButton(
                  onPressed: () => clickAdd!(),
                  child: Text(
                    'Add'.toUpperCase(),
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: defaultClr),
                  ),
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(4),
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          const EdgeInsets.all(5)),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(defaultClr),
                      backgroundColor: MaterialStateProperty.all<Color>(
                        Colors.white),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: const BorderSide(color: defaultClr)))),
                ),
              )
        : Container(
            width: 100,
            height: 40,
            constraints: BoxConstraints(minWidth: 100),
            margin: EdgeInsets.only(left: 20, right: 20),
            alignment: Alignment.center,
            child: Text(
              qty,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 15.0,
              ),
            ),
          );
  }
}
