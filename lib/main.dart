import 'dart:ui';

import 'package:capto/DBOrder.dart';
import 'package:capto/localdb/orderAdapter.dart';
import 'package:capto/models/app.dart';
import 'package:capto/routes/route_helper.dart' as routehelper;
import 'package:capto/util/constant.dart';
import 'package:firebase_core/firebase_core.dart' as firebasecore;
import 'package:flutter/foundation.dart';
// import 'package:flutter_stripe/flutter_stripe.dart';
// import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'util/styles.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(OrderAdapter());
  await Hive.openBox(DBProvider.TBL);
  if (!kIsWeb) {
    await firebasecore.Firebase.initializeApp();
  } else {
    await firebasecore.Firebase.initializeApp(
      options: const firebasecore.FirebaseOptions(
          apiKey: "AIzaSyDJxhMWnJvKsuuV-IAkaECziO0ybAt3hmw",
          authDomain: "royalpos-41e37.firebaseapp.com",
          databaseURL: "https://royalpos-41e37.firebaseio.com",
          projectId: "royalpos-41e37",
          storageBucket: "royalpos-41e37.appspot.com",
          messagingSenderId: "941247719853",
          appId: "1:941247719853:web:6713beaf79792f941e5924",
          measurementId: "G-E0DFCSRGNJ"),
    );
  }
  runApp(ChangeNotifierProvider.value(
    value: AppModel(),
    child: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isLoggedIn = false;

  @override
  Widget build(BuildContext context) {
    if (Provider.of<AppModel>(context).isLoading!) {
      return Container();
    } else {
      return GetMaterialApp(
        title: Constant.appName,
        debugShowCheckedModeBanner: false,
        navigatorKey: Get.key,
        scrollBehavior: const MaterialScrollBehavior().copyWith(
          dragDevices: {PointerDeviceKind.mouse, PointerDeviceKind.touch},
        ),
        theme: Provider.of<AppModel>(context).darkTheme!
            ? buildDarkTheme().copyWith()
            : buildLightTheme().copyWith(),
        supportedLocales: const [
          Locale('en'),
        ],
        locale: const Locale('en'),
        initialRoute: routehelper.RouteHelper.getInitialRoute(), //'l76q3s'
        getPages: routehelper.RouteHelper.routes,
        defaultTransition: Transition.topLevel,
        transitionDuration: const Duration(milliseconds: 500),
      );
    }
  }
}
//todo: change homepage listview ui and add photo ui change....
///#/rest/l76q3s
///#/rest/l76q3s/11