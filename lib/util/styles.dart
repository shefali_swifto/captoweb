import 'package:flutter/material.dart';

const defaultClr = Color(0XFF0D194D);
Color mainClr = Color(0XFF0D194D);
Color BTNBGCLR = mainClr;
const BTNTXTCLR = Colors.white;

ButtonStyle flatButtonStyle = TextButton.styleFrom(
  backgroundColor: BTNBGCLR,
  padding: const EdgeInsets.all(15),
);

ThemeData buildLightTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
      colorScheme: base.colorScheme.copyWith(secondary: Colors.black),
      brightness: Brightness.light,
      primaryColorLight: Colors.white54,
      hintColor: Colors.grey,
      primaryColor: Colors.white,
      dividerColor: Colors.grey,
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: AppBarTheme(
          elevation: 0,
          iconTheme: const IconThemeData(color: Colors.white),
          backgroundColor: mainClr,
          titleTextStyle: const TextStyle(
            color: Colors.white,
            fontSize: 18.0,
            fontWeight: FontWeight.w800,
          ),
          toolbarTextStyle: const TextStyle(color: Colors.white)),
      cardColor: Colors.white70,
      buttonTheme: ButtonThemeData(
          buttonColor: defaultClr,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          textTheme: ButtonTextTheme.primary),
      iconTheme: const IconThemeData(color: Colors.black));
}

ThemeData buildDarkTheme() {
  final ThemeData base = ThemeData.dark();
  return base.copyWith(
    colorScheme: base.colorScheme.copyWith(secondary: Colors.white),
    brightness: Brightness.dark,
    primaryColor: Colors.black,
    primaryColorLight: Colors.black45,
    scaffoldBackgroundColor: Colors.black,
    cardColor: Colors.white24,
    hintColor: Colors.white,
    dividerColor: Colors.grey,
    appBarTheme: AppBarTheme(
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.white),
        backgroundColor: mainClr,
        titleTextStyle: const TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
        toolbarTextStyle: const TextStyle(color: Colors.white)),
    buttonTheme: ButtonThemeData(
        buttonColor: defaultClr,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        textTheme: ButtonTextTheme.primary),
    iconTheme: const IconThemeData(color: Colors.white),
  );
}
