import 'dart:developer';

import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/dishOrderPojo.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/util/keyConstant.dart';
// import 'package:geocoder2/geocoder2.dart';
import 'package:geolocator/geolocator.dart';

class Constant {
  static String appName = "Capto";
  static String base_url ="https://royalpos.in/royalpos/public/"; //"https://swiftomatics.in/testroyalpos/public/"; // "http://192.168.29.2/royalpos/public/";////
  static String outlet_logo_url = base_url + "uploads/";
  static String currency = "";
  static const kGoogleApiKey = "AIzaSyDt2lQp8f7rIx7S3iQ6evRbp9dW3m7Qx0w";
  static String country_code = '';
  static String country = '';
  static bool isShowLog = true, allowCOD = true,showMenuImg=false;
  static String terms_url = 'https://royalpos.in/capto/terms.html';
  static String policy_url = 'https://royalpos.in/capto/privacy.html';
  static String contactus_url = 'https://royalpos.in/capto/contact.html';
  static String android_url =
      'https://play.google.com/store/apps/details?id=com.royalpos.capto';
  static String ios_url = 'https://itunes.apple.com/in/app/capto/id';
  static String share_app = "Please install from Android:" + android_url + "\n";

  static List<DishOrderPodo> dishorederlist = [];
  static List<DishOrderPodo> placeitemlist = [];
  static List<String> selidlist = [];

  static List<DishOrderPojo> cartItems=[];
  static List<int> cartItemIds=[];

  static Outlet? shopData;
  static String? round_boundry;
  static String? service_charge,deliveryType;
  static String decimal_value = '2';
  static Map? sel_address;

  static int api_catch_error = 102;
  static int api_no_net = 101;

  static String api_catch_error_msg = 'Something wrong!!!Try Later';
  static String api_no_net_msg = 'No Internet Connection';

  static showLog(var txt) {
    if (isShowLog) print(txt);
  }

  static showStringLog(String txt) {
    if (isShowLog) log(txt);
  }

  static Future getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;
    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      } else {
        permission = await Geolocator.checkPermission();
        if (permission == LocationPermission.deniedForever) {
          return Future.error(
              'Location permissions are permantly denied, we cannot request permissions.');
        } else {
          if (permission == LocationPermission.denied) {
            permission = await Geolocator.requestPermission();
            if (permission != LocationPermission.whileInUse &&
                permission != LocationPermission.always) {
              return Future.error(
                  'Location permissions are denied (actual value: $permission).');
            } else {
              await getLocation();
            }
          }
        }
      }
    } catch (e) {
      Constant.showLog(e);
    }
  }

  static Future getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    var lat = position.latitude.toString();
    var lang = position.longitude.toString();

    await KeyConstant.saveString(KeyConstant.key_lat, lat);
    await KeyConstant.saveString(KeyConstant.key_lng, lang);
    if (lat != null && lat.isNotEmpty && lang != null && lang.isNotEmpty) {
      // GeoData first = await Geocoder2.getDataFromCoordinates(
      //     latitude: position.latitude,
      //     longitude: position.longitude,
      //     googleMapApiKey: Constant.kGoogleApiKey);
      // String country = first.country;
      // await KeyConstant.saveString(KeyConstant.key_address, 'Current Location');
      // await KeyConstant.saveString(KeyConstant.key_country, country);
    }
  }

  static titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] =
          splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }
}
