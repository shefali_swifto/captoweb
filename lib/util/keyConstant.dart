import 'dart:convert';

import 'package:capto/models/outlets.dart';
import 'package:capto/util/constant.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class KeyConstant {
  static String key_lat = "userlat";
  static String key_lng = "userlng";
  static String key_zipcode= "userzipcode";
  static String key_type= "utype";//nearby,zipcode
  static String key_address= "uaddress";

  static String key_brand_id = "brand_id";
  static String key_rest_id = "rest_id";
  static String key_rest_uniq_id = "rest_uniq_id";
  static String key_rest_name = "rest_name";
  static String key_rest_address = "rest_address";
  static String key_rest_logo = "rest_logo";
  static String key_rest_count = "rest_cnt";
  static String key_rest_currency = "rest_currency";
  static String key_rest_desc = "rest_desc";
  static String key_delivery_available = "delivery_available";
  static String key_capto_delivery_status = "capto_delivery_status";
  static String key_advance_order = "advance_order";
  static String key_adv_order_limit = "adv_order_limit";
  static String key_price_wt = "price_wt";
  static String key_user_id = 'userid';
  static String key_user_name = 'username';
  static String key_user_phone = 'userphone';
  static String key_user_email = 'useremail';
  static String key_user_address = 'useraddress';
  static String key_country= "usercountry";
  static String key_firsttime = 'firsttime';
  static String key_service_charge = "service_charge";
  static String key_decimal_val = "decimal_value";
  static String key_round_boundry = "round_boundry";

  static String type_takeaway = "takeaway";
  static String type_delivery = "delivery";

  static String type_takeaway_txt = "Pickup";
  static String type_delivery_txt = "Delivery";

  static String o_type_nearby = "nearby";
  static String o_type_zipcode = "zipcode";

  static String status_inprocess = 'In Process';
  static String status_dispatch = 'Dispatched';
  static String status_complete = 'Complete';
  static String status_cancel = 'Canceled';
  static String status_ready = 'Order Ready';

  static String key_theme = "appTheme";
  static bool key_themeLight = false;
  static bool key_themeDark = true;
  static String key_firstTime = "firstTime";
  static String query='';

  static Future saveString(String key, String val) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(key, val);
  }

  static Future<String?> retriveString(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.containsKey(key)) {
      if (key == key_rest_count)
        return preferences.getInt(key).toString();
      else
        return preferences.getString(key);
    } else
      return null;
  }

  static Future<bool?> retriveBool(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.containsKey(key)) {
      return preferences.getBool(key);
    } else if (key == key_firstTime)
      return true;
    else
      return false;
  }

  static Future<bool> isUserLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? uid = preferences.getString(key_user_id);
    if (uid != null && uid.isNotEmpty && uid != '0')
      return true;
    else
      return false;
  }

  static Future saveOutletInfo(Outlet outlet, int tot_cnt,String brandid) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('outlet', json.encode(outlet.toJson()));
    preferences.setString(key_brand_id,brandid);
    preferences.setString(key_rest_id, outlet.id.toString());
    preferences.setString(key_rest_uniq_id, outlet.restUniqueId!);
    preferences.setString(key_rest_name, outlet.name!);
    preferences.setString(key_rest_address, outlet.address!);
    preferences.setString(key_rest_logo, outlet.logoUrl!);
    preferences.setInt(key_rest_count, tot_cnt);
    preferences.setString(key_price_wt, outlet.showItemPriceWithoutTax!);
    preferences.setString(
        key_service_charge, outlet.deliveryCharges.toString());
    preferences.setString(key_decimal_val, outlet.decimalValue.toString());
    if (outlet.adv_order_duration != null) {
      preferences.setInt(key_adv_order_limit, outlet.adv_order_duration!);
    }
    preferences.setString(key_advance_order, outlet.advance_order.toString());
    Map rounding = {
      'val_interval': outlet.intervalNumber,
      'rule_tag': outlet.roundingTag,
      'rounding_id': outlet.roundingId
    };
   // Constant.showLog(rounding);
    preferences.setString(key_round_boundry, json.encode(rounding));
    preferences.setString(key_delivery_available, outlet.delivery_available!);
    preferences.setString(key_capto_delivery_status, outlet.capto_delivery_status!);
    preferences.setString(key_rest_currency,outlet.currency!);
    preferences.setString(key_rest_desc, outlet.description!);
  }

  static Future<Outlet> getOulet() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String o=preferences.getString('outlet')??'';
    Outlet outlet=Outlet();
    if(o.isNotEmpty)
      outlet = Outlet.fromJson(json.decode(o));
    return outlet;
  }

  static Future saveUserInfo(Map res) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(KeyConstant.key_user_id, res['id'].toString());
    preferences.setString(KeyConstant.key_user_name, res['name']);
    preferences.setString(
        KeyConstant.key_user_phone, res['phone_no'].toString());
    preferences.setString(KeyConstant.key_user_email, res['email']);
  }

  Map priceCal(String taxtype, double tax_per_tot, double enterPrice) {
    //AppConst.printLog(taxtype+" "+tax_per_tot.toString());
    double priceperdish = 0.0, pwtax = 0.0, newgst = 0.0, cprice = enterPrice;
    if (taxtype != null && taxtype.isNotEmpty) {
      if (taxtype == "1") {
        priceperdish = cprice;
        newgst = cprice - (cprice * (100 / (100 + tax_per_tot)));
        pwtax = cprice - newgst;
      } else if (taxtype == "0") {
        pwtax = cprice;
        newgst = (cprice * tax_per_tot) / 100;
        priceperdish = cprice + newgst;
      }
    } else {
      priceperdish = cprice;
      pwtax = cprice;
      newgst = 0.0;
    }
    Map resJOSN = {
      "cal_price": priceperdish,
      "cal_price_without_tax": pwtax,
      "cal_tax": newgst
    };
    return resJOSN;
  }

  void showToast(String msg,BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(msg),
    ));
  }
}
