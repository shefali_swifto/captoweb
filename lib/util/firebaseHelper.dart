import 'dart:convert';
import 'dart:io';

import 'package:capto/util/constant.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class FirebaseHelper{
  BuildContext? context;
  Function? onclick,onrefresh;
  // FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;
  bool? isSplash=false;
  static bool isLaunch=false;

   FirebaseHelper(BuildContext context,{bool isSplash=false,Function? onRefresh=null}){
     this.context=context;
     this.isSplash=isSplash;
     this.onrefresh=onRefresh;
    firebaseCloudMessaging_Listeners();
     /*flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
     var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
     var iOS = new IOSInitializationSettings();
     var initSetttings = new InitializationSettings(android:android,iOS: iOS);
     // flutterLocalNotificationsPlugin.initialize(initSetttings,
     //     onSelectNotification: _onSelectNotification);*/
  }

  void setOnRefresh(Function onRefresh){
    this.onrefresh=onRefresh;
  }

  void firebaseCloudMessaging_Listeners() {
    //if (Platform.isIOS) iOS_Permission();

    // _fcm.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     Constant.showLog('on message $message');
    //     showNotification(message['notification']['title'], message['notification']['body'],message);
    //     if(onrefresh!=null)
    //       onrefresh(message);
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     Constant.showLog('on resume $message');
    //     if(isSplash)
    //       onClick(message);
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     Constant.showLog('on launch $message');
    //     if(isSplash)
    //       onClick(message);
    //   },
    // );
  }


}