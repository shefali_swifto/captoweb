import 'package:capto/localdb/order.dart';
import 'package:hive_flutter/adapters.dart';

// GENERATED CODE - DO NOT MODIFY BY HAND


// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************


class OrderAdapter extends TypeAdapter<Order> {
  @override
  final int typeId = 1;

  @override
  Order read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Order(
      id: fields[0] as String,
      dish_id: fields[1] as String,
      dnm: fields[2] as String,
      dimg: fields[3] as String,
      type: fields[4] as String,
      qty: fields[5] as String,
      priceperdish: fields[6] as String,
      price: fields[7] as String,
      discount: fields[8] as String,
      dishcmt: fields[9] as String,
      sold_by: fields[10] as String,
      tot_tax: fields[11] as String,
      tax_amt: fields[12] as String,
      price_without_tax: fields[13] as String,
      priceper_without_tax: fields[14] as String,
      taxdata: fields[15] as String,
      offer: fields[16] as String,
      disamount: fields[17] as String,
      pre_id: fields[18] as String,
      pre_flag: fields[19] as String,
      var_array: fields[20] as String,
      add_time: fields[21] as String,
      brand_id: fields[22] as String,
      rest_id: fields[23] as String,
      rest_name: fields[24] as String,
      rest_uniq_id: fields[25] as String,
      rest_address: fields[26] as String,
      rest_img: fields[27] as String

    );
  }

  @override
  void write(BinaryWriter writer, Order obj) {
    writer
      ..writeByte(28)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.dish_id)
      ..writeByte(2)
      ..write(obj.dnm)
      ..writeByte(3)
      ..write(obj.dimg)
      ..writeByte(4)
      ..write(obj.type)
      ..writeByte(5)
      ..write(obj.qty)
      ..writeByte(6)
      ..write(obj.priceperdish)
      ..writeByte(7)
      ..write(obj.price)
      ..writeByte(8)
      ..write(obj.discount)
      ..writeByte(9)
      ..write(obj.dishcmt)
      ..writeByte(10)
      ..write(obj.sold_by)
      ..writeByte(11)
      ..write(obj.tot_tax)
      ..writeByte(12)
      ..write(obj.tax_amt)
      ..writeByte(13)
      ..write(obj.price_without_tax)
      ..writeByte(14)
      ..write(obj.priceper_without_tax)
      ..writeByte(15)
      ..write(obj.taxdata)
      ..writeByte(16)
      ..write(obj.offer)
      ..writeByte(17)
      ..write(obj.disamount)
      ..writeByte(18)
      ..write(obj.pre_id)
      ..writeByte(19)
      ..write(obj.pre_flag)
      ..writeByte(20)
      ..write(obj.var_array)
      ..writeByte(21)
      ..write(obj.add_time)
      ..writeByte(22)
      ..write(obj.brand_id)
      ..writeByte(23)
      ..write(obj.rest_id)
      ..writeByte(24)
      ..write(obj.rest_name)
      ..writeByte(25)
      ..write(obj.rest_uniq_id)
      ..writeByte(26)
      ..write(obj.rest_address)
      ..writeByte(27)
      ..write(obj.rest_img)
    ;
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is OrderAdapter &&
              runtimeType == other.runtimeType &&
              typeId == other.typeId;
}