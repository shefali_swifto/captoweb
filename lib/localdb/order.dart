import 'package:hive/hive.dart';

@HiveType(typeId: 1)
class Order {
  
  @HiveField(0)
  String? id;

  @HiveField(1)
  String? dish_id;

  @HiveField(2)
  String? dnm;

  @HiveField(3)
  String? dimg;

  @HiveField(4)
  String? type;

  @HiveField(5)
  String? qty;

  @HiveField(6)
  String? priceperdish;

  @HiveField(7)
  String? price;

  @HiveField(8)
  String? discount;

  @HiveField(9)
  String? dishcmt;

  @HiveField(10)
  String?  sold_by;

  @HiveField(11)
  String? tot_tax;

  @HiveField(12)
  String? tax_amt;

  @HiveField(13)
  String? price_without_tax;

  @HiveField(14)
  String? priceper_without_tax;

  @HiveField(15)
  String? taxdata;

  @HiveField(16)
  String? offer;

  @HiveField(17)
  String?  disamount;

  @HiveField(18)
  String? pre_id;

  @HiveField(19)
  String? pre_flag;

  @HiveField(20)
  String?  var_array;

  @HiveField(21)
  String?  add_time;

  @HiveField(22)
  String? brand_id;

  @HiveField(23)
  String?  rest_id;

  @HiveField(24)
  String? rest_name;

  @HiveField(25)
  String?  rest_uniq_id;

  @HiveField(26)
  String? rest_address;

  @HiveField(27)
  String? rest_img;

  Order({
      this.id,
      this.dish_id,
      this.dnm,
      this.dimg,
      this.type,
      this.qty,
      this.priceperdish,
      this.price,
      this.discount,
      this.dishcmt,
      this.sold_by,
      this.tot_tax,
      this.tax_amt,
      this.price_without_tax,
      this.priceper_without_tax,
      this.taxdata,
      this.offer,
      this.disamount,
      this.pre_id,
      this.pre_flag,
      this.var_array,
      this.add_time,
      this.brand_id,
      this.rest_id,
      this.rest_name,
      this.rest_uniq_id,
      this.rest_address,
      this.rest_img});
}