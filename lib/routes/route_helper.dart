import 'dart:convert';
import 'package:capto/models/outlets.dart';
import 'package:capto/screens/addAddressScreen.dart';
import 'package:capto/screens/categoryItemScreen.dart';
import 'package:capto/screens/confirmScreen.dart';
import 'package:capto/screens/dinein/cartScreen.dart';
import 'package:capto/screens/dinein/placeorder.dart';
import 'package:capto/screens/infoDetailScreen.dart';
import 'package:capto/screens/infoScreen.dart';
import 'package:capto/screens/itemDetail.dart';
import 'package:capto/screens/loginScreen.dart';
import 'package:capto/screens/mainScreen.dart';
import 'package:capto/screens/notificationScreen.dart';
import 'package:capto/screens/orderDetailScreen.dart';
import 'package:capto/screens/orderPlacedScreen.dart';
import 'package:capto/screens/outletDetail.dart';
import 'package:capto/screens/paymentScreen.dart';
import 'package:capto/screens/registerScreen.dart';
import 'package:capto/screens/searchScreen.dart';
import 'package:capto/screens/dinein/searchScreen.dart' as dinein;
import 'package:capto/screens/selectAddress.dart';
import 'package:capto/screens/splashScreen.dart';
import 'package:capto/screens/verifyOTPScreen.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RouteHelper extends Route {
  static const String initial = '/'; //
  static const String addAddress = '/add-address';
  static const String confirmScreen = '/checkout';
  static const String infoDetail = '/info-detail'; //
  static const String info = '/info'; //
  static const String itemDetail = '/item-detail';
  static const String login = '/sign-in'; //
  static const String mainScreen = '/restaurant'; //
  static const String notification = '/notification'; //
  static const String orderDetail = '/order-detail';
  static const String orderPlaced = '/order-placed';
  static const String payment = '/payment';
  static const String register = '/sign-up'; //
  static const String searchItem = '/search-item';
  static const String searchDineinItem = '/item-search';
  static const String selectAddress = '/select-address';
  static const String splash = '/rest'; //
  static const String verifyOTP = '/verifyOTP';
  static const String placeOrder = '/placeOrder';
  static const String dineincart = '/dineinCart';
  static const String outletDetail = '/outletDetail';
  static const String catgoryItem = '/category-item';

  static String getInitialRoute() {
    return '$initial';
  }

  static String getMainRoute() => '$mainScreen/${KeyConstant.query}';

  static String getInfoRoute() => '$info';

  static String getInfoDetailRoute(String str) => '$infoDetail?page=$str';

  static String getSignInRoute() => '$login';

  static String getNotificationRoute() => '$notification';

  static String getSignUpRoute(String str) => '$register?phone=$str';

  static String getAddAddressRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$addAddress?data=$_data';
  }

  static String getCategoryItems(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$catgoryItem?data=$_data';
  }

  static String getCheckoutRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$confirmScreen?data=$_data';
  }

  static String getItemDetailRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$itemDetail?data=$_data';
  }

  static String getOrderDetailRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$orderDetail?data=$_data';
  }

  static String getPaymentRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$payment?data=$_data';
  }

  static String getSearchRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$searchItem?data=$_data';
  }

  static String getSearchItemRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$searchDineinItem?data=$_data';
  }

  static String getSelectAddressRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$selectAddress?data=$_data';
  }

  static String getOrderPlacedRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$orderPlaced?data=$_data';
  }

  static String getVerifyOTPRoute(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$verifyOTP?data=$_data';
  }

  static String getPlaceOrder(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$placeOrder?data=$_data';
  }

  static String getDineInCart(Map str) {
    String _data = base64Url.encode(utf8.encode(jsonEncode(str)));
    return '$dineincart?data=$_data';
  }

  static String getSplash(String str) {
    Constant.showLog('$splash/${str}');
    return '$splash/${str}';
  }

  static String getOutletDetail() => '$outletDetail';

  static List<GetPage> routes = [
    GetPage(name: initial, page: () => getRoute(SplashScreen())),
    GetPage(
        name: '${splash}/:id/:table',
        page: () {
          // Constant.showLog(Get.parameters);
          // Constant.showLog('id:' + Get.parameters['id'].toString());
          // Constant.showLog('table:' + Get.parameters['table'].toString());
          return SplashScreen(
            queryParam:
            Get.parameters['id'] == 'null' ? null : Get.parameters['id'],
            tblId: Get.parameters['table'],
          );
        }),
    GetPage(
        name: '${splash}/:id',
        page: () {
          return SplashScreen(
              queryParam:
              Get.parameters['id'] == 'null' ? null : Get.parameters['id']);
        }),
    GetPage(name: mainScreen, page: () => getRoute(MainScreen())),
    GetPage(name: outletDetail, page: () => getRoute(OutletDetail())),
    GetPage(name: login, page: () => getRoute(LoginScreen())),
    GetPage(name: info, page: () => getRoute(InfoScreen())),
    GetPage(
        name: infoDetail,
        page: () => getRoute(
            InfoDetailScreen(action: Get.parameters['page'].toString()))),
    GetPage(name: notification, page: () => getRoute(Notifications())),
    GetPage(
        name: register,
        page: () => getRoute(
            RegisterScreen(phone: Get.parameters['phone'].toString()))),
    GetPage(
        name: addAddress,
        page: () {
          Map d = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(AddAddressScreen(
            action: d['action'],
            addressList: d['addressList'],
          ));
        }),
    GetPage(
        name: confirmScreen,
        page: () {
          Map d = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          Outlet o = Outlet.fromJson(d['outletData']);
          return getRoute(ConfirmScreen(
            cartData: d['cartData'],
            outletData: o,
          ));
        }),
    GetPage(
        name: itemDetail,
        page: () {
          Map d = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          Outlet o = Outlet.fromJson(d['currentOutlet']);
          return getRoute(ItemDetail(
            currentOutlet: o,
            item: d['item'],
            restid: d[' restid'],
            isPriceWT: d['isPriceWT'],
            outletStatus: d['outletStatus'],
            //    changeTab: d['changeTab']
          ));
        }),
    GetPage(
        name: orderDetail,
        page: () {
          Map arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(OrderDetail(
              order_id: arg['order_id'],
              brandid: arg['brandid'],
              rest_id: arg['rest_id'],
              runiq_id: arg['runiq_id'],
              parentScreen: arg['parentScreen']));
        }),
    GetPage(
        name: orderPlaced,
        page: () {
          Map arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(OrderPlaced(
              orderJson: arg['orderJson'],
              transJson: arg['transJson'],
              currency: arg['currency'],
              brandid: arg['brandid'],
              runiqid: arg['runiqid']));
        }),
    GetPage(
        name: payment,
        page: () {
          Map arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(PaymentScreen(
              orderJson: arg['orderJson'],
              transJson: arg['transJson'],
              currency: arg['currency']));
        }),
    GetPage(
        name: searchItem,
        page: () {
          // dynamic arg = Get.parameters['data'];
          Map arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          Outlet o = Outlet.fromJson(arg['currentOutlet']);
          return getRoute(
              SearchScreen(currentOutlet: o, isTab: arg['isTab'] as bool));
        }),
    GetPage(
        name: searchDineinItem,
        page: () {
          Map<String,dynamic> arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          Outlet o = Outlet.fromJson(arg);
          return getRoute(
              dinein.SearchScreen(currentOutlet: o));
        }),
    GetPage(
        name: selectAddress,
        page: () {
          dynamic arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          Outlet o = Outlet.fromJson(arg['outletData']);
          return getRoute(SelectAddress(
            isChangeLocation: arg['isChangeLocation'],
            outletData: o,
            cartData: arg['cartData'],
          ));
        }),
    GetPage(
        name: verifyOTP,
        page: () {
          dynamic arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(VerifyCode(
              fromCart: arg['fromCart'],
              verId: arg['verId'].toString(),
              phoneNumber: arg['phoneNumber'].toString(),
              cResult: LoginScreen.loginGlobalKey.currentState!.cResult));
        }),
    GetPage(
        name: placeOrder,
        page: () {
          dynamic arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(PlaceOrder(
            tblRes: arg,
          ));
        }),
    GetPage(
        name: dineincart,
        page: () {
          dynamic arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(CartScreen(
            orderRes: arg,
          ));
        }),
    GetPage(
        name: catgoryItem,
        page: () {
          dynamic arg = jsonDecode(utf8.decode(base64Url
              .decode(Get.parameters['data'].toString().replaceAll(' ', '+'))));
          return getRoute(CategoryItemScreen(
            catList: arg['cat'],
            pos: arg['pos'],
            currentOutlet: Constant.shopData!,
          ));
        }),
  ];

  static getRoute(Widget navigateTo) {
    return navigateTo;
    // AppConstants.APP_VERSION < _minimumVersion ? UpdateScreen(isUpdate: true)
    //     : Get.find<SplashController>().configModel.maintenanceMode ? UpdateScreen(isUpdate: false)
    //     : Get.find<LocationController>().getUserAddress() != null ? navigateTo
    //     : AccessLocationScreen(fromSignUp: false, fromHome: false, route: Get.currentRoute);
  }
}
