import 'package:capto/DBOrder.dart';
import 'package:capto/apis/dataAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/screens/dialogLayout/variationDialog.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  Outlet? currentOutlet;

  SearchScreen({
    Key? key,
    required this.currentOutlet,
  }) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController controller = TextEditingController();
  List _searchResult = [];
  List _itemList = [];
  Map? res;

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  getAll() async {
    res = await getAllItems();
    if (res != null && res!.containsKey('success') && res!['success'] == 1) {
      _itemList = res!['data'];
    }
    if(mounted)
      setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getAll();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
            decoration: BoxDecoration(
              border: Border.all(color: mainClr),
              borderRadius: BorderRadius.all(Radius.circular(5))
            ),
            child: ListTile(
              leading:IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Image.asset('assets/ic_back.png',width: 18,),
                    ),
              title: TextField(
                controller: controller,
                decoration: InputDecoration(
                    hintText: 'Search', border: InputBorder.none),
                onChanged: onSearchTextChanged,
              ),
              trailing: IconButton(
                icon: Image.asset('assets/ic_close.png',width: 18,),
                onPressed: () {
                  controller.clear();
                  onSearchTextChanged('');
                },
              ),
            ),
          ),
          (res == null)
              ? CustomUI.CustProgress(context)
              : (_itemList != null)
              ? Expanded(
            child: _searchResult.length != 0 ||
                controller.text.isNotEmpty
                ? ListView.builder(
              itemCount: _searchResult.length,
              itemBuilder: (context, i) {
                return _setRow(_searchResult[i]);
              },
            )
                : ListView.builder(
              itemCount: _itemList.length,
              itemBuilder: (context, index) {
                return _setRow(_itemList[index]);
              },
            ),
          )
              : (res != null &&
              res!.containsKey('success') &&
              res!['success'] == Constant.api_no_net)
              ? NoInternetDialog(
            retryClick: () {
              res = null;
              getAll();
            },
          )
              : const SizedBox(),
        ],
      ),
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      if(mounted)
        setState(() {});
      return;
    }

    _itemList.forEach((data) {
      String nm = data['dishname'].toString().toLowerCase();
      if (nm.contains(text.toLowerCase())) _searchResult.add(data);
    });
    if(mounted)
      setState(() {});
  }


  _setRow(Map u) {
    // String img = "";
    // if (u.containsKey("dishimage_url")) {
    //   img = u['dishimage_url'];
    // }
    return InkWell(
      child: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Expanded(flex: 1,child:Text(u['dishname'].toString()) ,),
                Text(Constant.currency +
                    u['price'].toString())
              ],
            ),
          ),
          const Divider(height: 1,),
        ],
      ),
      onTap: () async {
        if (Constant.selidlist != null && Constant.selidlist.isNotEmpty) {
          if (Constant.dishorederlist[0].rest_id == widget.currentOutlet!.id.toString() &&
              Constant.dishorederlist[0].brandid == widget.currentOutlet!.brand_id) {
            _checkVariation(u);
          } else {
            _showAlert(u);
          }
        } else {
          _checkVariation(u);
        }
      },
    );
  }

  void _showAlert(Map featuredItems) {
    CustomUI.customAlertDialog(
        "Remove Item?",
        "Your cart contains items from " +
            Constant.dishorederlist[0].rest_name! +
            ". Do you want to discard the " +
            "selection and add items from " +
            widget.currentOutlet!.name! +
            "?", () async {
      await DBProvider().deleteAll();
      if (mounted) {
        setState(() {
          Constant.dishorederlist.clear();
          Constant.selidlist.clear();
          Get.back();
          _checkVariation(featuredItems);
        });
      }
    }, () {
      Get.back();
    }, context);
  }

  _checkVariation(Map featuredItems) async {
    if (featuredItems.containsKey("preflag") &&
        featuredItems['preflag'] == 'true') {
      bool d_result = await openBottomSheetDialog(context, DialogVariation(
        item: featuredItems,
        currentOutlet: widget.currentOutlet!,
        isUpdate: false,
        updatePOS: 0,
      ));


      if (d_result) {
        Provider.of<AppModel>(context, listen: false).increment();
      //  widget.refreshClick!();
        if (mounted) {
          setState(() {});
        }
      }
    } else {
      addItemToCart(featuredItems);
    }
  }

  Future addItemToCart(Map featuredItems) async {
    DishOrderPodo d = DishOrderPodo();
    d.dishid = featuredItems['dishid'].toString();
    d.cusineid = featuredItems['cusineid'].toString();
    d.dishname = featuredItems['dishname'];
    d.dishtype = featuredItems['type'];
    d.description = featuredItems['description'];
    d.price = featuredItems['price'].toString();
    d.priceperdish = featuredItems['price'].toString();
    d.dishimage = featuredItems['dishimage_url'];
    d.preflag = featuredItems['preflag'].toString();
    d.sold_by = featuredItems['sold_by'].toString();
    d.priceper_without_tax = featuredItems['price_without_tax'].toString();
    d.price_without_tax = featuredItems['price_without_tax'].toString();
    d.tax_amt = featuredItems['tax_amt'].toString();
    d.offer = featuredItems['offers'].toString();
    d.discount = featuredItems['discount_amount'].toString();
    d.combo_flag = featuredItems['combo_flag'].toString();
    d.qty = "1";
    d.brandid = Constant.shopData!.brand_id.toString();
    d.rest_id = Constant.shopData!.id.toString();
    d.rest_uniq_id = Constant.shopData!.restUniqueId.toString();
    d.rest_name = Constant.shopData!.name.toString();
    d.rest_img = Constant.shopData!.logo.toString();
    d.rest_address = Constant.shopData!.address.toString();
    List tax_list = [];
    if (featuredItems.containsKey('tax_data')) {
      tax_list = featuredItems['tax_data'];
      if (tax_list != null && tax_list.length > 0) {
        double tot_tax = tax_list
            .map<double>((m) => double.parse(m['value']))
            .reduce((a, b) => a + b);
        d.tot_tax = tot_tax.toString();
      } else {
        d.tot_tax = '0';
      }
    } else {
      d.tot_tax = '0';
    }
    d.tax_data = tax_list;
    d.pre = featuredItems['pre'];
    d.preid = "";
    d.dish_comment = "";

    int r = await DBProvider().addOrderDetail(d);
    d.orderdetailid = r.toString();
    Constant.selidlist.add(featuredItems['dishid'].toString());
    Constant.dishorederlist.add(d);
    KeyConstant().showToast('Item added to cart', context);
    Provider.of<AppModel>(context, listen: false).increment();
    //widget.refreshClick!();
    if (mounted) {
      setState(() {});
    }
  }
}
