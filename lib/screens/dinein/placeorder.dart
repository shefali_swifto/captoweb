import 'dart:convert';
import 'package:capto/apis/dataAPI.dart';
import 'package:capto/apis/dineInAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/TaxInvoicePodo.dart';
import 'package:capto/models/VariationPodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/screens/dialogLayout/customerDialog.dart';
import 'package:capto/ui/catItemRow.dart';
import 'package:capto/ui/swiftoBadge.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class PlaceOrder extends StatefulWidget {
  Map tblRes;

  PlaceOrder({Key? key, required this.tblRes}) : super(key: key);

  @override
  State<PlaceOrder> createState() => _PlaceOrderState();
}

class _PlaceOrderState extends State<PlaceOrder> {
  Map? menuRes, orderRes;
  List menuList = [];
  final ItemScrollController _scrollController = ItemScrollController();
  List<TaxInvoicePodo>? taxInvoiceList;
  List<String>? taxidlist;
  double subtot = 0.0, grand = 0.0;
  int decimal_val = 2, currentIndex = -1;
  String currency = '';

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  Future getMenu() async {
    menuList = [];
    dynamic res = await getAllCuisine();
    if (res != null) {
      if (res is Map) {
        menuRes = res;
        if (res.containsKey('data')) {
          menuList.addAll(res['data']);
          if (menuList.isNotEmpty) currentIndex = 0;
        }
      }
    }
    if (mounted) {
      setState(() {
        getDish();
      });
    }
  }

  Future getDish() async {
    if(menuList.isNotEmpty) {
      dynamic res1 =
      await getItemsByCusine(menuList[currentIndex]['cuisine_id'].toString());
      if (res1 != null && res1 is Map && res1.containsKey('data')) {
        List? dList = res1['data'];
        if (dList != null) {
          if (dList.isNotEmpty) {
            int pos = menuList.indexWhere(
                    (element) => element['cuisine_id'] == dList[0]['cusineid']);
            menuList[pos]['dish_data'] = [];
            menuList[pos]['dish_data'].addAll(res1['data']);
          } else if (!menuList[currentIndex].containsKey('dish_data')) {
            menuList[currentIndex]['dish_data'] = [];
          }
        }
      }
      if (mounted) {
        setState(() {});
      }
    }
  }

  Future getOrderDetail() async {
    orderRes = null;
    Constant.placeitemlist.clear();
    Map oRes = await fetchOrder(
        widget.tblRes['table_id'],
        Constant.shopData!.id.toString(),
        Constant.shopData!.restUniqueId!,
        Constant.shopData!.brand_id!);
    if (oRes.containsKey('success')) {
      if (oRes['success'] == 1) {
        orderRes = oRes;
        List l = oRes['order_details'];
        l.forEach((element) {
          DishOrderPodo dp = DishOrderPodo(
              dishid: element['dish_id'].toString(),
              dishname: element['dish_name'],
              dishtype: element['dish_type'],
              qty: element['quantity'].toString(),
              price: element['price'],
              price_without_tax: element['price_without_tax'],
              priceperdish: element['price_per_dish'],
              priceper_without_tax: element['price_per_dish_without_tax'],
              preid: element['preferences_id'],
              isNew: false);
          if (element['preferences_data'] != null &&
              element['preferences_data'].toString().isNotEmpty) {
            dp.varPojoList = [];
            List<VariationPodo> vlist = [];
            json.decode(element['preferences_data']).forEach((v) {
              vlist.add(new VariationPodo.fromJson(v));
            });
            dp.varPojoList!.addAll(vlist);
          }
          if (element['taxes_data'] != null &&
              element['taxes_data'].toString().isNotEmpty) {
            List tlist = json.decode(element['taxes_data']);
            dp.tax_data = tlist;
          }
          Constant.placeitemlist.add(dp);
        });

        Provider.of<AppModel>(context, listen: false).increment();
      }
    }
    _cal();
  }

  void _cal() {
    double s_tot = 0.0, g_tot = 0.0;
    taxInvoiceList ??= [];
    taxInvoiceList!.clear();
    taxidlist ??= [];
    taxidlist!.clear();
    List<DishOrderPodo> dlist = [];
    if (Constant.dishorederlist.isNotEmpty) {
      dlist.addAll(Constant.dishorederlist);
    }
    if (Constant.placeitemlist.isNotEmpty) {
      dlist.addAll(Constant.placeitemlist);
    }
    if (dlist.isNotEmpty) {
      s_tot = dlist
          .map<double>((m) => (m.isNew)
              ? (int.parse(m.qty!) * double.parse(m.price_without_tax!))
              : double.parse(m.price_without_tax!))
          .reduce((a, b) => a + b);
      g_tot = dlist
          .map<double>((m) => (m.isNew)
              ? (int.parse(m.qty!) * double.parse(m.price!))
              : double.parse(m.price!))
          .reduce((a, b) => a + b);
      int dpos = 0;
      for (DishOrderPodo pojo in Constant.dishorederlist) {
        double tax_tot = 0;
        List? tlist = pojo.tax_data;
        if (pojo.tot_tax != null) tax_tot = double.parse(pojo.tot_tax!);
        int qty = int.parse(pojo.qty!);
        String? tt = pojo.tax_amt;
        if (tt == null || tt.trim().isEmpty) tt = "0";
        double diff = qty * (double.parse(tt));
        if (tlist != null && tlist.isNotEmpty) {
          int tpos = 0;
          for (Map tax in tlist) {
            TaxInvoicePodo tp = TaxInvoicePodo();
            double d = (diff * double.parse(tax['value'].toString())) / tax_tot;
            if (taxidlist!.contains(tax['id'].toString())) {
              int p = taxidlist!.indexOf(tax['id'].toString());
              tp = taxInvoiceList![p];
              double pt = double.parse(tp.per_total!) +
                  double.parse(tax['value'].toString());
              tp.per_total = pt.toString();
              double tdiff = double.parse(tp.amount_total!) + d;
              tax['tax_value'] = d;
              tp.amount_total = tdiff.toString();
              taxInvoiceList![p] = tp;
            } else {
              tp.id = tax['id'].toString();
              tp.name = tax['text'];
              tp.type = tax['tax_amount'].toString();
              tp.per = tax['value'].toString();
              tp.per_total = tax['value'].toString();
              tax['tax_value'] = d.toString();
              tp.amount_total = d.toString();
              taxInvoiceList!.add(tp);
              taxidlist!.add(tax['id'].toString());
            }
            tlist[tpos] = tax;
            tpos++;
          }
          DishOrderPodo dishOrderPojo = Constant.dishorederlist[dpos];
          dishOrderPojo.tax_data = tlist;
          Constant.dishorederlist[dpos] = dishOrderPojo;
        }
        dpos++;
      }

      if (Constant.placeitemlist.isNotEmpty) {
        Constant.placeitemlist.forEach((element) {
          if (element.tax_data != null && element.tax_data!.isNotEmpty) {
            element.tax_data!.forEach((tax) {
              TaxInvoicePodo tp = TaxInvoicePodo();
              if (taxidlist!.contains(tax['tax_id'].toString())) {
                int p = taxidlist!.indexOf(tax['tax_id'].toString());
                tp = taxInvoiceList![p];
                double tdiff = double.parse(tp.amount_total!) +
                    double.parse(tax['tax_value']);
                tp.amount_total = tdiff.toString();
                taxInvoiceList![p] = tp;
              } else {
                tp.id = tax['tax_id'].toString();
                tp.name = tax['tax_name'];
                tp.per = tax['tax_per'].toString();
                tp.per_total = tax['tax_per'].toString();
                double d = double.parse(tax['tax_value']);
                tp.amount_total = d.toString();
                taxInvoiceList!.add(tp);
                taxidlist!.add(tax['tax_id'].toString());
              }
            });
          }
        });
      }

      subtot = s_tot;
      grand = g_tot;
    } else {
      subtot = 0.0;
      grand = 0.0;
    }

    if (mounted) {
      setState(() {});
    }
  }

  Future checkoutClick({String? custNm,String? custPh}) async {
    CustomUI.showProgress(context);
    List olist = [];
    Constant.dishorederlist.forEach((element) {
      double amt = double.parse(element.qty!) * double.parse(element.price!);
      double amtwt =
          double.parse(element.qty!) * double.parse(element.price_without_tax!);
      List plist = [], tlist = [];

      if (element.varPojoList != null && element.varPojoList!.isNotEmpty) {
        for (VariationPodo v in element.varPojoList!) {
          Map o = {
            'amount': double.parse(v.amount!).toStringAsFixed(decimal_val),
            'amount_wt':
                double.parse(v.amount_wt!).toStringAsFixed(decimal_val),
            'id': v.id,
            'name': v.name
          };
          plist.add(o);
        }
      }
      if (element.tax_data != null && element.tax_data!.isNotEmpty) {
        for (Map t in element.tax_data!) {
          Constant.showLog(t);
          Map tx = {
            'tax_value': double.parse(t['tax_value'].toString())
                .toStringAsFixed(decimal_val),
            'tax_id': t['id'],
            'tax_per': t['value'],
            'tax_name': t['text']
          };
          tlist.add(tx);
        }
      }
      Map item = {
        'quantity': element.qty,
        'dishid': element.dishid,
        'price': amt.toStringAsFixed(decimal_val),
        'price_without_tax': amtwt.toStringAsFixed(decimal_val),
        'price_per_dish':
            double.parse(element.priceperdish!).toStringAsFixed(decimal_val),
        'price_per_dish_without_tax':
            double.parse(element.priceper_without_tax!)
                .toStringAsFixed(decimal_val),
        'dish_comment':
            (element.dish_comment != null) ? element.dish_comment : '',
        'weight': '',
        'unit_id': '',
        'preferencesid': (element.preid != null) ? element.preid : '',
        'taxes_data': tlist,
        'preferences_data': plist
      };
      olist.add(item);
    });

    Map param = {
      'restaurant_id': Constant.shopData!.id!,
      'rest_unique_id': Constant.shopData!.restUniqueId!,
      'orderid': orderRes!['order_id'].toString(),
      'order': olist,
      'total_amt': subtot.toStringAsFixed(decimal_val),
      'grand_total': grand.toStringAsFixed(decimal_val),
      'rounding_json': '',
      'taxes': []
    };
    if(custNm!=null) {
      param['cust_name']=custNm;
    }
    if(custPh!=null) {
      param['cust_phone']=custPh;
    }

    Map res = await updateOrder(param, Constant.shopData!.brand_id!);
    CustomUI.hideProgress(context);
    if (res.containsKey('status') && res['status'] == 200) {
      if (res.containsKey('success')) {
        if (res['success'] == 1) {
          showToast("Order placed");
          Constant.dishorederlist.clear();
          Constant.selidlist.clear();
          Constant.placeitemlist.clear();
          getOrderDetail();
        } else if (res['success'] == 2) {
          Get.offNamed(RouteHelper.getSplash(KeyConstant.query));
        }
      }
    } else if (res.containsKey('message')) {
      showToast(res['message']);
    }
  }

  void selCat(int index) {
    currentIndex = index;
    _scrollController.jumpTo(index: index);
    if (!menuList[currentIndex].containsKey('dish_data')) getDish();
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    decimal_val = int.parse(Constant.shopData!.decimalValue!);
    currency = Constant.currency;
    getMenu();
    Future.delayed(const Duration(milliseconds: 50), () {
      getOrderDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(Constant.shopData!.name!),
            if (widget.tblRes.containsKey('table_nm'))
              Text(
                widget.tblRes['table_nm'],
                style: const TextStyle(
                    fontWeight: FontWeight.normal, fontSize: 14),
              )
          ],
        ),
        centerTitle: false,
        actions: [
          if(orderRes!=null)
            TextButton(
              onPressed: () async {
                await Get.toNamed(RouteHelper.getDineInCart(orderRes!));
                _cal();
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconBadge(
                    clr: Theme.of(context).appBarTheme.titleTextStyle!.color,
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    'View Cart',
                    style: TextStyle(
                        color: Theme.of(context)
                            .appBarTheme
                            .titleTextStyle!
                            .color),
                  )
                ],
              )),
        ],
      ),
      body: (menuRes == null)
          ? CustomUI.CustProgress(context)
          : (menuRes!.containsKey('success') &&
                  menuRes!['success'] == Constant.api_no_net)
              ? NoInternetDialog(
                  retryClick: () {
                    if (mounted) {
                      setState(() {
                        menuRes = null;
                        getMenu();
                      });
                    }
                  },
                )
              : (menuRes!.containsKey('success') &&
                      menuRes!['success'] == Constant.api_catch_error)
                  ? Center(
                      child: Text(menuRes!['message']),
                    )
                  : (menuList.isNotEmpty)
                      ? Column(
                          children: [
                            Container(
                              color: mainClr.withOpacity(0.05),
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 6,
                                    child: TextFormField(
                                      readOnly: true,
                                      onTap: () async {
                                        await Get.toNamed(
                                            RouteHelper.getSearchItemRoute(
                                                Constant.shopData!.toJson()));
                                        _cal();
                                      },
                                      decoration: InputDecoration(
                                        prefixIcon: Image.asset(
                                          'assets/ic_search.png',
                                          height: 13,
                                          width: 13,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary,
                                        ),
                                        hintText: 'Search Item',
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            borderSide:
                                                BorderSide(color: mainClr)),
                                        errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            borderSide: const BorderSide(
                                                color: Colors.red)),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary)),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: InkWell(
                                      onTap: () => searchCat(),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: mainClr, width: 1),
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(5)),
                                        ),
                                        padding: const EdgeInsets.all(10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Image.asset(
                                              'assets/ic_restaurant.png',
                                              color: mainClr,
                                              width: 20,
                                            ),
                                            const SizedBox(
                                              width: 5,
                                            ),
                                            Text('Menu',
                                                style: TextStyle(
                                                    color: mainClr,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w300))
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width,
                                      child: ScrollablePositionedList.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemScrollController: _scrollController,
                                        itemCount: menuList.length,
                                        itemBuilder: (context, index) {
                                          return Row(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  selCat(index);
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      border: Border(
                                                          bottom: BorderSide(
                                                              color:
                                                                  (currentIndex ==
                                                                          index)
                                                                      ? mainClr
                                                                      : Colors
                                                                          .grey,
                                                              width:
                                                                  (currentIndex ==
                                                                          index)
                                                                      ? 2
                                                                      : 1))),
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          20, 10, 20, 10),
                                                  child: Text(
                                                    menuList[index]
                                                        ['cuisine_name'],
                                                    style: const TextStyle(
                                                        fontSize: 18),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                    if (currentIndex > -1)
                                      (menuList[currentIndex]
                                              .containsKey('dish_data'))
                                          ? Flexible(
                                              child: CategoryItemRow(
                                                  om: menuList[currentIndex],
                                                  currentOutlet:
                                                      Constant.shopData!,
                                                  cartClick: () {},
                                                  refreshClick: () {
                                                    _cal();
                                                  },
                                                  isCaptoWeb: false))
                                          : Expanded(
                                              flex: 1,
                                              child: Center(
                                                  child: CustomUI.CustProgress(
                                                      context))),
                                  ],
                                )),
                            Container(
                              padding: const EdgeInsets.all(10),
                              color:
                                  Theme.of(context).appBarTheme.backgroundColor,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Bill Amount",
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .appBarTheme
                                                  .titleTextStyle!
                                                  .color),
                                        ),
                                        Text(
                                          currency +
                                              grand
                                                  .toStringAsFixed(decimal_val),
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Theme.of(context)
                                                  .appBarTheme
                                                  .titleTextStyle!
                                                  .color,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                  if (Constant.dishorederlist.isNotEmpty)
                                    Expanded(
                                      child: TextButton(
                                        onPressed: () async{
                                          if(Constant.placeitemlist.isNotEmpty){
                                            checkoutClick();
                                          }else{
                                            dynamic r=await showDialog(context: context, builder: (BuildContext context){
                                              return CustomerDialog();
                                            });
                                            if(r!=null && r is Map){
                                              checkoutClick(custNm:r['name'],custPh:r['phone']);
                                            }
                                          }
                                        },
                                        child: Container(
                                          constraints: const BoxConstraints(
                                              minHeight: 40),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.white54,
                                                  width: 2),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(5))),
                                          child: const Text(
                                            'PlaceOrder',
                                            style: TextStyle(color: BTNTXTCLR),
                                          ),
                                        ),
                                      ),
                                      flex: 1,
                                    )
                                ],
                              ),
                            ),
                          ],
                        )
                      : const SizedBox(),
    );
  }

  Future searchCat() async {
    dynamic r = await showDialog(
      context: context,
      builder: (BuildContext context) {
        List<Widget> rows = [];
        for (var om in menuList) {
          rows.add(Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              InkWell(
                onTap: () async {
                  Get.back(result: om);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(10.0),
                  child: Text(om['cuisine_name'].toString(),
                      style: const TextStyle(
                        fontSize: 17,
                      )),
                ),
              ),
              const Divider(
                height: 1,
              )
            ],
          ));
        }
        return AlertDialog(
          title: const Text(
            'Choose Category',
            textAlign: TextAlign.center,
          ),
          content: SingleChildScrollView(
            child: Column(
              children: rows,
            ),
          ),
          actions: [
            TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: const Text('Close'))
          ],
        );
      },
    );
    if (r != null && r is Map) {
      int index = menuList.indexWhere((element) =>
          element['cuisine_id'].toString() == r['cuisine_id'].toString());
      selCat(index);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
