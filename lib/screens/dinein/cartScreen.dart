import 'package:capto/DBOrder.dart';
import 'package:capto/apis/dataAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/TaxInvoicePodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/screens/dialogLayout/variationDialog.dart';
import 'package:capto/ui/cartItemRow.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  Map orderRes;

  CartScreen({Key? key, required this.orderRes}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  List<TaxInvoicePodo>? taxInvoiceList;
  List<String>? taxidlist;
  int decimal_val = 2;
  String currency = '', orderId = '';
  double subtot = 0.0, grand = 0.0;
  List<DishOrderPodo> dlist = [];

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  Future orderdetail() async {
    Map oRes = widget.orderRes;
    orderId = oRes['order_id'].toString();
    if (mounted) {
      setState(() {});
    }
  }

  Future _getItemDetail(
      String id, String brandid, String restid, String runiqid, int pos) async {
    Map data = await getItemDetail(restid, runiqid, id, brandid);
    if (data.containsKey("success") && data['success'] == 101) {
      CustomUI.customAlert('', "No Internet Connection", context);
    } else if (data['success'] == 1) {
      dynamic d_result = await showDialog(
        context: context,
        builder: (_) => DialogVariation(
          item: data,
          currentOutlet: Constant.shopData!,
          isUpdate: true,
          updatePOS: pos,
        ),
      );

      if (d_result != null && d_result is bool) {
        if (d_result) Provider.of<AppModel>(context, listen: false).increment();
        if (mounted) {
          setState(() {});
        }
      }
    }
  }

  void _removeItemAlert(DishOrderPodo d, int index) {
    CustomUI.customAlertDialog(
        "Remove Item?", "You want to remove " + d.dishname! + " from cart?",
        () async {
      await DBProvider().removeItemById(index, d.orderdetailid!);
      Constant.dishorederlist.removeAt(index);
      Constant.selidlist.removeAt(index);
      Get.back();
      _cal();
    }, () {
      Get.back();
    }, context);
  }

  Widget _setTax() {
    if (taxInvoiceList != null && taxInvoiceList!.isNotEmpty) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          primary: false,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: taxInvoiceList == null ? 0 : taxInvoiceList!.length,
          itemBuilder: (BuildContext context, int index) {
            TaxInvoicePodo p = taxInvoiceList![index];
            return calRow(
                p.name! +
                    " (" +
                    double.parse(p.per!).toStringAsFixed(decimal_val) +
                    "%)",
                currency +
                    double.parse(p.amount_total!).toStringAsFixed(decimal_val));
          });
    } else {
      return SizedBox();
    }
  }

  void _cal() {
    double s_tot = 0.0, g_tot = 0.0;
    taxInvoiceList ??= [];
    taxInvoiceList!.clear();
    taxidlist ??= [];
    taxidlist!.clear();
    List<DishOrderPodo> dlist = [];
    if (Constant.dishorederlist.isNotEmpty)
      dlist.addAll(Constant.dishorederlist);
    if (Constant.placeitemlist.isNotEmpty) {
      dlist.addAll(Constant.placeitemlist);
    }
    if (dlist.isNotEmpty) {
      s_tot = dlist
          .map<double>((m) => (m.isNew)
              ? (int.parse(m.qty!) * double.parse(m.price_without_tax!))
              : double.parse(m.price_without_tax!))
          .reduce((a, b) => a + b);
      g_tot = dlist
          .map<double>((m) => (m.isNew)
              ? (int.parse(m.qty!) * double.parse(m.price!))
              : double.parse(m.price!))
          .reduce((a, b) => a + b);
      int dpos = 0;
      for (DishOrderPodo pojo in Constant.dishorederlist) {
        double tax_tot = 0;
        List? tlist = pojo.tax_data;
        if (pojo.tot_tax != null) tax_tot = double.parse(pojo.tot_tax!);
        int qty = int.parse(pojo.qty!);
        String? tt = pojo.tax_amt;
        if (tt == null || tt.trim().isEmpty) tt = "0";
        double diff = qty * (double.parse(tt));
        if (tlist != null && tlist.isNotEmpty) {
          int tpos = 0;
          for (Map tax in tlist) {
            TaxInvoicePodo tp = TaxInvoicePodo();
            double d = (diff * double.parse(tax['value'].toString())) / tax_tot;
            if (taxidlist!.contains(tax['id'].toString())) {
              int p = taxidlist!.indexOf(tax['id'].toString());
              tp = taxInvoiceList![p];
              double pt = double.parse(tp.per_total!) +
                  double.parse(tax['value'].toString());
              tp.per_total = pt.toString();
              double tdiff = double.parse(tp.amount_total!) + d;
              tax['tax_value'] = d;
              tp.amount_total = tdiff.toString();
              taxInvoiceList![p] = tp;
            } else {
              tp.id = tax['id'].toString();
              tp.name = tax['text'];
              tp.type = tax['tax_amount'].toString();
              tp.per = tax['value'].toString();
              tp.per_total = tax['value'].toString();
              tax['tax_value'] = d.toString();
              tp.amount_total = d.toString();
              taxInvoiceList!.add(tp);
              taxidlist!.add(tax['id'].toString());
            }
            tlist[tpos] = tax;
            tpos++;
          }
          DishOrderPodo dishOrderPojo = Constant.dishorederlist[dpos];
          dishOrderPojo.tax_data = tlist;
          Constant.dishorederlist[dpos] = dishOrderPojo;
        }
        dpos++;
      }

      if (Constant.placeitemlist.isNotEmpty) {
        Constant.placeitemlist.forEach((element) {
          if (element.tax_data != null && element.tax_data!.isNotEmpty) {
            element.tax_data!.forEach((tax) {
              TaxInvoicePodo tp = TaxInvoicePodo();
              if (taxidlist!.contains(tax['tax_id'].toString())) {
                int p = taxidlist!.indexOf(tax['tax_id'].toString());
                tp = taxInvoiceList![p];
                double tdiff = double.parse(tp.amount_total!) +
                    double.parse(tax['tax_value']);
                tp.amount_total = tdiff.toString();
                taxInvoiceList![p] = tp;
              } else {
                tp.id = tax['tax_id'].toString();
                tp.name = tax['tax_name'];
                tp.per = tax['tax_per'].toString();
                tp.per_total = tax['tax_per'].toString();
                double d = double.parse(tax['tax_value']);
                tp.amount_total = d.toString();
                taxInvoiceList!.add(tp);
                taxidlist!.add(tax['tax_id'].toString());
              }
            });
          }
        });
      }

      subtot = s_tot;
      grand = g_tot;
    } else {
      subtot = 0.0;
      grand = 0.0;
    }

    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    decimal_val = int.parse(Constant.shopData!.decimalValue!);
    currency = Constant.currency;
    orderdetail();
    Future.delayed(Duration(milliseconds: 20), () {
      _cal();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Expanded(
                      child: SingleChildScrollView(
                    child: Column(
                      children: [
                        if (Constant.placeitemlist.isNotEmpty) _setPlaceItems(),
                        _setItems(),
                      ],
                    ),
                  )),
                  calRow("Sub Total",
                      currency + subtot.toStringAsFixed(decimal_val)),
                  _setTax(),
                  calRow("Bill Amount",
                      currency + grand.toStringAsFixed(decimal_val)),
                  const SizedBox(
                    height: 20,
                  ),
                  SwiftoBtn('Back To Menu', () {
                    Get.back();
                  })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setPlaceItems() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: Constant.placeitemlist.length,
      itemBuilder: (BuildContext context, int index) {
        DishOrderPodo podo = Constant.placeitemlist[index];
        return CartItems(
          currency: Constant.currency,
          data: podo,
          isEditable: false,
          clickInc: () async {},
          clickDec: () async {},
          clickRemove: () {},
          clickUpdate: () {},
        );
      },
    );
  }

  Widget _setItems() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: Constant.dishorederlist.length,
      itemBuilder: (BuildContext context, int index) {
        DishOrderPodo podo = Constant.dishorederlist[index];
        return CartItems(
          currency: Constant.currency,
          data: podo,
          isEditable: true,
          clickInc: () async {
            DishOrderPodo d = Constant.dishorederlist[index];
            int q = int.parse(d.qty.toString()) + 1;
            d.qty = q.toString();
            await DBProvider().updateQty(index, d.qty!, d.orderdetailid!);
            Constant.dishorederlist[index] = d;
            _cal();
          },
          clickDec: () async {
            DishOrderPodo d = Constant.dishorederlist[index];
            int q = int.parse(d.qty.toString()) - 1;
            if (q != 0) {
              d.qty = q.toString();
              await DBProvider().updateQty(index, d.qty!, d.orderdetailid!);
              Constant.dishorederlist[index] = d;
            } else {
              _removeItemAlert(d, index);
            }
            _cal();
          },
          clickRemove: () {
            DishOrderPodo d = Constant.dishorederlist[index];
            _removeItemAlert(d, index);
          },
          clickUpdate: () {
            if (podo.preflag != null &&
                podo.preflag!.isNotEmpty &&
                podo.preflag == 'true') {
              _getItemDetail(podo.dishid!, podo.brandid!, podo.rest_id!,
                  podo.rest_uniq_id!, index);
            }
          },
        );
      },
    );
  }

  Widget calRow(String key, String val) {
    return Container(
      margin: EdgeInsets.only(bottom: 5, left: 10, right: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[Text(key), Text(val)],
      ),
    );
  }
}
