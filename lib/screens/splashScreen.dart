import 'package:capto/apis/dataAPI.dart';
import 'package:capto/apis/dineInAPI.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/screens/dialogLayout/pinDialog.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  String? queryParam, tblId;

  SplashScreen({Key? key, this.queryParam, this.tblId}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLocationPermission = false, openSetting = false;
  String? ty;

  void checkUri() async {
    Constant.placeitemlist.clear();
    if (widget.queryParam != null && widget.queryParam!.isNotEmpty) {
      Outlet o = await getQROutletDetail(widget.queryParam!);
      if (o != null && o.success == 1) {
        KeyConstant.query = widget.queryParam!;
        await KeyConstant.saveOutletInfo(o, 1, o.brand_id!);
        Constant.shopData = o;
        Constant.currency = o.currency!;
        if (widget.tblId != null) {
          KeyConstant.query = KeyConstant.query + '/' + widget.tblId!;
          Map res = await createTblPin(
              widget.tblId!, o.id.toString(), o.restUniqueId!, o.brand_id!);
          if (res.containsKey('success')) {
            if (res['success'] == 1) {
              if (o.captoDineinAskPin != null &&
                  o.captoDineinAskPin == 'enable') {
                await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return PinDialog(
                        tblid: widget.tblId!,
                        tblNm: res['table_name'],
                      );
                    });
              } else {
                //check table already open or not
                if (res.containsKey('order_id') && res['order_id'] != 0) {
                  Map param = {
                    'table_id': widget.tblId,
                    'table_nm': res['table_name'],
                  };
                  Get.offNamed(RouteHelper.getPlaceOrder(param));
                } else {
                  CustomUI.showProgress(context);
                  Map sRes = await startOrder(
                      widget.tblId!,
                      Constant.shopData!.id.toString(),
                      Constant.shopData!.restUniqueId!,
                      Constant.shopData!.brand_id!,
                      '');
                  CustomUI.hideProgress(context);
                  if (sRes.containsKey('success')) {
                    if (sRes['success'] == 1) {
                      sRes['table_id'] = widget.tblId;
                      Get.offNamed(RouteHelper.getPlaceOrder(sRes));
                    }
                  }
                }
              }
            }
          }
        } else {
          Get.offNamed(RouteHelper.getOutletDetail());
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    checkUri();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        body: buildSplashScreen(context),
      ),
    );
  }

  Widget buildSplashScreen(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage((Theme.of(context).brightness == Brightness.dark)
              ? 'assets/splash_dark.jpg'
              : 'assets/splash_light.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(),
    );
  }
}
