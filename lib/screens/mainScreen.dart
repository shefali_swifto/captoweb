import 'dart:io';

import 'package:capto/apis/dataAPI.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/screens/addAddressScreen.dart';

import 'package:capto/screens/cartTab.dart';
import 'package:capto/screens/guestUserScreen.dart';
import 'package:capto/screens/homeTab.dart';
import 'package:capto/screens/orderTab.dart';
import 'package:capto/screens/profileTab.dart';
import 'package:capto/screens/searchScreen.dart';
import 'package:capto/ui/swiftoBadge.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  bool isLogin = false;
  PageController? _pageController;
  final ItemScrollController _scrollController = ItemScrollController();
  int sel_page = 0;
  FirebaseHelper? firebaseHelper;
  Map? res;
  List catList = [];
  Outlet? selOutlet;
  TextEditingController deliveryTypeCtrl = TextEditingController();

  _checkLoginUser() async {
    isLogin = await KeyConstant.isUserLogin();
    if (mounted) {
      setState(() {
        _getmenuItems();
      });
    }
  }

  _getmenuItems() async {
    selOutlet = Constant.shopData;
    // await getOutletDetail(
    //     Constant.shopData!.id.toString(), Constant.shopData!.brand_id!);
    // if(selOutlet!=null){
    //   selOutlet!.brand_id=Constant.shopData!.brand_id!;
    // }
    res = await getAllCuisine();
    catList = [];
    catList.clear();
    if (res != null && res!.containsKey('success')) {
      if (res!['success'] == 1) {
        Provider.of<AppModel>(context, listen: false).changeTab(0);
        catList = res!['data'];
        //  Constant.currency = Constant.shopData!.currency!;
        if (mounted) {
          setState(() {});
        }
      }
    }
  }

  @override
  void initState() {
    if (Constant.deliveryType != null)
      deliveryTypeCtrl.text = Constant.deliveryType!;
    super.initState();
    _checkLoginUser();
    _pageController = PageController(initialPage: 0);
    firebaseHelper = FirebaseHelper(context);
    deliveryTypeCtrl.addListener(() {
      Constant.deliveryType = deliveryTypeCtrl.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    sel_page = Provider.of<AppModel>(context).currentTab;
    return (res == null)
        ? CustomUI.CustProgress(context)
        : (res != null &&
                res!.containsKey('success') &&
                res!['success'] == Constant.api_no_net)
            ? NoInternetDialog(
                retryClick: () {
                  getInfo();
                },
              )
            : (res != null &&
                    res!.containsKey('success') &&
                    res!['success'] == Constant.api_catch_error)
                ? Center(
                    child: Text(res!['message']),
                  )
                : WillPopScope(
                    onWillPop: () async {
                      if (sel_page == 0) {
                        exit(0);
                      } else {
                        _pageController!.jumpToPage(0);
                      }
                      return false;
                    },
                    child: Scaffold(
                      body: PageView(
                        physics: const NeverScrollableScrollPhysics(),
                        controller: _pageController,
                        onPageChanged: onPageChanged,
                        children: <Widget>[
                          HomeTab(
                            currentOutlet: selOutlet!,
                            catList: catList,
                            scrollController: _scrollController,
                            deliveryTypeCtrl: deliveryTypeCtrl,
                            changeTab: () {
                              navigationTapped(1);
                            },
                          ),
                          CartTab(
                            currentOutlet: selOutlet,
                            deliveryType: (deliveryTypeCtrl.text ==
                                    KeyConstant.type_delivery_txt)
                                ? KeyConstant.type_delivery
                                : KeyConstant.type_takeaway,
                          ),
                          SearchScreen(
                            currentOutlet: Constant.shopData!,
                            isTab: true,
                          ),
                          (isLogin)
                              ? OrderTab(
                                  parentObj: firebaseHelper!,
                                )
                              : GuestUser(
                                  action: 'order',
                                  title: 'Orders',
                                  loginSuccess: () {
                                    _checkLoginUser();
                                  },
                                ),
                          (isLogin)
                              ? ProfileTab()
                              : GuestUser(
                                  action: 'profile',
                                  title: 'Profile',
                                  loginSuccess: () {
                                    _checkLoginUser();
                                  },
                                ),
                        ],
                      ),
                      bottomNavigationBar: BottomAppBar(
                        color: Theme.of(context).primaryColor,
                        shape: const CircularNotchedRectangle(),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            const SizedBox(width: 7),
                            IconButton(
                              icon: Image.asset(
                                'assets/ic_home.png',
                                width: 18.0,
                              ),
                              color: sel_page == 0
                                  ? defaultClr
                                  : null,
                              onPressed: () => _pageController!.jumpToPage(0),
                            ),
                            IconButton(
                              icon: IconBadge(
                                clr: sel_page == 1
                                    ? defaultClr
                                    : null,
                              ),
                              color: sel_page == 1
                                  ? defaultClr
                                  : null,
                              onPressed: () => _pageController!.jumpToPage(1),
                            ),
                            IconButton(
                              icon: Image.asset(
                                'assets/ic_search.png',
                                width: 18,
                                color: Colors.transparent,
                              ),
                              color: sel_page == 2
                                  ? defaultClr
                                  : null,
                              onPressed: () {
                                // if (sel_page != 0) {
                                _pageController!.jumpToPage(2);
                                // } else {
                                // searchCat();
                                //}
                              },
                            ),
                            IconButton(
                              icon: Image.asset(
                                'assets/ic_history.png',
                                width: 18,
                              ),
                              color: sel_page == 3
                                  ? defaultClr
                                  : null,
                              onPressed: () => _pageController!.jumpToPage(3),
                            ),
                            IconButton(
                              icon: Image.asset(
                                'assets/ic_person.png',
                                width: 18,
                              ),
                              color: sel_page == 4
                                  ? defaultClr
                                  : null,
                              onPressed: () => _pageController!.jumpToPage(4),
                            ),
                            const SizedBox(width: 7),
                          ],
                        ),
                      ),
                      floatingActionButtonAnimator:
                          FloatingActionButtonAnimator.scaling,
                      floatingActionButtonLocation:
                          FloatingActionButtonLocation.centerDocked,
                      floatingActionButton: FloatingActionButton(
                        backgroundColor: defaultClr,
                        elevation: 4.0,
                        child: Image.asset('assets/ic_search.png',
                            width: 18, color: BTNTXTCLR),
                        onPressed: () {
                          _pageController!.jumpToPage(2);
                        },
                      ),
                    ));
  }

  void navigationTapped(int page) {
    _pageController!.jumpToPage(page);
  }

  @override
  void dispose() {
    _pageController!.dispose();
    deliveryTypeCtrl.dispose();
    super.dispose();
  }

  void onPageChanged(int page) {
    Provider.of<AppModel>(context, listen: false).changeTab(page);
    if (mounted) {
      setState(() {
        this.sel_page = page;
      });
    }
  }

// searchCat() {
//   showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       List list = [];
//       list.addAll(catItems);
//       const Align(alignment: Alignment.bottomCenter);
//       return AlertDialog(
//           titlePadding: const EdgeInsets.all(10),
//           contentPadding: const EdgeInsets.only(top: 10),
//           content: Container(
//             padding: const EdgeInsets.only(
//                 left: 10, right: 10, top: 10, bottom: 10),
//             height: MediaQuery.of(context).size.height / 2,
//             width: MediaQuery.of(context).size.width / 2,
//             child: ListView.builder(
//               scrollDirection: Axis.vertical,
//               shrinkWrap: true,
//               itemCount: list.length,
//               itemBuilder: (BuildContext context, int index) {
//                 Map om = list[index];
//                 return (om['cuisine_dish_count'] > 0)
//                     ? Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         children: <Widget>[
//                           Container(
//                             padding:
//                                 const EdgeInsets.only(left: 10, right: 10),
//                             child: InkWell(
//                               onTap: () async{
//                                 if(HomeTab.menuGlobalKey.currentState!.searchCtrl.text.isNotEmpty) {
//                                   HomeTab.menuGlobalKey.currentState!.resetSearch();
//                                   await Future.delayed(Duration(milliseconds: 100));
//                                 }
//                                 Get.back();
//                                 _scrollController.scrollTo(
//                                     index: index + 1,
//                                     duration:
//                                         const Duration(milliseconds: 500));
//                               },
//                               child: Container(
//                                 padding:
//                                     const EdgeInsets.only(top: 7, bottom: 7),
//                                 child: Row(
//                                   mainAxisAlignment:
//                                       MainAxisAlignment.spaceBetween,
//                                   children: <Widget>[
//                                     Text(
//                                         (om['cuisine_id'] == 0)
//                                             ? 'Popular Items'
//                                             : om['cuisine_name'],
//                                         style: const TextStyle(
//                                           fontSize: 17,
//                                         )),
//                                     Text(om['cuisine_dish_count'].toString(),
//                                         style: const TextStyle(
//                                             fontSize: 17,
//                                             fontWeight: FontWeight.normal)),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ],
//                       )
//                     : const SizedBox();
//               },
//             ),
//           ));
//     },
//   );
// }
}
