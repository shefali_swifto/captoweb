import 'package:capto/DBOrder.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/screens/dialogLayout/variationDialog.dart';
import 'package:capto/ui/swiftoBadge.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/ui/swiftoQty.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

//import '../DBOrder.dart';

class ItemDetail extends StatefulWidget {
  Map item;
  String restid, isPriceWT, outletStatus;
  Outlet currentOutlet;
  Function? changeTab;

  ItemDetail(
      {Key? key,
      required this.currentOutlet,
      required this.item,
      required this.restid,
      required this.isPriceWT,
      required this.outletStatus,
      this.changeTab})
      : super(key: key);

  @override
  _ItemDetailState createState() => _ItemDetailState();
}

class _ItemDetailState extends State<ItemDetail> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String img = "", nm = "", price = "", pricewt = "";
  int decimal_val = 2, qty = 1;
  bool diffoutlet = false, inCart = false;

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  addClick() {
    if (Constant.selidlist.length > 0) {
      if (Constant.dishorederlist[0].rest_id ==
              widget.currentOutlet.id.toString() &&
          Constant.dishorederlist[0].brandid == widget.currentOutlet.brand_id) {
        _checkVariation(widget.item);
      } else {
        _showAlert(widget.item);
      }
    } else {
      _checkVariation(widget.item);
    }
  }

  @override
  void initState() {
    super.initState();
    if (Constant.decimal_value != null && Constant.decimal_value.isNotEmpty) {
      decimal_val = int.parse(Constant.decimal_value);
    } else {
      decimal_val = 2;
    }
    if (widget.item.containsKey("dishimage_url")) {
      img = widget.item['dishimage_url'];
    } else if (widget.item.containsKey("img")) {
      img = widget.item['img'];
    }
    if (widget.item.containsKey("dishname")) {
      nm = widget.item['dishname'];
    } else if (widget.item.containsKey("name")) {
      nm = widget.item['name'];
    }
    if (widget.item.containsKey("price")) {
      price = widget.item['price'];
    } else {
      price = '0';
    }
    if (widget.item.containsKey('price_without_tax')) {
      pricewt = widget.item['price_without_tax'];
    } else
      pricewt = '0';
    if (Constant.selidlist.length > 0) {
      if (Constant.dishorederlist[0].rest_id != widget.restid) {
        diffoutlet = true;
      }
      if (!diffoutlet &&
          Constant.selidlist.contains(widget.item['dishid'].toString())) {
        qty = 0;
        Constant.dishorederlist.forEach((data1) {
          if (data1.dishid == widget.item['dishid'].toString()) {
            inCart = true;
            int q = int.parse(data1.qty!) + qty;
            qty = q;
          }
        });
      }
    }
    new FirebaseHelper(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    //name
                                    Expanded(
                                      child: Text(
                                        nm.trim(),
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    //price
                                    Text(
                                      (Constant.shopData != null &&
                                              Constant.shopData!
                                                      .showItemPriceWithoutTax !=
                                                  null &&
                                              Constant.shopData!
                                                      .showItemPriceWithoutTax ==
                                                  'true')
                                          ? Constant.currency +
                                              double.parse(pricewt)
                                                  .toStringAsFixed(decimal_val)
                                          : Constant.currency +
                                              double.parse(price)
                                                  .toStringAsFixed(decimal_val),
                                      style: TextStyle(fontSize: 16.0),
                                    ),
                                  ]),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 5),
                                    child: Text('QUANTITY:'),
                                  ),
                                  SwiftoQty(true, true, qty.toString(), () {
                                    if (mounted) {
                                      setState(() {
                                        qty = qty + 1;
                                      });
                                    }
                                  }, () {
                                    if (mounted) {
                                      setState(() {
                                        if (qty != 1) qty = qty - 1;
                                      });
                                    }
                                  }, null),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
//            description
                              Text(widget.item['description'])
                            ],
                          )),
//            add to cart button
                    ],
                  ),
                ),
              ),
              (widget.outletStatus != null && widget.outletStatus == 'close')
                  ? SizedBox()
                  : Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: SwiftoBtn1("Add to order".toUpperCase(), () {
                          addClick();
                        }),
                      ),
                    ),
            ],
          ),
        ],
      ),
    );
  }

  _checkVariation(Map featuredItems) async {
    if (featuredItems.containsKey("preflag") &&
        featuredItems['preflag'] == 'true') {
      bool d_result = await showDialog(
        context: context,
        builder: (_) => DialogVariation(
          item: featuredItems,
          currentOutlet: widget.currentOutlet,
          isUpdate: false,
          updatePOS: 0,
        ),
      );

      if (d_result) {
        Provider.of<AppModel>(context, listen: false).increment();
        if (mounted) {
          setState(() {});
        }
      }
    } else {
      if (inCart) {
        int pos =
            Constant.selidlist.indexOf(featuredItems['dishid'].toString());
        DishOrderPodo d = Constant.dishorederlist[pos];
        d.qty = qty.toString();
        await DBProvider().updateQty(pos, d.qty!, d.orderdetailid!);
        Constant.dishorederlist[pos] = d;
        showToast('Item Qty updated');
        Provider.of<AppModel>(context, listen: false).increment();
      } else {
        _addItemToCart(featuredItems);
      }
    }
  }

  _addItemToCart(Map featuredItems) async {
    DishOrderPodo d = new DishOrderPodo();
    d.dishid = featuredItems['dishid'].toString();
    d.cusineid = featuredItems['cusineid'].toString();
    d.dishname = featuredItems['dishname'];
    d.dishtype = featuredItems['type'];
    d.description = featuredItems['description'];
    d.price = featuredItems['price'].toString();
    d.priceperdish = featuredItems['price'].toString();
    d.dishimage = featuredItems['dishimage_url'];
    d.preflag = featuredItems['preflag'].toString();
    d.sold_by = featuredItems['sold_by'].toString();
    d.priceper_without_tax = featuredItems['price_without_tax'].toString();
    d.price_without_tax = featuredItems['price_without_tax'].toString();
    d.tax_amt = featuredItems['tax_amt'].toString();
    d.offer = featuredItems['offers'].toString();
    d.discount = featuredItems['discount_amount'].toString();
    d.combo_flag = featuredItems['combo_flag'].toString();
    d.qty = "1";
    d.brandid = Constant.shopData!.brand_id.toString();
    d.rest_id = Constant.shopData!.id.toString();
    d.rest_uniq_id = Constant.shopData!.restUniqueId.toString();
    d.rest_name = Constant.shopData!.name.toString();
    d.rest_img = Constant.shopData!.logo.toString();
    d.rest_address = Constant.shopData!.address.toString();
    List tax_list = [];
    if (featuredItems.containsKey('tax_data')) {
      tax_list = featuredItems['tax_data'];
      if (tax_list != null && tax_list.length > 0) {
        double tot_tax = tax_list
            .map<double>((m) => double.parse(m['value']))
            .reduce((a, b) => a + b);
        d.tot_tax = tot_tax.toString();
      } else {
        d.tot_tax = '0';
      }
    } else {
      d.tot_tax = '0';
    }
    d.tax_data = tax_list;
    d.pre = featuredItems['pre'];
    d.preid = "";
    d.dish_comment = "";

    int r = await DBProvider().addOrderDetail(d);
    d.orderdetailid = r.toString();
    Constant.selidlist.add(featuredItems['dishid'].toString());
    Constant.dishorederlist.add(d);
    showToast('Item Added to cart');
    Provider.of<AppModel>(context, listen: false).increment();
    if (mounted) {
      setState(() {});
    }
  }

  _showAlert(Map featuredItems) {
    CustomUI.customAlertDialog(
        "Remove Item?",
        "Your cart contains items from " +
            Constant.dishorederlist[0].rest_name! +
            ". Do you want to discard the " +
            "selection and add items from " +
            widget.currentOutlet.name! +
            "?", () async {
      await DBProvider().deleteAll();
      if (mounted) {
        setState(() {
          Constant.dishorederlist.clear();
          Constant.selidlist.clear();
          Get.back();
          _checkVariation(featuredItems);
        });
      }
    }, () {
      Get.back();
    }, context);
  }
}
