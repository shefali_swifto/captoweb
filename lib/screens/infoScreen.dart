import 'package:capto/routes/route_helper.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InfoScreen extends StatefulWidget {
  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  @override
  void initState() {
    super.initState();
    new FirebaseHelper(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(Constant.appName),
        //elevation: 0.0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            ListTile(
              title: const Text(
                "Terms & Conditions",
                style: const TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              trailing: Image.asset(
                'assets/ic_next.png',
                width: 18,
              ),
              onTap: () {
                Get.toNamed(RouteHelper.getInfoDetailRoute('terms'));
              },
            ),
            const Divider(),
            ListTile(
              title: const Text(
                "Privacy Policy",
                style: const TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              trailing: Image.asset(
                'assets/ic_next.png',
                width: 18,
              ),
              onTap: () {
                Get.toNamed(RouteHelper.getInfoDetailRoute('policy'));
              },
            ),
            const Divider(),
            ListTile(
              title: const Text(
                "Contact us",
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              trailing: Image.asset(
                'assets/ic_next.png',
                width: 18,
              ),
              onTap: () {
                Get.toNamed(RouteHelper.getInfoDetailRoute('contact'));
              },
            ),
            const Divider(),
          ],
        ),
      ),
    );
  }
}
