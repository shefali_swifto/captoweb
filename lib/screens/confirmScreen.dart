import 'dart:convert';

import 'package:capto/DBOrder.dart';
import 'package:capto/apis/orderAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/TaxInvoicePodo.dart';
import 'package:capto/models/VariationPodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';

import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/cartItemRow.dart';
import 'package:capto/ui/home_screen.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:capto/util/styles.dart';

// import 'package:stripe_checkout/stripe_checkout.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:razorpay_web/razorpay_web.dart';

class ConfirmScreen extends StatefulWidget {
  Outlet outletData;
  Map cartData;

  ConfirmScreen({Key? key, required this.cartData, required this.outletData})
      : super(key: key);

  @override
  _ConfirmScreenState createState() => _ConfirmScreenState();
}

class _ConfirmScreenState extends State<ConfirmScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String round_amt = "",
      pay_amt = "",
      redeem_point = "",
      rp_amt = "",
      per_point = "",
      brandid = "";
  List<Map> plist = [], tlist = [];
  double tot_tax = 0.0, dis_per = 0.0, dis_amt = 0.0;
  Map? transPojo, orderJson, offer;
  Razorpay? _razorpay;
  bool isCOD = false, allowOnlinePay = false;
  BuildContext? ctx;
  String? subtot = '0.0',
      grand = '0.0',
      roundingJSON,
      pointJSON,
      service_charge,
      packaging_charge,
      note,
      points,
      delivery_type,
      pay,
      is_advanceorder,
      pick_dt,
      currency = '',
      restNote;
  List<TaxInvoicePodo> taxInvoiceList = [];
  int decimal_val = 2;

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  @override
  void initState() {
    super.initState();
    new FirebaseHelper(context);
    brandid = Constant.dishorederlist[0].brandid!;
    subtot = widget.cartData['subtot'];
    grand = widget.cartData['grand'];
    roundingJSON = widget.cartData['roundingJSON'];
    pointJSON = widget.cartData['pointJSON'];
    service_charge = widget.cartData['service_charge'];
    packaging_charge = widget.cartData['packaging_charge'];
    note = widget.cartData['note'];
    points = widget.cartData['points'];
    delivery_type = widget.cartData['delivery_type'];
    pay = widget.cartData['pay'];
    is_advanceorder = widget.cartData['is_advanceorder'];
    pick_dt = widget.cartData['pick_dt'];
    currency = widget.cartData['currency'];
    restNote = widget.cartData['restNote'];
    if (widget.outletData.razorpayEnable == 'true')
      allowOnlinePay = true;
    else if (widget.outletData.stripeEnable == 'true')
      allowOnlinePay = true;
    else if (widget.outletData.paypalEnable == 'true') allowOnlinePay = true;
    // if(widget.outletData.mpesaEnable=='true')
    //   allowOnlinePay=true;
    // if(widget.outletData.vivawalletEnable=='true')
    //   allowOnlinePay=true;
    Constant.showLog('onlinepay:${allowOnlinePay}');
    offer = widget.cartData['offer'];
    dis_per = widget.cartData['dis_per'];
    dis_amt = widget.cartData['dis_amt'];
    if (widget.cartData['taxInvoiceList'] != null) {
      List l = widget.cartData["taxInvoiceList"];
      l.forEach((e) => taxInvoiceList.add(TaxInvoicePodo.fromJson(e)));
    }
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    if (roundingJSON != null && roundingJSON!.isNotEmpty) {
      Map je = json.decode(roundingJSON!);
      if (je.containsKey("cash_rounding"))
        round_amt = je['cash_rounding'].toString();
    }
    if (pointJSON != null && pointJSON!.isNotEmpty) {
      Map jp = json.decode(pointJSON!);
      redeem_point = jp['redeem_points'].toString();
      rp_amt = jp['redeem_amount'].toString();
      per_point = jp['points_per_one_currency'].toString();
    }
    pay_amt = pay!;
    String dt = '';
    // if (widget.is_advanceorder != null &&
    //     widget.is_advanceorder.isNotEmpty &&
    //     widget.is_advanceorder == 'yes') {
    if (pick_dt != null && pick_dt!.isNotEmpty) {
      DateFormat defaultfmt = new DateFormat("yyyy-MM-dd HH:mm:ss");
      DateFormat dtfmt = new DateFormat("dd MMM yyyy hh:mm aaa");
      DateTime oDateTime = defaultfmt.parse(pick_dt!);
      dt = dtfmt.format(oDateTime);
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Confirm Order"),
        elevation: 0.0,
      ),
      body: Column(
        children: <Widget>[
          Flexible(
              child: Container(
                margin: const EdgeInsets.only(left: 10, right: 10),
                padding: const EdgeInsets.all(10),
                color: Theme.of(context).cardColor,
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        (pick_dt != null && pick_dt!.isNotEmpty)
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text((delivery_type ==
                                          KeyConstant.type_takeaway)
                                      ? "\nPickup DateTime"
                                      : "\nDelivery DateTime"),
                                  Text("\n" + dt)
                                ],
                              )
                            : const SizedBox(),
                        (delivery_type == KeyConstant.type_delivery)
                            ? const Text(
                                "Shipping Address",
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              )
                            : const SizedBox(),
                        (delivery_type == KeyConstant.type_delivery &&
                                Constant.sel_address != null)
                            ? ListTile(
                                title: Text(
                                  Constant.sel_address!['address_tag'],
                                ),
                                subtitle:
                                    Text(addressString(Constant.sel_address!)),
                              )
                            : const SizedBox(),
                        const SizedBox(height: 10.0),
                      ],
                    ),
                    _setItems(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    //      Divider(),
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Text("Delivery type"),
                              Text((delivery_type == KeyConstant.type_delivery)
                                  ? 'DELIVERY'
                                  : 'PICKUP')
                            ],
                          ),
                          const SizedBox(
                            height: 7.0,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Text("Sub Total"),
                              Text(currency! + subtot!)
                            ],
                          ),
                          _setTax(),
                          const SizedBox(
                            height: 7.0,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Text("Packaging Charge"),
                              (packaging_charge != null &&
                                      packaging_charge!.trim().length > 0)
                                  ? Text(currency! +
                                      double.parse(packaging_charge!)
                                          .toStringAsFixed(decimal_val))
                                  : Text(currency! + "0.00")
                            ],
                          ),
                          const SizedBox(
                            height: 7.0,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Text("Delivery Charge"),
                              (service_charge != null &&
                                      service_charge!.trim().length > 0)
                                  ? Text(currency! +
                                      double.parse(service_charge!)
                                          .toStringAsFixed(decimal_val))
                                  : Text(currency! + "0.00")
                            ],
                          ),
                          const SizedBox(
                            height: 7.0,
                          ),
                          if (round_amt.isNotEmpty)
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                const Text("Rounding"),
                                Text(currency! +
                                    double.parse(round_amt)
                                        .toStringAsFixed(decimal_val))
                              ],
                            ),
                          const SizedBox(
                            height: 7.0,
                          ),
                          (dis_amt > 0)
                              ? Container(
                                  margin: const EdgeInsets.only(bottom: 7),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text("Discount\n(" +
                                          offer!['name'].toString() +
                                          ")"),
                                      Text(Constant.currency +
                                          dis_amt.toStringAsFixed(decimal_val))
                                    ],
                                  ))
                              : const SizedBox(),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Text("Bill Amount"),
                              Text(currency! + grand!)
                            ],
                          ),
                          const SizedBox(
                            height: 7.0,
                          ),
                          (redeem_point != null && redeem_point.isNotEmpty)
                              ? Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Redeem Points-' + redeem_point),
                                        Text('(' +
                                            currency! +
                                            '1=' +
                                            per_point +
                                            ' Points)')
                                      ],
                                    ),
                                    Text(currency! + rp_amt)
                                  ],
                                )
                              : const SizedBox(),
                          const SizedBox(
                            height: 7.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              flex: 1),
          (restNote != null && restNote!.isNotEmpty)
              ? Container(
                  margin: const EdgeInsets.only(left: 10, right: 10, top: 5),
                  child: Text(
                    restNote!,
                    style: const TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.grey),
                  ))
              : const SizedBox(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 5, 5, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      "Total",
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      currency! +
                          double.parse(pay_amt).toStringAsFixed(decimal_val),
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    const Text(
                      "Delivery charges included",
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(5, 5, 10, 15),
                width: MediaQuery.of(context).size.width / 2, //170.0,
                height: 70.0,
                child: TextButton(
                  style: flatButtonStyle,
                  child: Text(
                    "Place Order".toUpperCase(),
                    style: const TextStyle(
                      color: BTNTXTCLR,
                    ),
                  ),
                  onPressed: () async {
                    if (double.parse(pay_amt) > 0 &&
                        Constant.allowCOD &&
                        allowOnlinePay) {
                      _selectPT();
                    } else {
                      CustomUI.customAlertDialog(
                          'Alert', 'Do you confirm this order?', () {
                        Get.back();
                        if (Constant.allowCOD) isCOD = true;
                        initTrans();
                      }, () {
                        Get.back();
                      }, context);
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  initTrans() {
    _createOrderJson('', '', '');
  }

  _setItems() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      physics: const NeverScrollableScrollPhysics(),
      itemCount:
          Constant.dishorederlist == null ? 0 : Constant.dishorederlist.length,
      itemBuilder: (BuildContext context, int index) {
        DishOrderPodo podo = Constant.dishorederlist[index];
        return CartItems(
          data: podo,
          isEditable: false,
          currency: currency!,
          clickRemove: () {},
          clickDec: () {},
          clickInc: () {},
          clickUpdate: () {},
        );
      },
    );
  }

  _setTax() {
    if (tlist == null) tlist = [];
    tlist.clear();
    tot_tax = 0.0;
    if (taxInvoiceList != null && taxInvoiceList.length > 0) {
      return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          primary: false,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: taxInvoiceList == null ? 0 : taxInvoiceList.length,
          itemBuilder: (BuildContext context, int index) {
            TaxInvoicePodo p = taxInvoiceList[index];
            Map tx = {
              'tax_id': p.id,
              'tax_value':
                  double.parse(p.amount_total!).toStringAsFixed(decimal_val),
              'tax_per': p.per,
              'tax_name': p.name
            };
            if (p.amount_total != null && p.amount_total!.isNotEmpty)
              tot_tax = tot_tax + double.parse(p.amount_total!);
            tlist.add(tx);
            return Column(
              children: <Widget>[
                const SizedBox(
                  height: 7.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(p.name! +
                        "(" +
                        double.parse(p.per!).toStringAsFixed(decimal_val) +
                        "%)"),
                    Text(currency! +
                        double.parse(p.amount_total!)
                            .toStringAsFixed(decimal_val))
                  ],
                )
              ],
            );
          });
    } else
      return const SizedBox();
  }

  _initTransaction(Map oJson) async {
    transPojo = await initTransaction(oJson, brandid);
    CustomUI.hideProgress(context);
    orderJson = oJson;
    if (transPojo!['success'] == 1) {
      if (isCOD || double.parse(pay_amt) == 0) {
        orderSuccess(transPojo!['order_id'].toString());
      }
      else if (widget.outletData.razorpayEnable == 'true') {
        _razorpay = Razorpay();
        _razorpay!.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
        _razorpay!.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
        //  _razorpay!.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
        double amount = double.parse(pay_amt) * 100;
        var options = {
          'key': widget.outletData.razorpayKeyId,
          'amount': amount,
          'name': Constant.appName,
          'description': Constant.appName + " Order",
          'order_id': transPojo!['razor_pay_order_id'],
          'prefill': {
            'contact':
            await KeyConstant.retriveString(KeyConstant.key_user_phone),
            'email': await KeyConstant.retriveString(KeyConstant.key_user_email)
          }
        };
        Constant.showLog(options);
        _razorpay!.open(options);
      }
      else if (widget.outletData.stripeEnable == 'true' ||
          widget.outletData.paypalEnable == 'true') {
        // Get.toNamed(RouteHelper.getPaymentRoute({'orderJson': orderJson!,
        //   'transJson': transPojo!,
        //   'currency': currency!}));
      }
    } else if (transPojo!.containsKey('message') &&
        transPojo!['message'] != null &&
        transPojo!['message'].toString().trim().length > 0) {
      showToast(transPojo!['message']);
    } else {
      showToast("Sorry,try again later");
    }
  }

  // Future<void> getCheckout() async {
  //   final String sessionId = await createCheckoutSession(Constant.dishorederlist);
  //   final result = await redirectToCheckout(
  //     context: context,
  //     sessionId: sessionId,
  //     publishableKey: 'pk_test_fyXUDQ7Xmu9UtWH6ihGniIqc00L8ZM1FBr',
  //     successUrl: 'https://checkout.stripe.dev/success',
  //     canceledUrl: 'https://checkout.stripe.dev/cancel',
  //   );
  //
  //   if (mounted) {
  //     final text = result.when(
  //       success: () => 'Paid succesfully',
  //       canceled: () => 'Checkout canceled',
  //       error: (e) => 'Error $e',
  //       redirected: () => 'Redirected succesfully',
  //     );
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       SnackBar(content: Text(text ?? '')),
  //     );
  //   }
  // }


//  void _handlePaymentSuccess(PaymentSuccessResponse response) {
 //   log('Success Response: $response');
  //  Fluttertoast.showToast(msg: "SUCCESS: ${response.paymentId!}", toastLength: Toast.LENGTH_SHORT);
  //}

 // void _handlePaymentError(PaymentFailureResponse response) {
    //log('Error Response: $response');
   // Fluttertoast.showToast(msg: "ERROR: ${response.code} - ${response.message!}", toastLength: Toast.LENGTH_SHORT);
 // }

  void _handleExternalWallet(ExternalWalletResponse response) {
//  log('External SDK Response: $response');
  //Fluttertoast.showToast(msg: "EXTERNAL_WALLET: ${response.walletName!}", toastLength: Toast.LENGTH_SHORT);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Constant.showLog('sucess:' + response.paymentId!);
    if (transPojo != null) {
      _razorpay!.clear();
      onlinePayDone(response.paymentId!, '', "paid");
    }
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Constant.showLog("error:" +
        response.code.toString() +
        " " +
        response.message.toString());
    onlinePayDone('', response.message.toString(), "fail");
  }

  _createOrderJson(String order_no, String dis, String transid) async {
    CustomUI.showProgress(context);
    DateTime now = DateTime.now();
    String custid = (await KeyConstant.retriveString(KeyConstant.key_user_id))!,
        address = '',
        ph = (await KeyConstant.retriveString(KeyConstant.key_user_phone))!,
        custnm = (await KeyConstant.retriveString(KeyConstant.key_user_name))!,
        custmail =
            (await KeyConstant.retriveString(KeyConstant.key_user_email))!,
        lat = "",
        lng = "",
        zipcode = "",
        dist = dis; //
    if (delivery_type == KeyConstant.type_delivery &&
        Constant.sel_address != null) {
      address = addressString(Constant.sel_address!);
      if (address == null) address = '';
      if (Constant.sel_address!.containsKey('latitude'))
        lat = Constant.sel_address!['latitude'];
      if (Constant.sel_address!.containsKey('longitude'))
        lng = Constant.sel_address!['longitude'];
      if (Constant.sel_address!.containsKey('zipcode'))
        zipcode = Constant.sel_address!['zipcode'];
    }
    String ps = 'init';
    if (double.parse(pay_amt) == 0) {
      ps = 'paid';
    } else if (isCOD && double.parse(pay_amt) > 0) ps = 'unpaid';
    String channel = "customer_app",
        created = now.millisecondsSinceEpoch.toString(),
        billDisc = "0",
        discount = "0",
        offer_id = "0",
        order_comment = note!,
        order_payment_status = ps,
        token = "",
        total_charges = packaging_charge!,
        delivery_charge = service_charge!,
        order_state = "Placed",
        order_subtotal = subtot!,
        order_total = pay_amt,
        redeem_points = pointJSON!,
        rounding_json = roundingJSON!;
    if (dis_amt > 0) {
      Map d = {
        'discount_per': dis_per.toStringAsFixed(decimal_val),
        'discount_amount': dis_amt.toStringAsFixed(decimal_val),
        'discount_type': offer!['discount_type']
      };
      discount = json.encode(d);
      offer_id = offer!['id'].toString();
      billDisc = dis_amt.toStringAsFixed(decimal_val);
    }
    List<Map> plist = [];
    if (pointJSON != null && pointJSON!.isNotEmpty) {
      Map ptype = {
        'payment_mode_text': 'loyalty_points',
        'amount': double.parse(rp_amt).toStringAsFixed(decimal_val),
        'paid_at': DateFormat("yyyy-MM-dd HH:mm:ss").format(DateTime.now())
      };
      plist.add(ptype);
    }
    String paymode = "online";
    if (!isCOD && double.parse(pay_amt) > 0) {
      Map ptype = {
        'payment_mode_text': paymode,
        'amount': double.parse(pay!).toStringAsFixed(decimal_val),
        'paid_at': DateFormat("yyyy-MM-dd HH:mm:ss").format(DateTime.now())
      };
      plist.add(ptype);
    }
    String paymentGateway = '';
    if (!isCOD) {
      if (widget.outletData.stripeEnable == 'true') {
        paymentGateway = 'stripe';
      } else if (widget.outletData.razorpayEnable == 'true') {
        paymentGateway = 'razorpay';
      } else if (widget.outletData.vivawalletEnable == 'true') {
        paymentGateway = 'viva_wallet';
      } else if (widget.outletData.paypalEnable == 'true') {
        paymentGateway = 'paypal';
      } else if (widget.outletData.mpesaEnable == 'true') {
        paymentGateway = 'mpesa';
      }
    }

    Map custJson = {
      'id': custid,
      'address': address,
      'phone': ph,
      'name': custnm,
      'email': custmail,
      'latitude': lat,
      'longitude': lng,
      'distance': dist,
      'zip_code': zipcode
    };

    Map storeJson = {
      'name': Constant.dishorederlist[0].rest_name,
      'merchant_ref_id': Constant.dishorederlist[0].rest_id
    };

    Map detailJson = {
      'channel': channel,
      'created': created,
      'delivery_type': delivery_type,
      'discount': discount,
      'offer_id': offer_id,
      'bill_amt_discount': billDisc,
      'order_comment': order_comment,
      'order_payment_status': order_payment_status,
      'order_state': order_state,
      'order_subtotal':
          double.parse(order_subtotal).toStringAsFixed(decimal_val),
      'order_total': double.parse(order_total).toStringAsFixed(decimal_val),
      'redeem_points': redeem_points,
      'rounding_json': rounding_json,
      'payment_type': (plist != null && plist.length != 0) ? plist : '',
      'taxes_data': tlist,
      'token': token,
      'total_charges': total_charges,
      'delivery_charge': delivery_charge,
      'total_taxes': tot_tax.toStringAsFixed(decimal_val),
      'txn_id': transid,
      'id': order_no,
      'is_advance_order': is_advanceorder,
      'delivery_pickup_datetime': pick_dt,
      'is_cod': (isCOD) ? 'yes' : 'no',
      'app_name': 'capto',
      'payment_gateway': paymentGateway
    };

    List itemList = [];
    for (DishOrderPodo od in Constant.dishorederlist) {
      double amt = double.parse(od.qty!) * double.parse(od.price!);
      double amtwt =
          double.parse(od.qty!) * double.parse(od.price_without_tax!);
      List olist = [], tlist = [];

      if (od.varPojoList != null && od.varPojoList!.isNotEmpty) {
        for (VariationPodo v in od.varPojoList!) {
          Map o = {
            'price': double.parse(v.amount!).toStringAsFixed(decimal_val),
            'price_without_tax':
                double.parse(v.amount_wt!).toStringAsFixed(decimal_val),
            'merchant_id': v.id,
            'title': v.name
          };
          olist.add(o);
        }
      } else {
        olist = [];
      }
      if (od.tax_data != null && od.tax_data!.isNotEmpty) {
        for (Map t in od.tax_data!) {
          Map tx = {
            'tax_value': double.parse(t['tax_value'].toString())
                .toStringAsFixed(decimal_val),
            'tax_id': t['id'],
            'tax_per': t['value'],
            'tax_name': t['text']
          };
          tlist.add(tx);
        }
      } else {
        tlist = [];
      }
      Map item = {
        'merchant_id': od.dishid,
        'title': od.dishname,
        'quantity': od.qty,
        'price_per_dish':
            double.parse(od.priceperdish!).toStringAsFixed(decimal_val),
        'price': amt.toStringAsFixed(decimal_val),
        'price_per_dish_without_tax':
            double.parse(od.priceper_without_tax!).toStringAsFixed(decimal_val),
        'price_without_tax': amtwt.toStringAsFixed(decimal_val),
        'taxes_data': tlist,
        'offer_id': od.offer,
        'discount': '0',
        'options_to_add': olist,
      };

      itemList.add(item);
    }

    Map dataJson = {
      'items': itemList,
      'details': detailJson,
      'store': storeJson
    };
    // Constant.showLog("---Items---");
    // Constant.showLog(itemList);
    // Constant.showLog("---Details---");
    // Constant.showLog(detailJson);
    // Constant.showLog("---Store---");
    // Constant.showLog(storeJson);

    Map orderJson = {'customer': custJson, 'order': dataJson};
    // Constant.showLog("---Customer---");
    // Constant.showLog(custJson);
    // Constant.showLog("---Data---");
    // Constant.showLog(dataJson);
    // Constant.showLog("----Order Json----");
    // Constant.showLog(orderJson);
    _initTransaction(orderJson);
  }

  onlinePayDone(String transid, String msg, String status) async {
    CustomUI.showProgress(ctx!);
    String oid = transPojo!['order_id'].toString();
    Map trans = {"type": 'razorpay', "token": transid};
    if (transPojo!.containsKey('razor_pay_order_id'))
      trans['razor_pay_orderid'] = transPojo!['razor_pay_order_id'];
    if (msg != null && msg.isNotEmpty) trans['message'] = msg;
    Map res = await updatePayment(
        oid,
        json.encode(trans),
        status,
        Constant.dishorederlist[0].rest_id!,
        Constant.dishorederlist[0].rest_uniq_id!,
        brandid);
    CustomUI.hideProgress(ctx!);
    if (res != null && res.containsKey('success') && res['success'] == 1) {
      if (status != 'fail') orderSuccess(oid);
    } else if (res != null && res.containsKey('message'))
      showToast(res['message']);
    else if (status == 'fail' && msg.isNotEmpty) showToast(msg);
  }

  orderSuccess(String orderid) async {
    String rid = Constant.dishorederlist[0].rest_id.toString();
    String runiqid = Constant.dishorederlist[0].rest_uniq_id.toString();
    Constant.selidlist.clear();
    Constant.dishorederlist.clear();
    await DBProvider().deleteAll();
    Provider.of<AppModel>(ctx!, listen: false).increment();
    Get.toNamed(RouteHelper.getOrderPlacedRoute({
      'orderJson': orderJson!,
      'transJson': transPojo!,
      'currency': currency!,
      'runiqid': runiqid,
      'brandid': brandid
    }));
  }

  String addressString(Map info) {
    String custadd = info['address'];
    String landmark = info['landmark'];
    String homeno = info['home_no'];
    String fulladd = "";
    if (homeno != null && homeno.isNotEmpty) fulladd = homeno + ", ";
    if (landmark != null && landmark.isNotEmpty)
      fulladd = fulladd + landmark + ", ";
    fulladd = fulladd + custadd;

    return fulladd;
  }

  _selectPT() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    InkWell(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/ic_close.png',
                          height: 25,
                          width: 25,
                        ),
                      ),
                      onTap: () {
                        Get.back();
                      },
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      'Choose Payment',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  color: Theme.of(context).primaryColor,
                  child: InkWell(
                    onTap: () {
                      Get.back();
                      isCOD = false;
                      initTrans();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Image.asset(
                            'assets/ic_creditcard.png',
                            height: 40,
                            color: defaultClr,
                          ),
                          const Flexible(
                            child: Text(
                              'Pay Now',
                              style: TextStyle(fontSize: 15),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  color: Theme.of(context).primaryColor,
                  child: InkWell(
                    onTap: () {
                      CustomUI.customAlertDialog('Are you sure?',
                          'Do you want to pay with cash on delivery?', () {
                        Get.back();
                        isCOD = true;
                        Get.back();
                        initTrans();
                      }, () {
                        Get.back();
                      }, context);
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Flexible(
                            child: Text(
                                (delivery_type == KeyConstant.type_delivery)
                                    ? 'Pay On Delivery'
                                    : 'Pay on Pickup',
                                style: const TextStyle(fontSize: 15)),
                          ),
                          Image.asset(
                            'assets/cash_on_delivery.png',
                            height: 40,
                            color: defaultClr,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
