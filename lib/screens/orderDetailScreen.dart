import 'dart:convert';

import 'package:capto/DBOrder.dart';
import 'package:capto/apis/orderAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/app.dart';

import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh_plus/pull_to_refresh_plus.dart';
import 'package:capto/util/styles.dart';

class OrderDetail extends StatefulWidget {
  String order_id, rest_id, runiq_id, brandid, parentScreen;
  int dval = 2;

  OrderDetail(
      {Key? key,
      required this.order_id,
      required this.brandid,
      required this.rest_id,
      required this.runiq_id,
      required this.parentScreen})
      : super(key: key);

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  DateFormat defaultfmt = DateFormat("yyyy-MM-dd HH:mm:ss");
  DateFormat dtfmt = DateFormat("dd MMM yy hh:mm aa");
  Map? res;
  RefreshController? _refreshController;
  bool isFetching = false;
  bool isEnd = false;
  bool isCancel = false;
  String currency = '';

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  getData() async {
    if (!isFetching && !isEnd) {
      isFetching = true;
      res = await getOrderDetail(
          widget.order_id, widget.rest_id, widget.runiq_id, widget.brandid);
      isFetching = false;
      if (res != null && res!.containsKey('restcurrency')) {
        currency = res!['restcurrency'];
      }
      isEnd = true;
      _refreshController!.refreshCompleted();
      _refreshController!.loadComplete();
      if (mounted) {
        setState(() {});
      }
    }
  }

  @override
  didUpdateWidget(OrderDetail? oldWidget) {
    super.didUpdateWidget(oldWidget!);
    if (isFetching == false) {
      _refreshController!.refreshCompleted();
      _refreshController!.loadComplete();
    }
  }

  _onRefresh() async {
    if (!isFetching) {
      isEnd = false;
      getData();
      _refreshController!.refreshCompleted();
    }
  }

  _onLoading() async {
    if (!isFetching && !isEnd) {
      getData();
    }
  }

  void dispose() {
    _refreshController!.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    FirebaseHelper(context, onRefresh: (Map message) {
      Constant.showLog("=== ON REFRESH===");
      Constant.showLog(message);
      if (message.containsKey("data") && message['data'] != null) {
        Map data = message['data'];
        if (data.isNotEmpty) {
          Constant.showLog(data);
          if (data.containsKey('order_no') &&
              data['order_no'].toString() == widget.order_id) {
            _onRefresh();
          }
        }
      }
    });
    _refreshController = RefreshController(initialRefresh: false);
    Future.delayed(const Duration(milliseconds: 10), () {
      this.getData();
    });
  }

  void backClick() {
    if (widget.parentScreen == 'order_list') {
      Get.back(result: (isCancel) ? "yes" : "no");
    } else {
      Get.offNamedUntil(RouteHelper.getMainRoute(), (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (Constant.decimal_value != null && Constant.decimal_value.isNotEmpty) {
      widget.dval = int.parse(Constant.decimal_value);
    }
    return WillPopScope(
      onWillPop: () async {
        backClick();
        return false;
      },
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: const Text('Order Detail'),
          ),
          body: Container(
            child: SmartRefresher(
                header: MaterialClassicHeader(
                    color: Theme.of(context).primaryColor,
                    backgroundColor: Theme.of(context).colorScheme.secondary),
                enablePullDown: true,
                enablePullUp: !isEnd,
                controller: _refreshController!,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: (res != null &&
                        res!.containsKey("success") &&
                        res!['success'] == 101)
                    ? NoInternetDialog(
                        retryClick: () {
                          getData();
                        },
                      )
                    : (res != null && res!.containsKey("message"))
                        ? Center(
                            child: Text(res!['message']),
                          )
                        : (res != null)
                            ? _setDetail(res!)
                            : const SizedBox()),
          )),
    );
  }

  _setDetail(Map details) {
    DateTime orderDateTime = defaultfmt.parse(details['order_time']);
    String otime = dtfmt.format(orderDateTime);
    String isAdvance = '', dtime = '';
    if (details.containsKey("is_advance_order")) {
      isAdvance = details['is_advance_order'];
    }
    //isAdvance != null && isAdvance == 'yes' &&
    if (details.containsKey("delivery_pickup_datetime") &&
        details['delivery_pickup_datetime'].toString().isNotEmpty) {
      String dptm = details['delivery_pickup_datetime'];
      DateTime dDateTime = defaultfmt.parse(dptm);
      dtime = dtfmt.format(dDateTime);
    }
    List itemList = [], taxList = [];
    if (details.containsKey("order_details")) {
      itemList = details['order_details'];
    }
    if (details.containsKey("taxes_data") && details['taxes_data'] != null)
      taxList = details['taxes_data'];
    String pack = "", disc = "", deli = "", offernm = "";
    if (details.containsKey("packaging_charge")) {
      pack = details['packaging_charge'].toString();
    }
    if (details.containsKey("delivery_charge")) {
      deli = details['delivery_charge'].toString();
    }
    if (details.containsKey("discount") &&
        details['discount'].toString() != '0') {
      try {
        Map discJSON = json.decode(details['discount'].toString());
        disc = discJSON['discount_amount'].toString();
      } catch (e) {
        Constant.showLog("discount error");
        Constant.showLog(e);
      }
    }
    if (details.containsKey('offer_name')) {
      offernm = (details['offer_name'].toString().isNotEmpty)
          ? "\n(" + details['offer_name'].toString() + ")"
          : "";
    }
    String? p_amt, p_hint, p_redeem;
    if (details.containsKey('redeem_points') &&
        details['redeem_points'] != null &&
        details['redeem_points'].toString().isNotEmpty) {
      Map rObj = json.decode(details['redeem_points']);
      p_hint = "(" +
          currency +
          1.toStringAsFixed(widget.dval) +
          "=" +
          rObj["points_per_one_currency"].toString() +
          " Points)";
      p_redeem = "Redeem Point-" + rObj["redeem_points"].toString();
      p_amt = currency +
          double.parse(rObj["redeem_amount"].toString())
              .toStringAsFixed(widget.dval);
    }
    String cRounding = "";
    if (details.containsKey("rounding_json")) {
      String r = details['rounding_json'].toString();
      if (r != null && r.isNotEmpty) {
        Map rJson = json.decode(r);
        String rval = rJson["cash_rounding"].toString();
        if (rval != null && rval.isNotEmpty) {
          cRounding = double.parse(rval).toStringAsFixed(widget.dval);
        }
      }
    }
    String dName = "", cust_address = "", footer = "";
    if (details.containsKey("driver_name")) dName = details['driver_name'];
    if (details.containsKey("cust_address")) {
      cust_address = details['cust_address'];
    }
    if (details.containsKey('payment_status')) {
      String ps = details['payment_status'];
      if (ps == 'unpaid') {
        footer = 'Cash On Delivery';
      } else if (ps == 'paid') {
        footer = 'Bill paid';
      } else if (ps == 'init') {
        footer = 'Transaction not complete';
      } else if (ps == 'fail') {
        if (details.containsKey('txn_id')) {
          Constant.showLog("fail");
          Constant.showLog(details['txn_id']);
//          Map tm = details['txn_id'];
//          if (tm.containsKey('message')) footer = tm['message'];
        }
      }
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
//Outlet info,order type,order time
          Card(
            color: Theme.of(context).scaffoldBackgroundColor,
            elevation: 4,
            child: Container(
              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Order #' + details['order_no'],
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text('Order type: ' +
                        details['delivery_type'].toString().toUpperCase()),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: const Text('Outlet'),
                        ),
                        flex: 1,
                      ),
                      Flexible(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            details['restname'],
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          details['restname'],
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(details['restaddress']),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(details['restphno']),
                      ),
                    ],
                  ),
                  const Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: const Text('Order Date'),
                        ),
                        flex: 1,
                      ),
                      Flexible(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(otime),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                  (dtime != null && dtime.isNotEmpty)
                      ? const SizedBox(height: 3)
                      : const SizedBox(),
                  (dtime != null && dtime.isNotEmpty)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: const Text('Pickup/Delivery Date'),
                              ),
                              flex: 1,
                            ),
                            Flexible(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: Text(dtime),
                              ),
                              flex: 1,
                            ),
                          ],
                        )
                      : const SizedBox(),
                ],
              ),
            ),
          ),
//Order Process/Tracking
          const SizedBox(
            height: 10.0,
          ),
          (details.containsKey('payment_status') &&
                  details['payment_status'] == 'init')
              ? const SizedBox()
              : _trackFlow(details['delivery_type'].toString(),
                  details['status'].toString()),
          const SizedBox(
            height: 10.0,
          ),
//order items,sub total,grand total,tax,rounding,discount,service charges,address
          Card(
            color: Theme.of(context).scaffoldBackgroundColor,
            elevation: 4,
            child: Container(
              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
              child: Column(
                children: <Widget>[
                  ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: itemList == null ? 0 : itemList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _setItemsRow(itemList[index]);
                      }),
                  const SizedBox(
                    height: 15.0,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text((itemList == null)
                        ? '0'
                        : itemList.length.toString() + " Items"),
                  ),
                  _setRow(
                      'Sub Total',
                      null,
                      currency +
                          double.parse(details['amount'])
                              .toStringAsFixed(widget.dval)),
                  //Discount
                  (disc != null && disc.isNotEmpty && double.parse(disc) != 0)
                      ? _setRow(
                          'Discount' + offernm,
                          null,
                          currency +
                              double.parse(disc).toStringAsFixed(widget.dval))
                      : const SizedBox(
                          height: 0.0,
                        ),
                  //Redeem Points
                  (p_hint != null && p_redeem != null && p_amt != null)
                      ? _setRow(p_redeem, p_hint, p_amt)
                      : const SizedBox(
                          height: 0,
                        ),
                  //Tax
                  (taxList != null && taxList.length > 0)
                      ? ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: taxList == null ? 0 : taxList.length,
                          itemBuilder: (BuildContext context, int index) {
                            Map t = taxList[index];
                            return _setRow(
                                t['tax_name'] + "(" + t['tax_per'] + "%)",
                                null,
                                currency +
                                    double.parse(t['tax_value'])
                                        .toStringAsFixed(widget.dval));
                          })
                      : const SizedBox(
                          height: 0.0,
                        ),
                  //Packaging Charges
                  (pack != null && pack.isNotEmpty && double.parse(pack) != 0)
                      ? _setRow(
                          'Packaging Charge',
                          null,
                          currency +
                              double.parse(pack).toStringAsFixed(widget.dval))
                      : const SizedBox(
                          height: 0.0,
                        ),
                  //Delivery Charges
                  (deli != null && deli.isNotEmpty && double.parse(deli) != 0)
                      ? _setRow(
                          'Delivery Charge',
                          null,
                          currency +
                              double.parse(deli).toStringAsFixed(widget.dval))
                      : const SizedBox(
                          height: 0.0,
                        ),
                  //Rounding
                  (cRounding != null &&
                          cRounding.isNotEmpty &&
                          double.parse(cRounding) != 0)
                      ? _setRow('Rounding', null, currency + cRounding)
                      : const SizedBox(
                          height: 0,
                        ),

                  const Divider(),
                  //Payable amount
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      const Text('Pay'),
                      Text(currency +
                          double.parse(details['grand_total'])
                              .toStringAsFixed(widget.dval))
                    ],
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  //Driver name
                  (dName != null && dName.isNotEmpty)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                              const Text('Driver Name'),
                              Text(dName)
                            ])
                      : const SizedBox(
                          height: 0,
                        ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  //Delivery address
                  (cust_address != null && cust_address.isNotEmpty)
                      ? Column(children: <Widget>[
                          const Text('Delivery Address:'),
                          Text(cust_address)
                        ])
                      : const SizedBox(
                          height: 0.0,
                        ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10.0,
          ),
//Payment Status
          (footer != null && footer.isNotEmpty)
              ? Card(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  elevation: 4,
                  child: Container(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        footer,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                        textAlign: TextAlign.center,
                      )),
                )
              : const SizedBox(),
//cancel button
          (details['status'].toString() == '6' &&
                  (details.containsKey('payment_status') &&
                      details['payment_status'] != 'init'))
              ? Container(
                  padding: const EdgeInsets.fromLTRB(5, 5, 10, 15),
                  width: 150.0,
                  height: 50.0,
                  child: TextButton(
                    style: flatButtonStyle,
                    child: Text(
                      "Cancel Order".toUpperCase(),
                      style: const TextStyle(
                        color: BTNTXTCLR,
                      ),
                    ),
                    onPressed: () {
                      CustomUI.customAlertDialog("Cancel Order Alert",
                          "Are you sure you want to cancel order?", () {
                        _cancelOrder(details);
                      }, () {
                        Get.back();
                      }, context);
                    },
                  ),
                )
              : (details['status'].toString() == '3')
                  ? Container(
                      padding: const EdgeInsets.fromLTRB(5, 5, 10, 15),
                      width: 150.0,
                      height: 50.0,
                      child: TextButton(
                        style: flatButtonStyle,
                        child: Text(
                          "ReOrder".toUpperCase(),
                          style: const TextStyle(
                            color: BTNTXTCLR,
                          ),
                        ),
                        onPressed: () {
                          _getReOrderDetail(details['restid'].toString(),
                              details['rest_unique_id'], widget.order_id);
                        },
                      ),
                    )
                  : const SizedBox(),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  _cancelOrder(Map details) async {
    Get.back();
    CustomUI.showProgress(context);
    Map res = await cancelOrder(widget.order_id, details['restid'].toString(),
        details['rest_unique_id'], widget.brandid);
    CustomUI.hideProgress(context);
    if (res['success'] == 101) {
      showToast("No Internet Connection");
    } else if (res['success'] == 1) {
      isCancel = true;
      showToast("Order Canceled");
      _onRefresh();
    } else {
      showToast("Order not cancel");
    }
  }

  _trackFlow(String dtype, String status) {
    bool s1 = false, s2 = false, s3 = false, s4 = false, s5 = false;
    if (status == "6") {
      s1 = true;
    } else if (status == "1") {
      s1 = true;
      s2 = true;
    } else if (status == "7") {
      s1 = true;
      s2 = true;
      s3 = true;
    } else if (status == "5") {
      s1 = true;
      s2 = true;
      s3 = true;
      s4 = true;
    } else if (status == "3") {
      s1 = true;
      s2 = true;
      s3 = true;
      s4 = true;
      s5 = true;
    }
    double m_top = 5.0;
    if (dtype == KeyConstant.type_delivery) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              const Text(
                'Order placed',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s1) ? mainClr : Colors.grey,
                child: Image.asset(
                  'assets/ic_done_all.png',
                  color: Colors.white,
                  width: 20,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Preparing',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s2) ? mainClr : Colors.grey,
                child: Image.asset(
                  'assets/ic_restaurant.png',
                  color: Colors.white,
                  width: 18,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Order Ready',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s3) ? mainClr: Colors.grey,
                child: Image.asset(
                  'assets/ic_food_ready.png',
                  color: Colors.white,
                  width: 20,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Dispatched',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s4) ? mainClr : Colors.grey,
                child:Image.asset(
                  'assets/ic_bike.png',
                  color: Colors.white,
                  width: 18,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Delivered',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s5) ? mainClr : Colors.grey,
                child:Image.asset(
                  'assets/ic_home.png',
                  color: Colors.white,
                  width: 18,
                ),
              )
            ],
          ),
        ],
      );
    } else {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              const Text(
                'Order placed',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s1) ? mainClr : Colors.grey,
                child:Image.asset(
                  'assets/ic_done_all.png',
                  color: Colors.white,
                  width: 20,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Preparing',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s2) ? mainClr : Colors.grey,
                child: Image.asset(
                  'assets/ic_restaurant.png',
                  color: Colors.white,
                  width: 18,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Order Ready',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s3) ? mainClr : Colors.grey,
                child:Image.asset(
                  'assets/ic_food_ready.png',
                  color: Colors.white,
                  width: 20,
                ),
              )
            ],
          ),
          const Divider(),
          Column(
            children: <Widget>[
              const Text(
                'Delivered',
                style: TextStyle(fontSize: 10.0),
              ),
              SizedBox(
                height: m_top,
              ),
              CircleAvatar(
                backgroundColor: (s5) ? mainClr : Colors.grey,
                child: Image.asset(
                  'assets/ic_home.png',
                  color: Colors.white,
                  width: 18,
                ),
              )
            ],
          ),
        ],
      );
    }
  }

  _setItemsRow(Map item) {
    String img = "", pref = "";
    if (item.containsKey("type")) {
      String dish_type = item['type'];
      if (dish_type == 'Veg') {
        img = "assets/veg.png";
      } else if (dish_type == 'NonVeg') {
        img = "assets/non_veg.png";
      }
    }
    if (item.containsKey("preflag") && item['preflag'] == 'true') {
      if (item.containsKey("preferences_json")) {
        List varList = item['preferences_json'];
        if (varList != null && varList.length > 0) {
          for (Map v in varList) {
            if (pref.trim().length == 0) {
              pref = v['name'];
            } else {
              pref = pref + "," + v['name'];
            }
          }
        }
      }
    }

    return Column(
      children: <Widget>[
// Item type(veg/nonveg),name,qty,price
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: Container(
                //width: MediaQuery.of(context).size.width / 1.8,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    (img != null && img.isNotEmpty)
                        ? Image.asset(img, height: 15, width: 15)
                        : const SizedBox(
                            height: 15,
                            width: 15,
                          ),
                    const SizedBox(width: 3.0),
                    Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      child: Text(
                        item['dishname'],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                // width: MediaQuery.of(context).size.width * 0.1,
                child: Text("x" + item['quantity'].toString() + ""),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                //width: MediaQuery.of(context).size.width * 0.2,
                alignment: Alignment.topRight,
                child: Text(currency + item['price'].toString()),
              ),
            ),
          ],
        ),
        (pref != null && pref.length > 0)
            ? Container(
                margin: const EdgeInsets.only(left: 18.0),
                alignment: Alignment.centerLeft,
                child: Text(pref),
              )
            : const SizedBox(height: 0.0),

        const SizedBox(
          height: 10.0,
        ),
      ],
    );
  }

  _setRow(String txt, String? subtxt, String val) {
    return Column(
      children: <Widget>[
        const SizedBox(
          height: 7.0,
        ),
        //sub total
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[Text(txt), Text(val)],
        ),
        (subtxt != null && subtxt.trim().length > 0)
            ? Container(
                alignment: Alignment.centerLeft,
                child: Text(subtxt),
              )
            : const SizedBox(
                height: 0,
              )
      ],
    );
  }

  _getReOrderDetail(String rid, String runiqid, String orderid) async {
    CustomUI.showProgress(context);
    Map res = await getReOrderDetail(orderid, rid, runiqid, widget.brandid);
    if (res.containsKey('success') && res['success'] == 101) {
      CustomUI.hideProgress(context);
      showToast("No Internet Connection");
    } else if (res.containsKey('order_details')) {
      List olist = res['order_details'];
      if (olist != null && olist.length > 0) {
        await DBProvider().deleteAll();
        for (Map o in olist) {
          String qty = o['quantity'].toString();
          double amt = double.parse(o['price'].toString()) / int.parse(qty);
          double amtwt =
              double.parse(o['price_without_tax'].toString()) / int.parse(qty);
          DishOrderPodo d = DishOrderPodo();
          d.dishid = o['dishid'].toString();
          d.cusineid = o['cuisine_id'].toString();
          d.dishname = o['dishname'];
          d.dishtype = o['type'];
          d.description = '';
          d.price = amt.toString();
          d.priceperdish = o['price_per_dish'].toString();
          d.priceper_without_tax = o['price_per_dish_without_tax'].toString();
          d.price_without_tax = amtwt.toString();
          d.dishimage = '';
          d.preflag = o['preflag'].toString();
          d.sold_by = 'each';
          d.tax_amt = o['tax_amt'].toString();
          d.offer = '';
          d.discount = o['discount'].toString();
          d.combo_flag = o['combo_flag'].toString();
          d.qty = qty;
          d.brandid = widget.brandid;
          d.rest_id = res['rest_id'].toString();
          d.rest_uniq_id = res['rest_uniq_id'];
          d.rest_name = res['rest_name'];
          d.rest_img = '';
          d.rest_address = res['rest_address'];
          List tax_list = [];
          if (o.containsKey('tax_data')) {
            tax_list = o['tax_data'];
            if (tax_list != null && tax_list.length > 0) {
              double tot_tax = tax_list
                  .map<double>((m) => double.parse(m['value']))
                  .reduce((a, b) => a + b);
              d.tot_tax = tot_tax.toString();
            } else {
              d.tot_tax = '0';
            }
          } else {
            d.tot_tax = '0';
          }
          d.tax_data = tax_list;
          d.pre = o['pre'];
          d.preid = "";
          d.dish_comment = "";
          await DBProvider().addOrderDetail(d);
        }
        await DBProvider().getOrderDetail();
        Provider.of<AppModel>(context, listen: false).increment();
        CustomUI.hideProgress(context);
        showToast('Item added to cart');
        Get.back();
      }
    } else {
      Get.back();
    }
  }
}
