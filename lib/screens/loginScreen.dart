import 'package:capto/apis/authenticationAPI.dart';

import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/ui/swiftoTxtField.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';

// import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:capto/util/styles.dart';

class LoginScreen extends StatefulWidget {
  static GlobalKey<_LoginScreenState> loginGlobalKey = GlobalKey();

  LoginScreen({
    Key? key,
  }) : super(key: loginGlobalKey);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController phController = TextEditingController();
  String phoneNumber = '',
      country_code = '+91',
      country = 'India',
      shortnm = 'IN';
  ConfirmationResult? cResult;

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  _loginSMS() async {
    Constant.country_code = country_code;
    Constant.country = country;
    phoneNumber = country_code + phController.text;
    Constant.showLog(phoneNumber + " " + country + "-" + country_code);
    if (phController.text.length == 0) {
      showToast('Please enter mobile number');
    } else if (phController.text.length > 14) {
      showToast('Please enter valid mobile number');
    }
    if (Constant.shopData!=null && Constant.shopData!.captoDineinAskPin != null &&
        Constant.shopData!.captoDineinAskPin == 'disable') {
      Constant.showLog("Not require phone verification");
      checkUser(phoneNumber);
    }else if (country_code != '+91') {
      checkUser(phoneNumber);
    } else {
      if (kIsWeb) {
        final confirmationResult =
            await FirebaseAuth.instance.signInWithPhoneNumber(phoneNumber);
        cResult = confirmationResult;
        Get.toNamed(RouteHelper.getVerifyOTPRoute({
          'fromCart': false,
          'verId': null,
          'phoneNumber': phoneNumber,
          //   'cResult': confirmationResult
        }));
      } else {
        await KeyConstant.saveString(KeyConstant.key_country, country);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            });
        final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId) {
          Constant.showLog("==PhoneCodeAutoRetrievalTimeout==");
        };

        final PhoneCodeSent smsCodeSent =
            (String verId, [int? forceCodeResend]) {
          Constant.showLog("==Sent==");
          Constant.showLog('sms code sent ' + verId);
          Get.back();
          Get.toNamed(RouteHelper.getVerifyOTPRoute(
              {'fromCart': false, 'verId': verId, 'phoneNumber': phoneNumber}));
        };

        final PhoneVerificationCompleted verifiedSuccess =
            (AuthCredential phoneAuthCredential) async {
          Constant.showLog("==PhoneVerificationCompleted==");
          Constant.showLog('verified');
          final User? user = (await FirebaseAuth.instance
                  .signInWithCredential(phoneAuthCredential))
              .user;
          if (user != null) {
            Constant.showLog("===user===");
            Constant.showLog(user.phoneNumber);
            checkUser(user.phoneNumber!);
          }
        };

        final PhoneVerificationFailed veriFailed =
            (FirebaseAuthException exception) {
          Constant.showLog("==PhoneVerificationFailed==");
          Constant.showLog(exception.message);
          Get.back();
        };

        await FirebaseAuth.instance.verifyPhoneNumber(
            phoneNumber: phoneNumber,
            codeAutoRetrievalTimeout: autoRetrieve,
            codeSent: smsCodeSent,
            timeout: const Duration(seconds: 5),
            verificationCompleted: verifiedSuccess,
            verificationFailed: veriFailed);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    if (Constant.shopData!.countryCode != null) {
      country_code = Constant.shopData!.countryCode!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Login'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Center(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // CountryCodePicker(
                          //   initialSelection: country_code,
                          //   onChanged: (CountryCode e) {
                          //     if(mounted)
                          //     setState(() {
                          //       country_code = e.dialCode!;
                          //       country = e.name!;
                          //       shortnm = e.code!;
                          //       Constant.showLog(country_code +
                          //           " " +
                          //           country +
                          //           " " +
                          //           shortnm);
                          //     });
                          //   },
                          //   builder: (countryCode) {
                          //     Constant.showLog(
                          //         "countryCode---" + countryCode!.dialCode!);
                          //     country_code = countryCode.dialCode!;
                          //     return Text('${countryCode.dialCode!}');
                          //   },
                          //   showCountryOnly: false,
                          //   showOnlyCountryWhenClosed: false,
                          //   alignLeft: false,
                          // ),
                          Text('${country_code}'),
                          Image.asset(
                            'assets/ic_down.png',
                            width: 15,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            flex: 1,
                            child: SwiftoTxtField(
                                phController,
                                "Mobile Number",
                                TextInputAction.done,
                                TextInputType.phone,
                                false,
                                10,
                                null),
                          )
                        ],
                      ),
                      Center(
                        child: SwiftoBtn("Login", () {
                          _loginSMS();
                        }),
                      ),
                      const SizedBox(
                        height: 25.0,
                      ),
                      InkWell(
                          onTap: () {
                            Get.toNamed(
                                RouteHelper.getInfoDetailRoute('terms'));
                            // Navigator.of(context).pushNamed(
                            //     RouteGenerator.infoDetail,
                            //     arguments: 'terms');
                          },
                          child: const Text(
                            'Terms & Conditions',
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: defaultClr),
                            textAlign: TextAlign.center,
                          )),
                      const SizedBox(height: 20.0),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }

  checkUser(String phoneNumber) async {
    CustomUI.showProgress(context);
    String ph = phoneNumber.toString().replaceFirst(Constant.country_code, '');
    Map res = await checkPhone(ph);
    CustomUI.hideProgress(context);
    if (res.containsKey('success')) {
      if (res['success'] == 101) {
        CustomUI.customAlert("", "No Internet Connection", context);
      } else if (res['success'] == 1) {
        // Provider.of<AppModel>(context, listen: false).changeTab(0);
        await KeyConstant.saveUserInfo(res['user']);
        Get.back(result: true);
      } else if (res['success'] == 0) {
        Get.offNamed(RouteHelper.getSignUpRoute(ph));
      } else if (res.containsKey('message')) {
        showToast(res['message']);
      }
    } else {
      showToast("Login failed");
    }
  }

  @override
  void dispose() {
    phController.dispose();
    super.dispose();
  }
}
