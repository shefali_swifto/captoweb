import 'package:capto/apis/orderAPI.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh_plus/pull_to_refresh_plus.dart';

class OrderList extends StatefulWidget {
  String TAG;
  FirebaseHelper parentObj;

  OrderList({Key? key, required this.TAG,required this.parentObj}) : super(key: key);

  @override
  _OrderListState createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  List feedList = [];
  int _page = 1;
  Map? result;
  RefreshController? _refreshController;
  bool isFetching = false;
  bool isEnd = false;

  @override
  initState() {
    super.initState();
    feedList = [];
    _refreshController = RefreshController(initialRefresh: false);
    _loadProduct();
    widget.parentObj.setOnRefresh((Map message){
      if (message.containsKey("data") && message['data'] != null) {
        Map data = message['data'];
        if (data.isNotEmpty) {
          _page = 1;
          _loadProduct();
        }
      }
    });
  }

  @override
  didUpdateWidget(Widget oldWidget) {
   // super.didUpdateWidget(oldWidget);
    if (isFetching == false) {
      _refreshController!.refreshCompleted();
      _refreshController!.loadComplete();
    }
  }

  _loadProduct() async {
    if (!isFetching && !isEnd) {
      isFetching = true;
      result = await getOrderList(widget.TAG, _page);
      isFetching = false;
      if (result!["success"] == 1 && result!.containsKey('data')) {
        List d = result!['data'];
        if (_page == 1) {
          feedList.clear();
          feedList = d;
        } else
          feedList.addAll(d);
        if (d.length == 0) isEnd = true;
      } else
        isEnd = true;
      _refreshController!.refreshCompleted();
      _refreshController!.loadComplete();
      if(mounted)
      setState(() {});
    }
  }

  _onRefresh() async {
    if (!isFetching) {
      isEnd = false;
      _page = 1;
      feedList = [];
      _loadProduct();
      _refreshController!.refreshCompleted();
    }
  }

  _onLoading() async {
    if (!isFetching && !isEnd) {
      _page = _page + 1;
      _loadProduct();
      _refreshController!.loadComplete();
    }
  }

  void dispose() {
    _refreshController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: (result == null && _page == 1)
            ? CustomUI.CustProgress(context)
            : Container(
                child: SmartRefresher(
                  header: MaterialClassicHeader(
                      color: Theme.of(context).primaryColor,
                      backgroundColor: Theme.of(context).colorScheme.secondary),
                  enablePullDown: true,
                  enablePullUp: !isEnd,
                  controller: _refreshController!,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                  child: (feedList != null && feedList.isNotEmpty)
                      ? ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: feedList.length,
                          itemBuilder: (BuildContext context, int index) {
                            Map om = feedList[index];
                            return _orderRow(om, index);
                          })
                      : (result != null &&
                              _page == 1 &&
                              result!["success"] == Constant.api_no_net)
                          ? NoInternetDialog(retryClick: () {
                              _loadProduct();
                            })
                          : (result != null &&
                                  _page == 1 &&
                                  result!["success"] == Constant.api_catch_error)
                              ? Center(child: Text(result!['message']))
                              : (result != null &&
                                      _page == 1 &&
                                      result!["success"] == 1 &&
                                      feedList.length == 0)
                                  ? _emptyList()
                                  : SizedBox(),
                ),
              ));
  }

  _emptyList() {
    return Center(
      child: Text('No Orders Yet.'),
    );
  }

  _orderRow(Map data, int i1) {
    DateFormat defaultfmt = new DateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dtfmt = new DateFormat("dd MMM yyyy");
    DateTime orderDateTime = defaultfmt.parse(data['start_time']);
    String otime = dtfmt.format(orderDateTime);
    String status = "";
    Color? clr;
    String isAdvance = '', dtime = '';
    if (data.containsKey("is_advance_order"))
      isAdvance = data['is_advance_order'];
    if (isAdvance != null &&
        isAdvance == 'yes' &&
        data.containsKey("delivery_pickup_datetime") &&
        data['delivery_pickup_datetime'].toString().isNotEmpty) {
      String dptm = data['delivery_pickup_datetime'];
      DateTime dDateTime = defaultfmt.parse(dptm);
      dtime = dtfmt.format(dDateTime);
    }
    if (data.containsKey('status')) {
      String st = data['status'];
      if (st == "1" || st == "6") {
        status = KeyConstant.status_inprocess;
        clr = Colors.blue;
      } else if (st == "7") {
        status = KeyConstant.status_ready;
        clr = Colors.blue;
      } else if (st == "5") {
        status = KeyConstant.status_dispatch;
        clr = Colors.blue;
      } else if (st == "3") {
        status = KeyConstant.status_complete;
        clr = Colors.green;
      } else if (st == "4") {
        status = KeyConstant.status_cancel;
        clr = Colors.red;
      } else {
        status = "";
      }
    } else
      status = "";
    return InkWell(
      child: Container(
          color: Theme.of(context).cardColor,
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.fromLTRB(
              10, 5, 10, (feedList.length - 1 == i1) ? 30 : 5),
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                child: Text('Order #' + data['channel_order_id'],
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    )),
              ),
              SizedBox(
                height: 7.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Container(
                      child: Text('Status'),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(
                      child: Text(
                        status,
                        style: TextStyle(color: clr),
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 7.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Container(
                      child: Text('Date'),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(
                      child: Text(otime),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 7.0,
              ),
              (isAdvance != null &&
                      isAdvance == 'yes' &&
                      dtime != null &&
                      dtime.isNotEmpty)
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: Container(
                            child: Text('PickUp/Delivery Date'),
                            alignment: Alignment.centerLeft,
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Container(
                            child: Text(otime),
                            alignment: Alignment.centerLeft,
                          ),
                        ),
                      ],
                    )
                  : SizedBox(),
              (isAdvance != null &&
                      isAdvance == 'yes' &&
                      dtime != null &&
                      dtime.isNotEmpty)
                  ? SizedBox(
                      height: 7,
                    )
                  : SizedBox(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Container(
                      child: Text('Order Amount'),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(
                      child: Text(data['currency'] + data['grand_total']),
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                ],
              ),
            ],
          )),
      onTap: () async {
        await Get.toNamed(RouteHelper.getOrderDetailRoute({
          'order_id': data['order_id'].toString(),
          'rest_id': data['rest_id'].toString(),
          'runiq_id': data['rest_unique_id'],
          'brandid': data['brand_id'],
          'parentScreen':'order_list'
        }));
        // await Navigator.of(context).pushNamed(RouteGenerator.orderDetail,arguments: {
        //   'order_id': data['order_id'].toString(),
        //   'rest_id': data['rest_id'].toString(),
        //   'runiq_id': data['rest_unique_id'],
        //   'brandid': data['brand_id'],
        //   'parentScreen': data['order_list']
        // });
        if(mounted) {
          setState(() {
            result = null;
            _onRefresh();
          });
        }
      },
    );
  }
}
