import 'dart:convert';

import 'package:capto/apis/authenticationAPI.dart';
import 'package:capto/screens/dialogLayout/searchAddressDialog.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:geocoder2/geocoder2.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

class AddAddressScreen extends StatefulWidget {
  String action;
  List addressList;
  int? selpos;

  AddAddressScreen(
      {Key? key,
      required this.action,
      required this.addressList,
      this.selpos})
      : super(key: key);

  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _z = new TextEditingController();
  String address = '',
      hfno = '',
      landmark = '',
      tag = '',
      lat = '0.0',
      lng = '0.0',
      city = '',
      zcode = '';
  final _formKey = new GlobalKey<FormState>();

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  getCurrentLocation() async {
    LocationPermission geolocationStatus = await Geolocator.checkPermission();
    bool isGpsEnabled = await Geolocator.isLocationServiceEnabled();
    if(!isGpsEnabled){
      showToast("GPS service disable");
      bool res=await Geolocator.openLocationSettings();
      if(res)
        getCurrentLocation();
    }else if(geolocationStatus!=LocationPermission.always && geolocationStatus!=LocationPermission.whileInUse){
      await Geolocator.requestPermission();
      getCurrentLocation();
    }else if ((geolocationStatus == LocationPermission.always || geolocationStatus==LocationPermission.whileInUse) && isGpsEnabled) {
      Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      lat = position.latitude.toString();
      lng = position.longitude.toString();
      GeoData first = await Geocoder2.getDataFromCoordinates(
          latitude: position.latitude,
          longitude: position.longitude,
          googleMapApiKey: Constant.kGoogleApiKey);
      city=first.city;
      zcode=first.postalCode;
      _z.text=zcode;
      address = first.address;
      if(mounted) {
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    super.initState();
    new FirebaseHelper(context);
    if (widget.action == 'edit_address') {
      Map m = widget.addressList[widget.selpos!];
      address = m['address'];
      hfno = m['home_no'];
      landmark = m['landmark'];
      tag = m['address_tag'];
      lat = m['latitude'];
      lng = m['longitude'];
      city = m['city'];
      zcode = (m.containsKey('zipcode')) ? m['zipcode'] : '';
      _z.text = zcode;
    }
  }

  @override
  Widget build(BuildContext context) {
   // Constant.showLog('zip:' + zcode);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text((widget.action == 'add_address')
            ? 'Add new Address'
            : 'Update Address'),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        getCurrentLocation();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset('assets/ic_location.png',width: 18,),
                          const SizedBox(
                            width: 2,
                          ),
                          const Text('Current Location'),
                        ],
                      ),
                    ),
                    const SizedBox(height: 5),
                    InkWell(
                        onTap: () async{
                        //  _handlePressButton();
                         dynamic r=await showDialog(context: context, builder: (BuildContext context){
                           return const SearchAdderssDialog();
                         });
                         if(r!=null && r is Map){
                           //lat,lng,city,zcode,_z.text,address
                           GeoData first = await Geocoder2.getDataFromAddress(
                               address: r['description'],
                               googleMapApiKey: Constant.kGoogleApiKey);
                           lat = first.latitude.toString();
                           lng = first.longitude.toString();
                           city=first.city;
                           zcode=first.postalCode;
                           _z.text=zcode;
                           address = r['description'];
                           Constant.showLog(lat+" "+lng);
                           Constant.showLog(city+" "+zcode);
                           Constant.showLog(address);
                           if(mounted){
                             setState(() {});
                           }
                         }
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const Text('Address'),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Text(address),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              height: 1,
                              color: Theme.of(context).dividerColor,
                            )
                          ],
                        )),
                    const SizedBox(
                      height: 15.0,
                    ),
                    const Text('Home No./Flat No.'),
                    TextFormField(
                      key: Key('enterno'),
                      initialValue: hfno,
                      onSaved: (val) => hfno = val!,
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    const Text('Landmark'),
                    TextFormField(
                      key: new Key('enterlandmark'),
                      initialValue: landmark,
                      onSaved: (val) => landmark = val!,
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    const Text('Zipcode'),
                    TextFormField(
                      key: new Key('enterzipcode'),
                      controller: _z,
                      onSaved: (val) => zcode = val!,
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    const Text('Tag'),
                    TextFormField(
                      key: new Key('entertag'),
                      initialValue: tag,
                      onSaved: (val) => tag = val!,
                    ),
                  ],
                ),
              )),
          Center(
            child: SwiftoBtn(
                (widget.action == 'add_address')
                    ? "Add Address"
                    : "Update Address", () async {
              _formKey.currentState!.save();
              if (address.isEmpty) {
                showToast("Address required");
              } else {
                if (tag.isEmpty) tag = 'Other';

                Map addRow = {
                  'address': address,
                  'latitude': lat,
                  'longitude': lng,
                  'address_tag': tag,
                  'city': city,
                  'landmark': landmark,
                  'home_no': hfno,
                  'zipcode': zcode
                };
                // Constant.showLog("==ADD ROW==");
                // Constant.showLog(addRow);
                if (widget.addressList == null) widget.addressList = [];
                if (widget.action == 'add_address')
                  widget.addressList.add(addRow);
                else
                  widget.addressList[widget.selpos!] = addRow;
                String addJsonarray = json.encode(widget.addressList);
//                      Constant.showLog(widget.addressList);
//                      Constant.showLog(addJsonarray);

                Map res = await updateAddress(addJsonarray);
                if (res.containsKey('success')) {
                  if (res['success'] == 101) {
                    showToast("No Internet Connection");
                  } else if (res['success'] == 1) {
                    if (widget.action == 'add_address')
                      showToast('Address Added');
                    else
                      showToast('Address Updated');
                    Get.back(result: widget.addressList);
                  } else if (res.containsKey('message')) {
                    showToast(res['message']);
                  }
                }
              }
            }),
          ),
          const SizedBox(
            height: 5,
          )
        ],
      ),
    );
  }

}
