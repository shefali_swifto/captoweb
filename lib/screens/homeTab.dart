import 'package:capto/routes/route_helper.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/screens/dialogLayout/deliveryTypeDialog.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:image_network/image_network.dart';

class HomeTab extends StatefulWidget {
  static GlobalKey<_HomeTabState> menuGlobalKey = GlobalKey();
  Outlet currentOutlet;
  List catList;
  ItemScrollController scrollController;
  Function changeTab;
  TextEditingController deliveryTypeCtrl;

  HomeTab(
      {Key? key,
      required this.currentOutlet,
      required this.catList,
      required this.scrollController,
      required this.changeTab,
      required this.deliveryTypeCtrl})
      : super(key: menuGlobalKey);

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  String rest_id = '',
      rest_uniq_id = '',
      rest_name = '',
      rest_address = '',
      rest_logo = '';
  String outlet_timings = '', outlet_distance = '';
  int s_res = -1;
  Outlet? selOutlet;
  List menuList = [], itemList = [];
  TextEditingController searchCtrl = TextEditingController();

  _checkBrandOutlets() async {
    if (widget.currentOutlet != null) {
      rest_id = widget.currentOutlet.id.toString();
      rest_uniq_id = widget.currentOutlet.restUniqueId!;
      rest_name = widget.currentOutlet.name!;
      rest_logo = widget.currentOutlet.logo!;
      rest_address = widget.currentOutlet.address!;
    }

    if (widget.currentOutlet.outletOpeningTiming != null &&
        widget.currentOutlet.outletOpeningTiming!.trim().length > 0) {
      outlet_timings =
          widget.currentOutlet.outletOpeningTiming!.substring(0, 5) +
              ' - ' +
              widget.currentOutlet.outletClosingTiming!.substring(0, 5);
    }
    if (widget.currentOutlet.distance != null &&
        widget.currentOutlet.distance!.isNotEmpty) {
      double dis = double.parse(widget.currentOutlet.distance.toString());
      if (dis != 0) {
        outlet_distance = dis.toStringAsFixed(2) + ' KM';
      }
    }
    Constant.decimal_value = widget.currentOutlet.decimalValue!;
    if (mounted) setState(() {});
  }

  Future openDialog() async {
    dynamic r = await showDialog(
        context: context,
        barrierDismissible: (widget.deliveryTypeCtrl.text.isNotEmpty),
        builder: (BuildContext context) => DeliveryTypeDialog(
              type: widget.deliveryTypeCtrl.text,
              allowDelivery: (selOutlet != null &&
                  selOutlet!.delivery_available != null &&
                  selOutlet!.capto_delivery_status != null &&
                  selOutlet!.capto_delivery_status == 'enable' &&
                  selOutlet!.delivery_available == "yes"),
            ));
    if (r != null && r is String) {
      widget.deliveryTypeCtrl.text = r;
      if (mounted) {
        setState(() {});
      }
    }
  }

  void resetSearch() {
    searchCtrl.text = '';
    if (mounted) {
      setState(() {
        itemList.addAll(menuList);
      });
    }
  }

  void categoryClick(int index) async {
    dynamic r = await Get.toNamed(
      RouteHelper.getCategoryItems({'cat': widget.catList, 'pos': index}),
    );
    if (r != null && r is String && r == 'opencart') {
      widget.changeTab();
    }
  }

  @override
  void initState() {
    super.initState();
    selOutlet = widget.currentOutlet;
    _checkBrandOutlets();
    Future.delayed(const Duration(milliseconds: 10), () {
      if (widget.deliveryTypeCtrl.text.isEmpty) openDialog();
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return Scaffold(
      body: SingleChildScrollView(
        //padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          children: [
            headerUI(),
            ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                //   maxCrossAxisExtent: MediaQuery.of(context).size.width / 2.8,
                //   mainAxisExtent: 130,
                //   mainAxisSpacing: 10.0, // Customize this value as needed
                //   crossAxisSpacing: 10.0, // Customize this value as needed
                // ),
                itemCount: widget.catList.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () => categoryClick(index),
                    child: catRowUI(index),
                  );
                }),
            // isPortrait
            //     ? GridView.builder(
            //         shrinkWrap: true,
            //         physics: const NeverScrollableScrollPhysics(),
            //         gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            //           maxCrossAxisExtent:
            //               MediaQuery.of(context).size.width / 2.8,
            //           mainAxisExtent: 130,
            //           mainAxisSpacing: 10.0, // Customize this value as needed
            //           crossAxisSpacing: 10.0, // Customize this value as needed
            //         ),
            //         itemCount: widget.catList.length,
            //         itemBuilder: (BuildContext context, int index) {
            //           return GestureDetector(
            //             onTap: () => categoryClick(index),
            //             child: catRowUI(index),
            //           );
            //         },
            //       )
            //     : GridView.builder(
            //         physics: const NeverScrollableScrollPhysics(),
            //         shrinkWrap: true,
            //         gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            //           maxCrossAxisExtent: MediaQuery.of(context).size.width / 7,
            //           mainAxisExtent: 130,
            //           mainAxisSpacing: 10.0,
            //           crossAxisSpacing: 10.0,
            //         ),
            //         itemCount: widget.catList.length,
            //         itemBuilder: (BuildContext context, int index) {
            //           return GestureDetector(
            //             onTap: () => categoryClick(index),
            //             child: catRowUI(index),
            //           );
            //         },
            //       ),
            footerUI()
          ],
        ),
      ),
    );
  }

  Widget catRowUI(int index) {
    String? imgurl = widget.catList[index]['cuisine_image_url'];
    return Card(
      //margin: const EdgeInsets.all(10),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  widget.catList[index]['cuisine_name'],
                  //textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Spacer(),
                const Icon(Icons.navigate_next)
              ],
            )
          ],
        ),
      ),
    );
    //(imgurl != null && imgurl.isNotEmpty)
    // ? Card(
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       mainAxisSize: MainAxisSize.min,
    //       children: [
    //         // CircleAvatar(
    //         //   radius: 41,
    //         //   backgroundColor: Colors.white,
    //         //   child:
    //         ImageNetwork(
    //           image: imgurl,
    //           height: 80,
    //           width: 80,
    //           duration: 1500,
    //           curve: Curves.easeIn,
    //           onPointer: true,
    //           debugPrint: false,
    //           fullScreen: false,
    //           fitAndroidIos: BoxFit.cover,
    //           fitWeb: BoxFitWeb.cover,
    //           borderRadius: BorderRadius.circular(70),
    //           onLoading: CircularProgressIndicator(
    //             color: mainClr,
    //           ),
    //           onError: Image.asset(
    //             'assets/ic_restaurant.png',
    //             color: Colors.red,
    //             height: 80,
    //             width: 18,
    //           ),
    //           onTap: () => categoryClick(index),
    //         ),
    //         // ),
    //         const SizedBox(height: 3),
    //         Flexible(
    //           child: Text(
    //             widget.catList[index]['cuisine_name'],
    //             textAlign: TextAlign.center,
    //             style: const TextStyle(
    //               fontSize: 18,
    //               fontWeight: FontWeight.bold,
    //               overflow: TextOverflow.ellipsis,
    //             ),
    //           ),
    //         ),
    //       ],
    //     ),
    //   )
    // : Card(
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       mainAxisSize: MainAxisSize.min,
    //       children: [
    //         // CircleAvatar(
    //         //   radius: 40,
    //         //   backgroundColor: Colors.black12,
    //         //   child:
    //         Image.asset(
    //           'assets/ic_restaurant.png',
    //           height: 80,
    //           width: 20,
    //           color: mainClr,
    //         ),
    //         // ),
    //         const SizedBox(
    //           height: 6,
    //         ),
    //         Flexible(
    //           child: Text(
    //             widget.catList[index]['cuisine_name'],
    //             textAlign: TextAlign.center,
    //             style: const TextStyle(
    //               fontSize: 18,
    //               fontWeight: FontWeight.bold,
    //             ),
    //           ),
    //         ),
    //       ],
    //     ),
    //   ),
    //);
  }

  Widget headerUI() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  widget.currentOutlet.name!,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                flex: 1,
              ),
              Container(
                width: 120,
                alignment: Alignment.center,
                child: TextFormField(
                  controller: widget.deliveryTypeCtrl,
                  readOnly: true,
                  style: const TextStyle(fontSize: 14),
                  onTap: () => openDialog(),
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.only(
                      left: 15,
                      right: 10,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(color: defaultClr)),
                    suffixIcon: Image.asset(
                      'assets/ic_down.png',
                      height: 15,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget footerUI() {
    String address = widget.currentOutlet.address!;
    if (outlet_distance != null && outlet_distance.isNotEmpty)
      address = address + '(${outlet_distance})';
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.only(bottom: 20),
      child: Card(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.only(top: 8, bottom: 8),
                  child: Text(
                    widget.currentOutlet.name!,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w800),
                  )),
              if (widget.currentOutlet.description != null &&
                  widget.currentOutlet.description!.isNotEmpty)
                Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    widget.currentOutlet.description!,
                  ),
                ),
              Container(
                margin: const EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/icon_location.png',
                      width: 17,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      address,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/ic_clock.png',
                      width: 15,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      outlet_timings,
                    ),
                  ],
                ),
              ),
              if (widget.currentOutlet.note != null &&
                  widget.currentOutlet.note!.isNotEmpty)
                Container(
                  margin: const EdgeInsets.only(top: 15),
                  alignment: Alignment.center,
                  child: Text(
                    widget.currentOutlet.note!,
                    style: const TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 14,
                    ),
                  ),
                ),
              Container(
                margin: const EdgeInsets.only(top: 15, bottom: 15),
                child: const Divider(
                  height: 0.5,
                ),
              ),
              Container(
                //  alignment: Alignment.center,
                child: const Text(
                  'Powered by RoyalPOS',
                  style: TextStyle(fontSize: 12),
                ),
              ),
              const SizedBox(
                height: 5.0,
              ),
              if (s_res == Constant.api_no_net)
                SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                  child: NoInternetDialog(
                    retryClick: () async {
                      if (mounted) {
                        setState(() {
                          s_res = -1;
                        });
                      }
                    },
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    searchCtrl.dispose();
    super.dispose();
  }
}
