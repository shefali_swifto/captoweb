import 'package:capto/DBOrder.dart';
import 'package:capto/apis/dataAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/screens/dialogLayout/variationDialog.dart';
import 'package:capto/ui/itemListRow.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class CategoryItemScreen extends StatefulWidget {
  List catList;
  int pos;
  Outlet currentOutlet;

  CategoryItemScreen(
      {Key? key,
      required this.catList,
      required this.pos,
      required this.currentOutlet})
      : super(key: key);

  @override
  State<CategoryItemScreen> createState() => _CategoryItemScreenState();
}

class _CategoryItemScreenState extends State<CategoryItemScreen> {
  int? catIndex;
  Map? cat, itemRes;
  List itemList = [];

  void addClick(Map featuredItems) {
    if (Constant.selidlist != null && Constant.selidlist.length > 0) {
      if (Constant.dishorederlist[0].rest_id ==
              widget.currentOutlet.id.toString() &&
          Constant.dishorederlist[0].brandid == widget.currentOutlet.brand_id) {
        _checkVariation(featuredItems);
      } else {
        _showAlert(featuredItems);
      }
    } else {
      _checkVariation(featuredItems);
    }
  }

  Future incClick(Map featuredItems) async {
    if (Constant.selidlist != null) {
      if (featuredItems.containsKey("preflag") &&
          featuredItems['preflag'] == 'true') {
        dynamic r = showDialog(
          context: context,
          builder: (_) => DialogVariation(
            item: featuredItems,
            currentOutlet: widget.currentOutlet,
            isUpdate: false,
            updatePOS: 0,
          ),
        );
        if (r != null && r is bool) {
          if (r) {
            Provider.of<AppModel>(context, listen: false).increment();
            if (mounted) {
              setState(() {});
            }
          }
        }
      } else {
        int pos =
            Constant.selidlist.indexOf(featuredItems['dishid'].toString());
        DishOrderPodo d = Constant.dishorederlist[pos];
        int q = int.parse(d.qty.toString()) + 1;
        d.qty = q.toString();
        await DBProvider().updateQty(pos, d.qty!, d.orderdetailid!);
        Constant.dishorederlist[pos] = d;
        Provider.of<AppModel>(context, listen: false).increment();
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  Future decClick(Map featuredItems) async {
    if (Constant.selidlist != null) {
      if (featuredItems.containsKey("preflag") &&
          featuredItems['preflag'] == 'true') {
        CustomUI.customAlert(
            "",
            "This item has multiple customizations added. Proceed to cart to remove item",
            context);
      } else {
        int pos =
            Constant.selidlist.indexOf(featuredItems['dishid'].toString());
        DishOrderPodo d = Constant.dishorederlist[pos];
        int q = int.parse(d.qty.toString()) - 1;
        if (q != 0) {
          d.qty = q.toString();
          await DBProvider().updateQty(pos, d.qty!, d.orderdetailid!);
          Constant.dishorederlist[pos] = d;
        } else {
          await DBProvider().removeItemById(pos, d.orderdetailid!);
          Constant.dishorederlist.removeAt(pos);
          Constant.selidlist.removeAt(pos);
        }
        Provider.of<AppModel>(context, listen: false).increment();
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  _checkVariation(Map featuredItems) async {
    if (featuredItems.containsKey("preflag") &&
        featuredItems['preflag'] == 'true') {
      bool d_result = await openBottomSheetDialog(
          context,
          DialogVariation(
            item: featuredItems,
            currentOutlet: widget.currentOutlet,
            isUpdate: false,
            updatePOS: 0,
          ));

      if (d_result) {
        Provider.of<AppModel>(context, listen: false).increment();
        if (mounted) {
          setState(() {});
        }
      }
    } else {
      addItemToCart(featuredItems);
    }
  }

  addItemToCart(Map featuredItems) async {
    DishOrderPodo d = new DishOrderPodo();
    d.dishid = featuredItems['dishid'].toString();
    d.cusineid = featuredItems['cusineid'].toString();
    d.dishname = featuredItems['dishname'];
    d.dishtype = featuredItems['type'];
    d.description = featuredItems['description'];
    d.price = featuredItems['price'].toString();
    d.priceperdish = featuredItems['price'].toString();
    d.dishimage = featuredItems['dishimage_url'];
    d.preflag = featuredItems['preflag'].toString();
    d.sold_by = featuredItems['sold_by'].toString();
    d.priceper_without_tax = featuredItems['price_without_tax'].toString();
    d.price_without_tax = featuredItems['price_without_tax'].toString();
    d.tax_amt = featuredItems['tax_amt'].toString();
    d.offer = featuredItems['offers'].toString();
    d.discount = featuredItems['discount_amount'].toString();
    d.combo_flag = featuredItems['combo_flag'].toString();
    d.qty = "1";
    d.brandid = Constant.shopData!.brand_id.toString();
    d.rest_id = Constant.shopData!.id.toString();
    d.rest_uniq_id = Constant.shopData!.restUniqueId.toString();
    d.rest_name = Constant.shopData!.name.toString();
    d.rest_img = Constant.shopData!.logo.toString();
    d.rest_address = Constant.shopData!.address.toString();
    List tax_list = [];
    if (featuredItems.containsKey('tax_data')) {
      tax_list = featuredItems['tax_data'];
      if (tax_list != null && tax_list.length > 0) {
        double tot_tax = tax_list
            .map<double>((m) => double.parse(m['value']))
            .reduce((a, b) => a + b);
        d.tot_tax = tot_tax.toString();
      } else {
        d.tot_tax = '0';
      }
    } else {
      d.tot_tax = '0';
    }
    d.tax_data = tax_list;
    d.pre = featuredItems['pre'];
    d.preid = "";
    d.dish_comment = "";

    int r = await DBProvider().addOrderDetail(d);
    d.orderdetailid = r.toString();
    Constant.selidlist.add(featuredItems['dishid'].toString());
    Constant.dishorederlist.add(d);
    Provider.of<AppModel>(context, listen: false).increment();
    if (mounted) {
      setState(() {});
    }
  }

  _showAlert(Map featuredItems) {
    CustomUI.customAlertDialog(
        "Remove Item?",
        "Your cart contains items from " +
            Constant.dishorederlist[0].rest_name! +
            ". Do you want to discard the " +
            "selection and add items from " +
            widget.currentOutlet.name! +
            "?", () async {
      await DBProvider().deleteAll();
      if (mounted) {
        setState(() {
          Constant.dishorederlist.clear();
          Constant.selidlist.clear();
          Get.back();
          _checkVariation(featuredItems);
        });
      }
    }, () {
      Get.back();
    }, context);
  }

  void getItemList() async {
    itemRes = await getItemsByCusine(cat!['cuisine_id'].toString());

    if (itemRes != null && itemRes!.containsKey('success')) {
      if (itemRes!['success'] == 1) {
        itemList = itemRes!['data'];
      }
    }
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    catIndex = widget.pos;
    cat = widget.catList[catIndex!];
    super.initState();
    Future.delayed(const Duration(milliseconds: 10), () {
      getItemList();
    });
  }

  @override
  Widget build(BuildContext context) {
    cat = widget.catList[catIndex!];
    return Scaffold(
      appBar: AppBar(
        title: Text(cat!['cuisine_name']),
      ),
      body: Column(
        children: [
          Expanded(
              child: (itemRes == null)
                  ? CustomUI.CustProgress(context)
                  : (itemRes!.containsKey('success') &&
                          itemRes!['success'] == Constant.api_no_net)
                      ? NoInternetDialog(
                          retryClick: () {
                            if (mounted) {
                              setState(() {
                                itemRes = null;
                                getItemList();
                              });
                            }
                          },
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: itemList.length,
                          itemBuilder: (BuildContext context, int index) {
                            Map featuredItems = itemList[index];
                            return (featuredItems['sold_by'] == 'weight')
                                ? const SizedBox()
                                : ListProduct(
                                    outletStatus: widget
                                        .currentOutlet.outletCurrentStatus!,
                                    restid: widget.currentOutlet.id.toString(),
                                    isPriceWT: widget
                                        .currentOutlet.showItemPriceWithoutTax!,
                                    decimal_val: int.parse(
                                        widget.currentOutlet.decimalValue!),
                                    data: featuredItems,
                                    clickAdd: () => addClick(featuredItems),
                                    clickInc: () => incClick(featuredItems),
                                    clickDec: () => decClick(featuredItems),
                                    clickView: () async {
                                      if (featuredItems['preflag'].toString() ==
                                          'false') {
                                        dynamic r = await Get.toNamed(
                                            RouteHelper.getItemDetailRoute({
                                          'item': featuredItems,
                                          'isPriceWT': widget.currentOutlet
                                              .showItemPriceWithoutTax!,
                                          'restid': widget.currentOutlet.id
                                              .toString(),
                                          'outletStatus': widget.currentOutlet
                                              .outletCurrentStatus!,
                                          'currentOutlet':
                                              widget.currentOutlet.toJson(),
                                        }));
                                        if (mounted) {
                                          setState(() {});
                                        }
                                      } else {
                                        _checkVariation(featuredItems);
                                      }
                                    },
                                    isLast: (itemList.length - 1 == index),
                                  );
                          },
                        )),
          Consumer<AppModel>(
            builder: (context, counter, child) => Container(
              color: mainClr.withOpacity(0.05),
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      'Total: ' + Constant.currency + '${counter.exp_grand}',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  // SwiftoBtn1('View Cart1', ()=>Get.back(result: 'opencart')),
                  TextButton(
                    onPressed: () => Get.back(result: 'opencart'),
                    child: Container(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      constraints: const BoxConstraints(minHeight: 40),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(color: BTNBGCLR, width: 1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(5))),
                      child: Text(
                        'View Cart',
                        style: TextStyle(color: BTNBGCLR),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        margin: const EdgeInsets.only(bottom: 40),
        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
        decoration: BoxDecoration(
            color: BTNBGCLR,
            borderRadius: const BorderRadius.all(Radius.circular(5))),
        child: InkWell(
          onTap: () {
            searchCat();
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                'assets/ic_restaurant.png',
                color: BTNTXTCLR,
                width: 20,
              ),
              const SizedBox(
                width: 5,
              ),
              const Text(
                'Menu',
                style: TextStyle(color: BTNTXTCLR),
              )
            ],
          ),
        ),
      ),
    );
  }

  searchCat() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        List list = [];
        list.addAll(widget.catList);
        const Align(alignment: Alignment.bottomCenter);
        return AlertDialog(
          titlePadding: const EdgeInsets.all(10),
          contentPadding: const EdgeInsets.only(top: 10),
          content: Container(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
            height: MediaQuery.of(context).size.height / 2,
            width: MediaQuery.of(context).size.width / 2,
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                Map om = list[index];
                return (om['cuisine_dish_count'] > 0)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: InkWell(
                              onTap: () async {
                                catIndex = index;
                                cat = om;
                                Get.back();
                                if (mounted) {
                                  setState(() {
                                    itemRes = null;
                                    getItemList();
                                  });
                                }
                              },
                              child: Container(
                                padding:
                                    const EdgeInsets.only(top: 7, bottom: 7),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                        (om['cuisine_id'] == 0)
                                            ? 'Popular Items'
                                            : om['cuisine_name'],
                                        style: const TextStyle(
                                          fontSize: 17,
                                        )),
                                    Text(om['cuisine_dish_count'].toString(),
                                        style: const TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.normal)),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : const SizedBox();
              },
            ),
          ),
        );
      },
    );
  }
}
