
import 'package:capto/models/app.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';


class PaymentScreen extends StatefulWidget {
  Map orderJson, transJson;
  String currency = '';

  PaymentScreen(
      {Key? key,
      required this.orderJson,
      required this.transJson,
      required this.currency})
      : super(key: key);

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String url = '', successURL = '', failURL = '';

  @override
  void initState() {
    super.initState();
    url = widget.transJson['payment_url']['redirect_url'];
    successURL = widget.transJson['payment_url']['success_url'];
    failURL = widget.transJson['payment_url']['fail_url'];
    FirebaseHelper(context);

  }

  void backClick() {
    CustomUI.customAlertDialog(
        "Cancel Payment", "Do you want to cancel payment?", () async {
      Get.back();
      Provider.of<AppModel>(context, listen: false).changeTab(0);
      Get.offNamedUntil(RouteHelper.getMainRoute(), (route) => false);
    }, () {
      Get.back();
    }, context);
  }

  void failUI() {
    CustomUI.customAlert('Payment Failed', '', context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        backClick();
        return false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Image.asset(
              'assets/ic_back.png',
              height: 30,
            ),
            onPressed: backClick,
          ),
          centerTitle: true,
          title: const Text('Payment'),
          //elevation: 0.0,
        ),
        body:Container()
        // WebView(
        //   initialUrl: 'Loading...',
        //   onWebViewCreated: (WebViewController webViewController) {
        //     _controller=webViewController;
        //     _controller.loadUrl(url);
        //   },
        //   pageStart: (String pageurl) async {
        //     Constant.showLog('Page started loading: $pageurl');
        //     if (pageurl == successURL) {
        //       String brandid = Constant.dishorederlist[0].brandid!;
        //       String runiqid =
        //           Constant.dishorederlist[0].rest_uniq_id.toString();
        //
        //       Constant.selidlist.clear();
        //       Constant.dishorederlist.clear();
        //          await DBProvider().deleteAll();
        //       Provider.of<AppModel>(context, listen: false).increment();
        //       Get.offNamed(RouteHelper.getOrderPlacedRoute({
        //         'orderJson': widget.orderJson,
        //         'transJson': widget.transJson,
        //         'currency': widget.currency,
        //         'runiqid': runiqid,
        //         'brandid': brandid
        //       }));
        //     } else if (pageurl == failURL) {
        //       failUI();
        //     }
        //   },
        // )
      ),
    );
  }
}
