import 'package:capto/apis/authenticationAPI.dart';
import 'package:capto/models/app.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/ui/swiftoTxtField.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';


class VerifyCode extends StatefulWidget {
  final bool fromCart;
  final String? phoneNumber;
  final String? verId;
  ConfirmationResult? cResult;

  VerifyCode({this.fromCart = false, this.verId, this.phoneNumber,this.cResult});

  @override
  _LoginSMSState createState() => _LoginSMSState();
}

class _LoginSMSState extends State<VerifyCode> with TickerProviderStateMixin {
  TextEditingController _otpController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _otpController.dispose();
    super.dispose();
  }

  _loginSMS(smsCode, context) async {
    try {
      // final AuthCredential credential = PhoneAuthProvider.credential(
      //   verificationId: widget.verId!,
      //   smsCode: smsCode,
      // );

      final UserCredential? user =await widget.cResult!.confirm(smsCode);
        //  (await FirebaseAuth.instance.signInWithCredential(credential)).user;
      if (user != null) {
        Constant.showLog("===user===");
        Constant.showLog(user.user!.phoneNumber!);
        checkUser();
      } else {
        Constant.showLog("===fail2===");
        Constant.showLog("invalidSMSCode");
      }
    } catch (e) {
      Constant.showLog("===fail3===");
      Constant.showLog(e.toString());
    }
  }

  checkUser() async {
    CustomUI.showProgress(context);
    String ph =
        widget.phoneNumber.toString().replaceFirst(Constant.country_code, '');
    Map res = await checkPhone(ph);
    CustomUI.hideProgress(context);
    if (res.containsKey('success')) {
      if (res['success'] == 101) {
        CustomUI.customAlert("", "No Internet Connection", context);
      } else if (res['success'] == 1) {
        Provider.of<AppModel>(context, listen: false).changeTab(0);
        await KeyConstant.saveUserInfo(res['user']);
        Get.offNamed(RouteHelper.getOutletDetail());
      } else if (res['success'] == 0) {
        Get.offNamed(RouteHelper.getSignUpRoute(ph));
      } else if (res.containsKey('message')) {
        showToast(res['message']);
      }
    } else {
      showToast("Login failed");
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    'Phone Number Verification',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
                  child: RichText(
                    text: TextSpan(
                        text: "Enter the code sent to ",
                        children: [
                          TextSpan(
                              text: widget.phoneNumber,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15)),
                        ],
                        style: TextStyle(fontSize: 15)),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SwiftoTxtField(_otpController, "Enter OTP",
                    TextInputAction.done, TextInputType.number, false, 6, null),
                Center(
                  child: SwiftoBtn("Verify", () {
                    if (_otpController.text.length != 6)
                      showToast("Please enter valid OTP");
                    else
                      _loginSMS(_otpController.text, context);
                  }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
