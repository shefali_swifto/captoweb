import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OutletDetail extends StatefulWidget {
  const OutletDetail({Key? key}) : super(key: key);

  @override
  State<OutletDetail> createState() => _OutletDetailState();
}

class _OutletDetailState extends State<OutletDetail> {
  @override
  void initState() {
    super.initState();
    Constant.deliveryType = KeyConstant.type_takeaway_txt;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                outletDetailUI(),
                SizedBox(
                  height: 20,
                ),
                Text('Choose Delivery Type',),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  decoration: BoxDecoration(
                      border: Border.all(color: defaultClr, width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: Row(
                    children: [
                      typeUI(KeyConstant.type_takeaway_txt),
                      if (Constant.shopData != null &&
                          Constant.shopData!.delivery_available != null &&
                          Constant.shopData!.capto_delivery_status != null &&
                          Constant.shopData!.capto_delivery_status ==
                              'enable' &&
                          Constant.shopData!.delivery_available == "yes")
                        typeUI(KeyConstant.type_delivery_txt)
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 9,
                ),

                Container(
                  height: 50,
                  child: SwiftoBtn('Go To Menu', () {
                    Get.offNamedUntil(
                        RouteHelper.getMainRoute(), (route) => false);
                  }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget outletDetailUI() {
    return Column(
      children: [
        Text(
          Constant.shopData!.name!,
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w800),
        ),
        SizedBox(
          height: 5,
        ),
        if (Constant.shopData!.address != null &&
            Constant.shopData!.address!.isNotEmpty)
          Text(Constant.shopData!.address!),
        SizedBox(
          height: 5,
        ),
        if (Constant.shopData!.phno != null &&
            Constant.shopData!.phno!.isNotEmpty)
          Text(Constant.shopData!.phno!),
        SizedBox(
          height: 5,
        ),
      ],
    );
  }

  Widget typeUI(String txt) {
    return Expanded(
      flex: 1,
      child: InkWell(
        onTap: (){
          Constant.deliveryType = txt;
          if (mounted) {
            setState(() {});
          }
        },
        child: Container(
          decoration:
              (Constant.deliveryType != null && txt == Constant.deliveryType)
                  ? BoxDecoration(
                      color: defaultClr,
                      borderRadius: BorderRadius.all(Radius.circular(5)))
                  : null,
          height: 50,
          alignment: Alignment.center,
          child: Text(
            txt,
            style: TextStyle(
                fontSize: 20,
                fontWeight: (Constant.deliveryType != null &&
                        txt == Constant.deliveryType)
                    ? FontWeight.bold
                    : FontWeight.normal,
                color: (Constant.deliveryType != null &&
                        txt == Constant.deliveryType)
                    ? BTNTXTCLR
                    : null),
          ),
        ),
      ),
    );
  }
}
