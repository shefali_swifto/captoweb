import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeliveryTypeDialog extends StatefulWidget {
  String type;
  bool allowDelivery = true;
  DeliveryTypeDialog({Key? key, required this.type, this.allowDelivery= true})
      : super(key: key);

  @override
  State<DeliveryTypeDialog> createState() => _DeliveryTypeDialogState();
}

class _DeliveryTypeDialogState extends State<DeliveryTypeDialog> {
  bool isFirstTime=true;

  @override
  void initState() {
    super.initState();
    isFirstTime=(widget.type.isEmpty);
  }
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Choose Delivery Type',style: TextStyle(fontSize: 16),textAlign: TextAlign.center,),
      content: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: TextButton(
                  onPressed: () {
                    widget.type = KeyConstant.type_takeaway_txt;
                    if (mounted) {
                      setState(() {});
                    }
                  },
                  child: Text(
                    KeyConstant.type_takeaway_txt,
                    style: TextStyle(
                      fontSize: 16,
                      color: Theme.of(context).textTheme.bodySmall!.color,
                        fontWeight: (widget.type == KeyConstant.type_takeaway_txt)
                            ? FontWeight.bold
                            : null),
                  )),
            ),
            Divider(),
            if (widget.allowDelivery)
              Container(
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                    onPressed: () {
                      widget.type = KeyConstant.type_delivery_txt;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                    child: Text(
                      KeyConstant.type_delivery_txt,
                      style: TextStyle(
                          fontSize: 16,
                          color: Theme.of(context).textTheme.bodySmall!.color,
                          fontWeight:
                              (widget.type == KeyConstant.type_delivery_txt)
                                  ? FontWeight.bold
                                  : null),
                    )),
              ),
          ],
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              if(widget.type.isNotEmpty){
                Get.back(result: widget.type);
              }else{
                KeyConstant().showToast('Please choose delivery type', context);
              }
            },
            child: Text('Submit')),
        if(!isFirstTime)
        TextButton(
            onPressed: () => Get.back(),
            child: Text(
              'Close',
              style: TextStyle(color: Colors.red),
            )),
      ],
    );
  }
}
