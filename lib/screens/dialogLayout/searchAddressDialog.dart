import 'package:capto/apis/dataAPI.dart';
import 'package:flutter/material.dart';

class SearchAdderssDialog extends StatefulWidget {
  const SearchAdderssDialog({Key? key}) : super(key: key);

  @override
  State<SearchAdderssDialog> createState() => _SearchAdderssDialogState();
}

class _SearchAdderssDialogState extends State<SearchAdderssDialog> {
  TextEditingController _ctrl = TextEditingController();
  List list = [];

  void getData() async {
    list = [];
    dynamic res = await searchLocation(_ctrl.text);
    if (res != null && res is Map) {
      if (res['predictions'] != null && res['predictions'] is List) {
        list = res['predictions'];
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    _ctrl.addListener(() {
      if (_ctrl.text.isNotEmpty) {
        getData();
      } else {
        list.clear();
        if (mounted) {
          setState(() {});
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.5),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Card(
              child: Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Image.asset('assets/ic_back.png',width: 18,)),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    flex: 1,
                    child: TextFormField(
                      controller: _ctrl,
                      decoration: const InputDecoration(
                          hintText: 'Search',
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          enabledBorder: InputBorder.none),
                    ),
                  ),
                ],
              ),
            ),
            if (list.isNotEmpty)
              Container(
                color: Theme.of(context).primaryColor,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: list.length,
                    itemBuilder: (BuildContext context, int index) {
                      dynamic a = list[index];
                      return Column(
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.of(context).pop(a);
                            },
                            child: Container(
                              padding: const EdgeInsets.all(15),
                              width: MediaQuery.of(context).size.width,
                              child: Text(a['description']),
                            ),
                          ),
                          const Divider(
                            height: 1,
                          )
                        ],
                      );
                    }),
              )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _ctrl.dispose();
    super.dispose();
  }
}
