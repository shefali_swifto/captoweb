
import 'package:capto/DBOrder.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/VariationPodo.dart';
import 'package:capto/models/modifierCatPojo.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/models/preModelPojo.dart';
import 'package:capto/ui/preUI.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/ui/swiftoQty.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogVariation extends StatefulWidget {
  Map item;
  Outlet currentOutlet;
  bool isUpdate = false;
  int updatePOS;

  DialogVariation(
      {Key? key,
        required this.item,
        required this.currentOutlet,
        required this.isUpdate,
        required this.updatePOS})
      : super(key: key);

  @override
  _DialogVariationState createState() => _DialogVariationState();
}

class _DialogVariationState extends State<DialogVariation> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> vidlist = [];
  List<VariationPodo> vlist = [];
  double price = 0, price_wt = 0;
  int qty = 1, decimal_val = 2;
  String taxtype = "";
  List taxlist = [];
  List<ModifierCat> precatlist1 = [];
  List<Pre> prelist1 = [];
  double tax_per_tot = 0;
  String? rest_id, rest_uniq_id, rest_name, rest_address, rest_logo;

  showToast(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(CustomUI.showToast(msg, context));
  }

  void getData(){
    List precatlist = widget.item['modifier_cat'];
    List prelist = widget.item['pre'];
    precatlist.reversed.forEach((v) {
      precatlist1.add(new ModifierCat.fromJson(v));
    });
    prelist.forEach((v) {
      Pre p=new Pre.fromJson(v);
      if(vidlist.contains(p.preid)){
        p.isCheck=true;
        p.qty=(int.parse(vlist[vidlist.indexOf(p.preid!)].quantity!)/qty) as int;

        int mcatpos = precatlist1
            .indexWhere((element) => element.catId == p.pretype);
        ModifierCat mCat = precatlist1[mcatpos];
        mCat.tot_sel = (mCat.tot_sel != null) ? (mCat.tot_sel! + 1) : 1;
        precatlist1[mcatpos] = mCat;
      }
      prelist1.add(p);
    });
    if(mounted){
      setState(() {

      });
    }
  }

  void selPre(Pre pre, int maxSelection) {
    int mcatpos =
    precatlist1.indexWhere((element) => element.catId == pre.pretype);
    int pos = prelist1.indexWhere((element) => element.preid == pre.preid);
    ModifierCat mCat = precatlist1[mcatpos];
    Pre selpre = prelist1[pos];
    if(maxSelection==1 && !selpre.isCheck!){
      List<Pre> selP=prelist1.where((element) => (element.isCheck! && element.pretype==mCat.catId)).toList();
      if(selP.isNotEmpty){
        int pos1=prelist1.indexWhere((element) => element.preid==selP[0].preid);
        Pre selpre1 = prelist1[pos1];
        mCat.tot_sel = mCat.tot_sel! - 1;
        precatlist1[mcatpos] = mCat;

        selpre1.isCheck = false;
        prelist1[pos1] = selpre1;
      }
    }
    if (selpre.isCheck!) {
      mCat.tot_sel = mCat.tot_sel! - 1;
      precatlist1[mcatpos] = mCat;

      selpre.isCheck = false;
      prelist1[pos] = selpre;

      if (mounted) setState(() {});
    }
    else {
      if (mCat.tot_sel! < maxSelection || maxSelection == 0) {
        mCat.tot_sel = (mCat.tot_sel != null) ? (mCat.tot_sel! + 1) : 1;
        precatlist1[mcatpos] = mCat;

        selpre.isCheck = true;
        prelist1[pos] = selpre;

        if (mounted) setState(() {});
      } else {
        showToast('Max $maxSelection Allowed');
      }
    }
  }

  void updateQty(Pre pre) {
    int pos = prelist1.indexWhere((element) => element.preid == pre.preid);
    Pre selpre = pre;
    prelist1[pos] = selpre;
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    super.initState();
    taxlist.clear();
    price = 0;
    price_wt = 0;
    taxtype = "";
    tax_per_tot = 0;
    if (widget.currentOutlet.decimalValue != null &&
        widget.currentOutlet.decimalValue!.isNotEmpty) {
      decimal_val = int.parse(widget.currentOutlet.decimalValue!);
    } else {
      decimal_val = 2;
    }

    if (widget.isUpdate) {
      DishOrderPodo d = Constant.dishorederlist[widget.updatePOS];
      price = double.parse(d.priceperdish!);
      price_wt = double.parse(d.priceper_without_tax!);
      qty = int.parse(d.qty!);
      if (d.varPojoList != null) {
        vlist = d.varPojoList!;
        vidlist = d.preid!.split(",");
      }
    } else {
      price = double.parse(widget.item['price'].toString());
      price_wt = double.parse(widget.item['price_without_tax'].toString());
    }
    if (widget.item.containsKey('tax_data')) {
      taxlist = widget.item['tax_data'];
      if (taxlist != null && taxlist.length > 0) {
        taxtype = taxlist[0]['tax_amount'];
        tax_per_tot = taxlist
            .map<double>((m) => double.parse(m['value'].toString()))
            .reduce((a, b) => a + b);
      }
    }
    if (widget.currentOutlet != null) {
      rest_id = widget.currentOutlet.id.toString();
      rest_uniq_id = widget.currentOutlet.restUniqueId;
      rest_name = widget.currentOutlet.name;
      rest_logo = widget.currentOutlet.logo;
      rest_address = widget.currentOutlet.address;
    }
    getData();
    //  setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: IconButton(
                  icon: Image.asset('assets/ic_close.png',height: 30,),
                  tooltip: 'Close',
                  onPressed: () {
                    if(mounted) {
                      setState(() {
                        Get.back(result: false);
                      });
                    }
                  },
                ),
              ),
              Expanded(
                child: Text(
                  widget.item['dishname'],
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
                flex: 1,
              ),
              Container(
                margin: const EdgeInsets.only(right: 15.0),
                child: Text((Constant.shopData != null &&
                    Constant.shopData!.showItemPriceWithoutTax != null &&
                    Constant.shopData!.showItemPriceWithoutTax == 'true')?
                Constant.currency +
                    double.parse(price_wt.toString())
                        .toStringAsFixed(decimal_val):Constant.currency +
                    double.parse(price.toString())
                        .toStringAsFixed(decimal_val),
                  style: const TextStyle(fontSize: 17),
                ),
              )
            ],
          ),
          const Divider(height: 1,),
          Expanded(
            flex: 1,
            child: (precatlist1 != null && precatlist1.length > 0)
                ? Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(
                  top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
              child: Container(
                  child: PreUI(
                      precatlist: precatlist1,
                      prelist: prelist1,
                      updateQty: (Pre pre) {
                        updateQty(pre);
                      },
                      selPre: (Pre pre, int maxSelection) {
                        selPre(pre, maxSelection);
                      })

              ),
            )
                : const SizedBox(),
          ),
          const Divider(height: 1,),
          Container(
            padding: const EdgeInsets.fromLTRB(10,5,10,5),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 5.0),
                  child: const Text('QUANTITY:'),
                ),
                SwiftoQty(true, true, qty.toString(), () {
                  if(mounted) {
                    setState(() {
                      qty = qty + 1;
                    });
                  }
                }, () {
                  if(mounted) {
                    setState(() {
                      if (qty != 1) qty = qty - 1;
                    });
                  }
                }, null),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  flex: 1,
                  child: SwiftoBtn1("Add to order".toUpperCase(), () {
                    String varMsg = '';
                    precatlist1.forEach((element) {
                      if (element.tot_sel! < element.minSelection!) {
                        List<String> lnm = varMsg.split(',');
                        lnm.add(element.catName!);
                        lnm.remove("");
                        varMsg = lnm.join(',');
                      }
                    });
                    if (varMsg.isNotEmpty) {
                      showToast('Please select from ' + varMsg);
                    }else {
                      _addItemToCart();
                    }
                  }),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _addItemToCart() async {
    Constant.showLog("===Add to cart===");
    if (Constant.selidlist == null) Constant.selidlist = [];
    if (Constant.dishorederlist == null) Constant.dishorederlist = [];
    Map featuredItems = widget.item;
    int no_of_occur =await DBProvider().getNoOcurr(featuredItems['dishid'].toString());
    Constant.showLog(widget.isUpdate.toString() + " " + no_of_occur.toString());
    List<VariationPodo> vlist = [];
    String prefid='',prenm='';
    double vtot = 0, vtot_wt = 0;
    double priceperdish = double.parse(featuredItems['price'].toString());
    double pwtax = double.parse(featuredItems['price_without_tax'].toString());
    double newgst = double.parse(featuredItems['tax_amt'].toString());
    bool singleExist=prelist1.any((element) => element.type=='Single');
    await Future.wait(prelist1.map((element) async {
      if (element.isCheck!) {
        VariationPodo v = new VariationPodo();
        v.name = element.prenm;
        v.quantity = (int.parse(qty.toString()) * element.qty).toString();
        v.id = element.preid;
        Map priceCal = KeyConstant().priceCal(taxtype, tax_per_tot, double.parse(element.preprice!));
        v.amount = priceCal['cal_price'].toString();
        v.amount_wt =
            priceCal['cal_price_without_tax'].toString();
        vlist.add(v);
        if (prefid.isNotEmpty) {
          prefid = prefid + ',' + element.preid!;
          prenm = prenm +
              ',' +
              v.quantity! +
              ' X ' +
              element.prenm!;
        } else {
          prefid = element.preid!;
          prenm = v.quantity! + ' X ' + element.prenm!;
        }
        vtot = vtot + (element.qty * double.parse(v.amount!));
        vtot_wt =
            vtot_wt + (element.qty * double.parse(v.amount_wt!));
      }
    }));
    if(singleExist){
      newgst = vtot - vtot_wt;
      priceperdish = vtot;
      pwtax = vtot_wt;
    }else {
      newgst = newgst + (vtot - vtot_wt);
      priceperdish = priceperdish + vtot;
      pwtax = pwtax + vtot_wt;
    }
    DishOrderPodo d = new DishOrderPodo();
    DishOrderPodo? selPodo;
    int selpos = -1;
    d.dishid = featuredItems['dishid'].toString();
    d.cusineid = featuredItems['cusineid'].toString();
    d.dishname = featuredItems['dishname'];
    d.dishtype = featuredItems['type'];
    d.description = featuredItems['description'];
    d.price = priceperdish.toString();//price.toString();
    d.price_without_tax = pwtax.toString();//price_wt.toString();
    d.priceperdish = featuredItems['price'].toString();
    d.priceper_without_tax = featuredItems['price_without_tax'].toString();
    if (featuredItems.containsKey('dishimage_url')) {
      d.dishimage = featuredItems['dishimage_url'];
    }
    d.preflag = featuredItems['preflag'].toString();
    d.sold_by = featuredItems['sold_by'].toString();
    d.tax_amt = newgst.toString();
    d.offer = featuredItems['offers'].toString();
    d.discount = featuredItems['discount_amount'].toString();
    d.combo_flag = featuredItems['combo_flag'].toString();
    d.brandid=widget.currentOutlet.brand_id.toString();
    d.rest_id = rest_id!;
    d.rest_uniq_id = rest_uniq_id!;
    d.rest_name = rest_name!;
    d.rest_img = rest_logo!;
    d.rest_address = rest_address!;
    d.varPojoList = vlist;
    d.tax_data = taxlist;
    d.pre = featuredItems['pre'];
    d.tot_tax = tax_per_tot.toString();
    d.preid = prefid;//preid;
    d.dish_comment = "";
    d.qty = qty.toString();
    bool isAdd = false;
    if (widget.isUpdate) {
      d.orderdetailid = Constant.dishorederlist[widget.updatePOS].orderdetailid;
      d.dishimage = Constant.dishorederlist[widget.updatePOS].dishimage;
      await DBProvider().updateItem(widget.updatePOS, d);
      Constant.dishorederlist[widget.updatePOS] = d;
      Get.back(result: true);
    } else if (no_of_occur == 0) {
      isAdd = true;
    } else {
      isAdd = true;
      for (int i = 0; i < Constant.dishorederlist.length; i++) {
        DishOrderPodo selpos1 = Constant.dishorederlist[i];
        if (selpos1.dishid == featuredItems['dishid'].toString()) {
          if (prefid == selpos1.preid) {
            isAdd = false;
            selPodo = Constant.dishorederlist[i];
            selpos = i;
            break;
          } else if (prefid.trim().length == 0 &&
              selpos1.preid!.trim().length == 0) {
            isAdd = false;
            selpos = i;
            selPodo = Constant.dishorederlist[i];
            break;
          } else {
            List<String> plist = [];
            if (prefid != null && prefid.trim().length > 0) {
              plist = prefid.split(",");
            }
            List<String> selplist = [];
            if (selpos1.preid != null && selpos1.preid!.trim().length > 0) {
              selplist = selpos1.preid!.split(",");
            }
            if (plist.length == selplist.length) {
              if (plist.length > 0) {
                int c = 0;
                for (String s in plist) {
                  if (selplist.contains(s)) c++;
                }

                if (c == plist.length) {
                  isAdd = false;
                  selpos = i;
                  selPodo = Constant.dishorederlist[i];

                  break;
                }
              } else {
                isAdd = false;
                selpos = i;
                selPodo = Constant.dishorederlist[i];

                break;
              }
            }
          }
        }
      }
    }

    if (!widget.isUpdate) {
      if (isAdd) {
        int r = await DBProvider().addOrderDetail(d);
        d.orderdetailid = r.toString();
        Constant.selidlist.add(d.dishid!);
        Constant.dishorederlist.add(d);
        Get.back(result: true);
      } else if (selpos >= 0 && selPodo != null) {
        await DBProvider().updateQty(selpos, d.qty!, selPodo.orderdetailid.toString());
        selPodo.qty = d.qty;
        Constant.dishorederlist[selpos] = selPodo;
        Get.back(result: true);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
