import 'package:flutter/material.dart';

class EmptyCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      width: screenSize.width,
      child: FittedBox(
        fit: BoxFit.cover,
        child: Container(
          width:
              screenSize.width / (2 / (screenSize.height / screenSize.width)),
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Container(
                color: Theme.of(context).cardColor,
                padding: const EdgeInsets.all(10),
                child: Image.asset(
                  'assets/fast_food.png',
                  height: 120,
                ),
              ),
              const SizedBox(height: 20),
              Text("Your cart is empty",
                  style: TextStyle(
                      fontSize: 28, color: Theme.of(context).colorScheme.secondary),
                  textAlign: TextAlign.center),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text("Add something from the menu",
                    style: TextStyle(
                        fontSize: 16, color: Theme.of(context).colorScheme.secondary),
                    textAlign: TextAlign.center),
              ),
              const SizedBox(height: 50)
            ],
          ),
        ),
      ),
    );
  }
}
