import 'package:capto/util/keyConstant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class TimeSlotDialog extends StatefulWidget {
  final GlobalKey<ScaffoldState>? parentScaffoldKey;

  TimeSlotDialog({Key? key, this.parentScaffoldKey}) : super(key: key);

  @override
  _TimeSlotDialogState createState() => _TimeSlotDialogState();
}

class _TimeSlotDialogState extends State<TimeSlotDialog> {
  int pos = -1;
  DateTime dt_start = new DateTime.now();
  String tm = '';

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      titlePadding: EdgeInsets.fromLTRB(16, 25, 16, 0),
      title: Text(
        'Select Pickup Time',
       // style: Theme.of(context).textTheme.body2,
      ),
      children: <Widget>[
        setSlots('15 minutes', 15),
        setSlots('30 minutes', 30),
        setSlots('45 minutes', 45),
        setSlots('1 hour', 60),
        InkWell(
          onTap: () async {
            TimeOfDay? selectedTime = await showTimePicker(
              context: context,
              initialTime: new TimeOfDay.now(),
            );
            if (selectedTime != null) {
              if(mounted)
              setState(() {
                pos = 0;
                DateTime d = DateTime(dt_start.year, dt_start.month,
                    dt_start.day, selectedTime.hour, selectedTime.minute);
                if (d.isAfter(new DateTime.now()))
                  tm = DateFormat("HH:mm").format(d);
                else
                  tm = '';
              });
            }
          },
          child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(left: 15),
            child: Text((tm.isEmpty) ? 'Choose Time' : 'Selected Time: ' + tm,
                style: TextStyle(color: (pos == 0) ? Colors.blue : null)),
          ),
        ),
        Row(
          children: <Widget>[
            MaterialButton(
              onPressed: () {
                Get.back();
              },
              child: Text(
                'Cancel',
                style: TextStyle(color: Theme.of(context).hintColor),
              ),
            ),
            MaterialButton(
              onPressed: () {
                if (pos == -1 || (pos == 0 && tm.isEmpty))
                  showToast('Please select time');
                else {
                  if (pos != 0) {
                    dt_start = dt_start.add(new Duration(minutes: pos));
                    String stm =
                        DateFormat('yyyy-MM-dd HH:mm:ss').format(dt_start);
                    Get.back(result: stm);
                  } else {
                    List t1=tm.split(":");
                    dt_start = DateTime(dt_start.year, dt_start.month,
                        dt_start.day,int.parse(t1[0].toString()),int.parse(t1[1]));
                    Get.back(result: DateFormat('yyyy-MM-dd HH:mm:ss').format(dt_start));
                  }
                }
              },
              child: Text(
                'Submit',
                style: TextStyle(color: Theme.of(context).colorScheme.secondary),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.end,
        ),
        SizedBox(height: 10),
      ],
    );
  }

  setSlots(String title, int i) {
    return InkWell(
      onTap: () {
        if(mounted)
        setState(() {
          pos = i;
        });
      },
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(left: 15),
        child: Text(
          title,
          style: TextStyle(color: (i == pos) ? Colors.blue : null),
        ),
      ),
    );
  }
}
