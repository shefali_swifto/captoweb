import 'package:capto/apis/dineInAPI.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class PinDialog extends StatefulWidget {
  String tblid;
  String? tblNm;

  PinDialog({Key? key, required this.tblid, required this.tblNm})
      : super(key: key);

  @override
  State<PinDialog> createState() => _PinDialogState();
}

class _PinDialogState extends State<PinDialog> {
  GlobalKey<FormState> loginFormKey = new GlobalKey<FormState>();
  TextEditingController pinCtrl = TextEditingController();
  String pwd = '', errorMsg = '';

  Future login() async {
    Constant.dishorederlist.clear();
    Constant.selidlist.clear();
    Constant.placeitemlist.clear();
    CustomUI.showProgress(context);
    Map res = await checkPin(widget.tblid, Constant.shopData!.id.toString(),
        Constant.shopData!.restUniqueId!, Constant.shopData!.brand_id!, pwd);
    CustomUI.hideProgress(context);
    if (res.containsKey('success')) {
      if (res['success'] == 1) {
        if (res.containsKey('order_id')) {
          res['table_id'] = widget.tblid;
          res['table_nm']=(widget.tblNm!=null)?widget.tblNm:'';
          Get.offNamed(RouteHelper.getPlaceOrder(res));
        } else {
          startNewOrder();
        }
      } else if (res.containsKey('message')) {
        errorMsg = res['message'];
        if (mounted) {
          setState(() {});
        }
      }
    }
  }

  Future startNewOrder() async {
    CustomUI.showProgress(context);
    Map sRes = await startOrder(widget.tblid, Constant.shopData!.id.toString(),
        Constant.shopData!.restUniqueId!, Constant.shopData!.brand_id!, '');
    CustomUI.hideProgress(context);
    if (sRes.containsKey('success')) {
      if (sRes['success'] == 1) {
        sRes['table_id'] = widget.tblid;
        Get.offNamed(RouteHelper.getPlaceOrder(sRes));
      }
    }
  }

  @override
  void initState() {
    pinCtrl.addListener(() {
      if (errorMsg.isNotEmpty && pinCtrl.text.isNotEmpty) {
        errorMsg = '';
        if (mounted) {
          setState(() {});
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Please type Pin'),
      content: Form(
        key: loginFormKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              if (widget.tblNm != null && widget.tblNm!.isNotEmpty)
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.center,
                  child: Text(
                    widget.tblNm!,
                    style: TextStyle(fontSize: 18, color: mainClr),
                    textAlign: TextAlign.center,
                  ),
                ),
              TextFormField(
                controller: pinCtrl,
                decoration:
                    InputDecoration(hintText: 'Enter Pin', labelText: 'Pin'),
                keyboardType: TextInputType.number,
                maxLength: 4,
                obscureText: true,
                onSaved: (input) => this.pwd = input.toString(),
                validator: (input) {
                  if (input!.isEmpty) {
                    return 'Pin required';
                  }
                },
              ),
              if (errorMsg.isNotEmpty)
                Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text(
                      errorMsg,
                      style: TextStyle(color: Colors.red),
                    )),
              SizedBox(
                height: 10,
              ),
              SwiftoBtn('Check', () async {
                if (loginFormKey.currentState!.validate()) {
                  loginFormKey.currentState!.save();
                  login();
                }
              })
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    pinCtrl.dispose();
    super.dispose();
  }
}
