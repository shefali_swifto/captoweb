import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoTxtField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomerDialog extends StatefulWidget {
  const CustomerDialog({super.key});

  @override
  State<CustomerDialog> createState() => _CustomerDialogState();
}

class _CustomerDialogState extends State<CustomerDialog> {
  TextEditingController nmCtrl = TextEditingController();
  TextEditingController phCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Customer Detail'),
      content: SingleChildScrollView(
        child: Column(
          children: [
            SwiftoTxtField(nmCtrl, 'Name', TextInputAction.next,
                TextInputType.text, false, null, null),
            SwiftoTxtField(phCtrl, 'Contact Number', TextInputAction.next,
                TextInputType.phone, false, null, null),
          ],
        ),
      ),
      actions: [
        Row(
          children: [
            Expanded(
              flex: 1,
              child: SwiftoBtn('Close', () {
                Navigator.of(context).pop();
              }),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 1,
              child: SwiftoBtn('Submit', () {
                Navigator.of(context)
                    .pop({'name': nmCtrl.text, 'phone': phCtrl.text});
              }),
            )
          ],
        )
      ],
    );
  }
}
