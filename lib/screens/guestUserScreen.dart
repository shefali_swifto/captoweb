
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GuestUser extends StatefulWidget {
  String action, title;
  Function loginSuccess;
  GuestUser({
    Key? key,
    required this.action,
    required this.title,
    required this.loginSuccess
  }) : super(key: key);

  @override
  _GuestUserState createState() => _GuestUserState();
}

class _GuestUserState extends State<GuestUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(widget.title),
        elevation: 0.0,
        actions: <Widget>[
          (widget.action == 'profile')
              ? IconButton(
                  icon: Image.asset(
                    'assets/ic_info.png',
                    width: 18,
                    color: Theme.of(context).appBarTheme.iconTheme!.color,
                  ),
                  onPressed: () {
                    Get.toNamed(RouteHelper.getInfoRoute());
                  },
                )
              : SizedBox(),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Not Logged in',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28)),
            SizedBox(
              height: 5.0,
            ),
            Text('You are not currently signin',
                style: TextStyle(fontSize: 15)),
            SizedBox(
              height: 10.0,
            ),
            SwiftoBtn("Sign In".toUpperCase(), () async{
              dynamic r=await Get.toNamed(RouteHelper.getSignInRoute());
              if(r!=null && r is bool && r){
                widget.loginSuccess();
              }
            }),
          ],
        ),
      ),
    );
  }
}
