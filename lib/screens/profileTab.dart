import 'dart:convert';

import 'package:capto/apis/authenticationAPI.dart';

import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileTab extends StatefulWidget {
  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  bool isEdit = false;
  String nm = '',
      mail = '',
      phone = '',
      _city = '',
      country = '',
      countrycode = '',
      points = '';
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = new GlobalKey<FormState>();
  Map? res;

  getData() async {
    CustomUI.showProgress(context);
    res = await getProfile();
    CustomUI.hideProgress(context);
    if (mounted) setState(() {});
  }

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  _updateProfile(String nm, String mail, String city, String coun,
      String councode, String ph) async {
    CustomUI.showProgress(context);
    Map res1 = await updateProfile(nm, city, coun, councode, ph, mail);
    if (res1.containsKey('success')) {
      if (res1['success'] == 101) {
        CustomUI.hideProgress(context);
        showToast("No Internet Connection");
      } else if (res1['success'] == 1) {
        Map user = res1['user'];
        KeyConstant.saveUserInfo(user);
        CustomUI.hideProgress(context);
        if (mounted)
          setState(() {
            res = user;
            isEdit = false;
          });
      } else if (res1.containsKey('message')) {
        CustomUI.hideProgress(context);
        showToast(res1['message']);
      } else {
        CustomUI.hideProgress(context);
        showToast('Update profile failed');
      }
    } else
      CustomUI.hideProgress(context);
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 10), () {
      this.getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Text("Profile"),
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Image.asset('assets/ic_info.png',height: 20,),
              onPressed: () {
                Get.toNamed(RouteHelper.getInfoRoute());
                // Navigator.of(context).pushNamed(RouteGenerator.info);
              },
            ),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          children: <Widget>[
            (res != null)
                ? (res!.containsKey('name'))
                    ? _setProfile(res!)
                    : (res!.containsKey('success'))
                        ? (res!['success'] == Constant.api_no_net)
                            ? NoInternetDialog(
                                retryClick: () {
                                  getData();
                                },
                              )
                            : (res!.containsKey('message'))
                                ? Center(
                                    child: Text(res!['message']),
                                  )
                                : const SizedBox()
                        : const SizedBox()
                : const SizedBox(),
            const Divider(),
            const SizedBox(
              height: 20,
            )
          ],
        ));
  }

  _setProfile(Map user) {
    nm = '';
    mail = '';
    phone = '';
    _city = '';
    country = '';
    countrycode = '';
    points = '';
    if (user.containsKey('name')) nm = user['name'];
    if (user.containsKey('email')) mail = user['email'];
    if (user.containsKey("city")) _city = user['city'];
    if (user.containsKey("country_code")) countrycode = user['country_code'];
    if (user.containsKey("country")) country = user['country'];
    if (user.containsKey('phone_no')) phone = countrycode + user['phone_no'];
    if (user.containsKey('points')) points = user['points'].toString();

    List addressList = [];
    if (user.containsKey('address_json')) addressList = user['address_json'];
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text(
                  nm,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 18),
                ),
                subtitle: const Text(''), //'Your Point:' + points
                trailing: InkWell(
                  child: const Text("Logout",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: defaultClr,
                      )),
                  onTap: () {
                    CustomUI.customAlertDialog(
                        'Logout', 'Are you sure you want to logout?', () async {
                      Get.back();
                      logout(context);
                    }, () {
                      Get.back();
                    }, context);
                  },
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Account Information".toUpperCase(),
                      style: const TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    (!isEdit)
                        ? IconButton(
                            icon: Image.asset(
                              'assets/ic_edit.png',
                              height: 20.0,
                            ),
                            onPressed: () {
                              if (mounted)
                                setState(() {
                                  isEdit = true;
                                });
                            },
                            tooltip: "Edit",
                          )
                        : TextButton(
                            style: flatButtonStyle,
                            onPressed: () {
                              _formKey.currentState!.save();
                              // Constant.showLog('name:' + nm);
                              // Constant.showLog('email:' + mail);
                              // Constant.showLog('city:' + _city);
                              // Constant.showLog('country:' + country);
                              // Constant.showLog('country code:' + countrycode);
                              // Constant.showLog(
                              //     'phone:' + phone.replaceAll(countrycode, ''));
                              _updateProfile(
                                  nm,
                                  mail,
                                  _city,
                                  country,
                                  countrycode,
                                  phone.replaceAll(countrycode, ''));
                            },
                            child: const Text(
                              'update',
                              style: TextStyle(color: BTNTXTCLR),
                            )),
                  ],
                ),
              ),
              //Name
              ListTile(
                title: const Text(
                  "Name",
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                subtitle: (!isEdit)
                    ? Text(
                        (nm != null && nm.isNotEmpty) ? nm : '',
                      )
                    : TextFormField(
                        // controller: _nmController,
                        key: const Key('entername'),
                        initialValue: nm,
                        maxLines: 1,
                        decoration:
                            const InputDecoration(hintText: 'Enter name'),
                        onSaved: (val) => nm = val!,
                      ),
              ),
              //Email
              ListTile(
                title: const Text(
                  "Email",
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                subtitle: (!isEdit)
                    ? Text(
                        (mail != null && mail.isNotEmpty) ? mail : '',
                      )
                    : TextFormField(
                        key: const Key('enteremail'),
                        initialValue: mail,
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        decoration:
                            const InputDecoration(hintText: 'Enter email'),
                        onSaved: (val) => mail = val!,
                      ),
              ),
              ListTile(
                title: const Text(
                  "Phone",
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                subtitle: Text(
                  (phone != null && phone.isNotEmpty) ? phone : '',
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "You Saved Addresses".toUpperCase(),
                      style: const TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    InkWell(
                      child: Row(
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Theme.of(context).iconTheme.color,
                            child: Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: CircleAvatar(
                                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Image.asset('assets/ic_add.png',height: 15,),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 5.0,
                          ),
                          const Text(
                            'Add new',
                            style: TextStyle(color: defaultClr),
                          )
                        ],
                      ),
                      onTap: () async {
                        Get.toNamed(RouteHelper.getAddAddressRoute({
                          'action': 'add_address',
                          'addressList': user['address_json'],
                        }));
                        // await Navigator.of(context).pushNamed(RouteGenerator.addAddress,arguments: {
                        //   'action': 'add_address',
                        //   'addressList': user['address_json'],
                        // });
                        if (mounted) {
                          setState(() {
                            getData();
                          });
                        }
                      },
                    )
                  ],
                ),
              ),
              (addressList != null && addressList.length > 0)
                  ? ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: addressList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _setAddressRow(addressList, index);
                      })
                  : const SizedBox(
                      height: 0.0,
                    ),
            ],
          ),
        ));
  }

  _setAddressRow(List addressList, int index) {
    Map info = addressList[index];
    String custadd = info['address'];
    String landmark = info['landmark'];
    String homeno = info['home_no'];
    String fulladd = "";
    if (homeno != null && homeno.isNotEmpty) fulladd = homeno + ", ";
    if (landmark != null && landmark.isNotEmpty)
      fulladd = fulladd + landmark + ", ";
    fulladd = fulladd + custadd;

    return Container(
      padding: const EdgeInsets.all(5.0),
      margin: const EdgeInsets.only(bottom: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            title: Text(info['address_tag'].toString()),
            subtitle: Text(fulladd),
            trailing: PopupMenuButton<String>(
              padding: const EdgeInsets.all(0.0),
              onSelected: (String val) async {
                if (val == 'edit') {
                  await Get.toNamed(RouteHelper.getAddAddressRoute({
                    'action': 'edit_address',
                    'addressList': addressList,
                    'selpos': index,
                  }));
                  if (mounted) {
                    setState(() {
                      getData();
                    });
                  }
                } else if (val == 'del') {
                  List alist = addressList;
                  alist.removeAt(index);
                  String addJsonarray = json.encode(alist);
                  //  Constant.showLog(addJsonarray);

                  Map res = await updateAddress(addJsonarray);
                  if (res.containsKey('success')) {
                    if (res['success'] == 101) {
                      showToast("No Internet Connection");
                    } else if (res['success'] == 1) {
                      showToast('Address Deleted');
                      if (mounted) setState(() {});
                    } else if (res.containsKey('message')) {
                      showToast(res['message']);
                    }
                  }
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                PopupMenuItem<String>(
                  value: 'edit',
                  child: ListTile(
                    leading: Image.asset(
                      'assets/ic_edit.png',
                      height: 20,
                    ),
                    title: Text('Edit'),
                  ),
                ),
                PopupMenuItem<String>(
                  value: 'del',
                  child: ListTile(
                    leading: Image.asset(
                      'assets/ic_delete.png',
                      height: 20,
                    ),
                    title: Text('Delete'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
