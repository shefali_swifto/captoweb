import 'package:capto/apis/authenticationAPI.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/ui/swiftoTxtField.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterScreen extends StatefulWidget {
  String phone;

  RegisterScreen({Key? key, required this.phone}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _usernameControl = new TextEditingController();
  final TextEditingController _emailControl = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  void getLocation() async {
   // await Constant.getCurrentLocation();
  }

  _registerUser(String mail, String nm) async {
    CustomUI.showProgress(context);
    Map res = await registerUser(widget.phone, mail, nm);
    if (res['success'] == 101) {
      CustomUI.hideProgress(context);
      CustomUI.customAlert("", "No Internet Connection", context);
    } else if (res['success'] == 1) {
      await KeyConstant.saveUserInfo(res['user']);
      CustomUI.hideProgress(context);
      showToast("Thank you for registering with " + Constant.appName + ".");
      Get.offNamedUntil(RouteHelper.getOutletDetail(),(route)=>false);
      //Navigator.of(context).pushNamedAndRemoveUntil(RouteGenerator.mainScreen, (route) => false);
    } else if (res.containsKey('message')) {
      CustomUI.hideProgress(context);
      showToast(res['message']);
    }
  }

  @override
  void initState() {
    super.initState();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Register'),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 0, 20, 0),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(
                top: 25.0,
              ),
              child: Text(
                "Create an account",
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                  color: defaultClr,
                ),
              ),
            ),
            SizedBox(height: 30.0),
            SwiftoTxtField(_usernameControl,"Username",TextInputAction.next,TextInputType.text,false,null,null),
            SwiftoTxtField(_emailControl,"Email",TextInputAction.done,TextInputType.emailAddress,false,null,null),
            Center(
              child: SwiftoBtn("Register".toUpperCase(),() {
                if (_usernameControl.text.isEmpty) {
                  showToast('Name required');
                } else if (_emailControl.text.isEmpty) {
                  showToast('Email required');
                } else if (!_emailControl.text.contains('@') &&
                    !_emailControl.text.contains('.')) {
                  showToast('Email invalid');
                } else {
                  _registerUser(_emailControl.text, _usernameControl.text);
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}
