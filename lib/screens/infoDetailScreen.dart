import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:flutter/material.dart';
//import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

class InfoDetailScreen extends StatefulWidget {
  String action;

  InfoDetailScreen({
    Key? key,
    required this.action,
  }) : super(key: key);

  @override
  _InfoDetailScreenState createState() => _InfoDetailScreenState();
}

class _InfoDetailScreenState extends State<InfoDetailScreen> {
  String url = '', title = Constant.appName;
  //late PlatformWebViewController _controller;

  @override
  void initState() {
    super.initState();
    if (widget.action == 'terms') {
      title = 'Terms & Conditions';
      url = Constant.terms_url;
    } else if (widget.action == 'policy') {
      title = 'Privacy Policy';
      url = Constant.policy_url;
    } else if (widget.action == 'contact') {
      title = 'Contact us';
      url = Constant.contactus_url;
    }
    FirebaseHelper(context);
    // _controller = PlatformWebViewController(
    //   const PlatformWebViewControllerCreationParams(),
    // )..loadRequest(
    //     LoadRequestParams(
    //       uri: Uri.parse(url),
    //     ),
    //   );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(title),
          //elevation: 0.0,
        ),
        body:Container()
        //PlatformWebViewWidget(
          //PlatformWebViewWidgetCreationParams(controller: _controller),
        //).build(context)
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
