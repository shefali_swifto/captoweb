import 'package:capto/apis/authenticationAPI.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  Map? res;
  List? list;

  getData() async {
    CustomUI.showProgress(context);
    res = await getNotifications();
    if (res != null && res!.containsKey('success')) {
      if (res!['success'] == 1 && res!.containsKey('data')) list = res!['data'];
    }
    CustomUI.hideProgress(context);
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    super.initState();
    new FirebaseHelper(context);
    Future.delayed(Duration(milliseconds: 10), () {
      getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          "Notifications",
        ),
        elevation: 0.0,
      ),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: (list != null && list!.length > 0)
              ? ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: res!['data'].length,
                  itemBuilder: (BuildContext context, int index) {
                    return _setNotification(res!['data'][index]);
                  })
              : (list != null && list!.length == 0)
                  ? Center(
                      child: Text('No Data'),
                    )
                  : (res != null &&
                          res!.containsKey('success') &&
                          res!['success'] == Constant.api_no_net)
                      ? Container(child: NoInternetDialog(
                          retryClick: () {
                            getData();
                          },
                        ))
                      : (res != null && res!.containsKey('message'))
                          ? Center(
                              child: Text(res!['message']),
                            )
                          : SizedBox()),
    );
  }

  _setNotification(Map n) {
    DateFormat defaultfmt = new DateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dtfmt = new DateFormat("dd-MM-yyyy HH:mm");
    DateTime orderDateTime = defaultfmt.parse(n['created_at']);
    String nTime = dtfmt.format(orderDateTime);
    String img = n['image_name'];
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(n['message_text']),
          subtitle: Text(nTime),
          onTap: () {},
        ),
        Divider(),
      ],
    );
  }
}
