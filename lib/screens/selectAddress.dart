import 'package:capto/apis/authenticationAPI.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

class SelectAddress extends StatefulWidget {
  bool isChangeLocation;
  Outlet? outletData;
  Map? cartData;

  SelectAddress(
      {Key? key,
      required this.isChangeLocation,
      this.outletData,
      this.cartData})
      : super(key: key);
  @override
  _SelectAddressState createState() => _SelectAddressState();
}

class _SelectAddressState extends State<SelectAddress> {
  Map? current_loc;
  Map? res;
  List addressList = [];

  getData() async {
    CustomUI.showProgress(context);
    res = await getProfile();
    if (res!.containsKey('address_json')) addressList = res!['address_json'];
    CustomUI.hideProgress(context);
    if (addressList.length == 0) {
      Get.toNamed(RouteHelper.getAddAddressRoute({
        'action': 'add_address',
        'addressList': addressList,
      }));
    } else if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    _setCurrentLocation(false);
    new FirebaseHelper(context);
    Future.delayed(const Duration(milliseconds: 10), () {
      this.getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text((widget.isChangeLocation)
              ? "Choose Your Location"
              : "Select Address"),
          elevation: 0.0,
        ),
        body: (res != null &&
                res!.containsKey("success") &&
                res!['success'] == 101)
            ? NoInternetDialog(
                retryClick: () {
                  getData();
                },
              )
            : (res != null && res!.containsKey('message'))
                ? Center(child: Text(res!['message']))
                : _setAddress(addressList));
  }

  _setAddress(List addressList) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            crossAxisAlignment: (widget.isChangeLocation)
                ? CrossAxisAlignment.start
                : CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              (widget.isChangeLocation)
                  ? InkWell(
                      child: Row(
                        children: <Widget>[
                          Image.asset('assets/ic_location.png', color: defaultClr,width: 18,),
                          const SizedBox(
                            width: 5.0,
                          ),
                          const Text(
                            'Current Location',
                            style: TextStyle(color: defaultClr),
                          )
                        ],
                      ),
                      onTap: () {
                        if (current_loc != null) {
                          Get.back(result: current_loc);
                        } else
                          _setCurrentLocation(true);
                      },
                    )
                  : const SizedBox(),
              (!widget.isChangeLocation)
                  ? InkWell(
                      child: Row(
                        children: <Widget>[
                          Image.asset('assets/ic_add.png', color: defaultClr,height: 18,),
                          const SizedBox(
                            width: 5.0,
                          ),
                          const Text(
                            'Add new',
                            style: TextStyle(color: defaultClr),
                          )
                        ],
                      ),
                      onTap: () async {
                        dynamic r = await Get.toNamed(
                            RouteHelper.getAddAddressRoute({
                          'action': 'add_address',
                          'addressList': addressList
                        }));
                        if (r != null && r is List) {
                          this.addressList = r;
                          if (mounted) {
                            setState(() {});
                          }
                        }
                      },
                    )
                  : const SizedBox(),
            ],
          ),
        ),
        (addressList != null && addressList.length > 0)
            ? ListView.builder(
                shrinkWrap: true,
                primary: false,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: addressList.length,
                itemBuilder: (BuildContext context, int index) {
                  return _setAddressRow(addressList, index);
                })
            : const SizedBox(
                height: 0.0,
              ),
      ],
    );
  }

  _setAddressRow(List addressList, int index) {
    Map info = addressList[index];
    String custadd = info['address'];
    String landmark = info['landmark'];
    String homeno = info['home_no'];
    String fulladd = "";
    if (homeno != null && homeno.isNotEmpty) fulladd = homeno + ", ";
    if (landmark != null && landmark.isNotEmpty)
      fulladd = fulladd + landmark + ", ";
    fulladd = fulladd + custadd;

    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            title: Text(info['address_tag'].toString()),
            subtitle: Text(fulladd),
            onTap: () {
              Constant.sel_address = info;
              if (!widget.isChangeLocation) {
                Get.toNamed(RouteHelper.getCheckoutRoute({
                  'outletData': widget.outletData!.toJson(),
                  'cartData': widget.cartData!
                }));
              } else
                Get.back(result: info);
            },
          ),
          const SizedBox(height: 5,)
        ],
      ),
    );
  }

  _setCurrentLocation(bool isTap) async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      var lat1 = position.latitude.toString();
      var lang1 = position.longitude.toString();

      String lat = lat1.toString();
      String lng = lang1.toString();
      // List<Placemark> placemark = await Geolocator()
      //     .placemarkFromCoordinates(double.parse(lat1), double.parse(lang1));
      // String city = placemark[0].locality;
      // current_loc = {
      //   'city': city,
      //   'address_tag': 'Current Location',
      //   'latitude': lat,
      //   'longitude': lng
      // };
      if (isTap) Get.back(result: current_loc);
    } catch (e) {
      current_loc = null;
      return;
    }
  }
}
