import 'package:capto/models/app.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class OrderPlaced extends StatefulWidget {
  Map orderJson, transJson;
  String currency, runiqid, brandid;

  OrderPlaced(
      {Key? key,
      required this.orderJson,
      required this.transJson,
      required this.currency,
      required this.brandid,
      required this.runiqid})
      : super(key: key);
  @override
  _OrderPlacedState createState() => _OrderPlacedState();
}

class _OrderPlacedState extends State<OrderPlaced> {
  Map? d;

  @override
  void initState() {
    super.initState();
    d = widget.orderJson['order']['details'];
    new FirebaseHelper(context);
  }

  void backClick(){
    Provider.of<AppModel>(context, listen: false).changeTab(0);
    Get.offNamedUntil(RouteHelper.getMainRoute(),(route)=>false);
    //Navigator.of(context).pushNamedAndRemoveUntil(RouteGenerator.mainScreen, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        backClick();
        return false;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text('Confirmation'),
            centerTitle: true,
            leading: IconButton(
              icon: Image.asset(
                'assets/ic_back.png',
                width: 18,
              ),
              onPressed: backClick,
            ),
          ),
          body: Container(
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  alignment: AlignmentDirectional.center,
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            width: 150,
                            height: 150,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: LinearGradient(
                                    begin: Alignment.bottomLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      Colors.green.withOpacity(1),
                                      Colors.green.withOpacity(0.2),
                                    ])),
                            child: Image.asset(
                              'assets/ic_check.png',
                              color: Theme.of(context).scaffoldBackgroundColor,
                              width: 90,
                            ),
                          ),
                          Positioned(
                            right: -30,
                            bottom: -50,
                            child: Container(
                              width: 100,
                              height: 100,
                              decoration: BoxDecoration(
                                color: Theme.of(context)
                                    .scaffoldBackgroundColor
                                    .withOpacity(0.15),
                                borderRadius: BorderRadius.circular(150),
                              ),
                            ),
                          ),
                          Positioned(
                            left: -20,
                            top: -50,
                            child: Container(
                              width: 120,
                              height: 120,
                              decoration: BoxDecoration(
                                color: Theme.of(context)
                                    .scaffoldBackgroundColor
                                    .withOpacity(0.15),
                                borderRadius: BorderRadius.circular(150),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      Opacity(
                        opacity: 0.4,
                        child: Text(
                          'Your Order has been successfully placed!',
                          textAlign: TextAlign.center,
                          //  style: Theme.of(context).textTheme.display2.merge(TextStyle(fontWeight: FontWeight.w300)),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    height: 255,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            topLeft: Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.15),
                              offset: Offset(0, -2),
                              blurRadius: 5.0)
                        ]),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width - 40,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        // mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          //calRow('Subtotal',widget.currency+d['order_subtotal']),
                          Text(
                            'Order #' + widget.transJson['order_no'].toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                                fontSize: 18),
                          ),
                          Divider(height: 30),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Total',
                                 // style: Theme.of(context).textTheme.title,
                                ),
                              ),
                              Text(widget.currency + d!['order_total'],
                              //    style: Theme.of(context).textTheme.title
                              )
                            ],
                          ),
                          SizedBox(height: 20),
                          SizedBox(
                              width: MediaQuery.of(context).size.width - 40,
                              child: SwiftoBtn('View Order', () {
                                Provider.of<AppModel>(context, listen: false).changeTab(0);
                                Get.offNamed(RouteHelper.getOrderDetailRoute({
                                  'order_id': widget.transJson['order_id']
                                      .toString(),
                                  'rest_id': widget.orderJson['order']['store']['merchant_ref_id'],
                                  'runiq_id': widget.runiqid,
                                  'brandid': widget.brandid,
                                  'parentScreen': 'order_placed',
                                }));
                                // Navigator.of(context).pushReplacementNamed(RouteGenerator.orderDetail,arguments: {
                                //   'order_id': widget.transJson['order_id']
                                //       .toString(),
                                //   'rest_id': widget.orderJson['order']['store']['merchant_ref_id'],
                                //   'runiq_id': widget.runiqid,
                                //   'brandid': widget.brandid,
                                //   'parentScreen': 'order_placed',
                                // });

                              })),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  calRow(String title, String val) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Text(
                title,
              ),
            ),
            Text(val,)
          ],
        ),
        SizedBox(height: 3),
      ],
    );
  }
}
