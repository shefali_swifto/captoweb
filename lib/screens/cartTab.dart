import 'dart:convert';

import 'package:capto/DBOrder.dart';
import 'package:capto/apis/dataAPI.dart';
import 'package:capto/models/DishOrderPodo.dart';
import 'package:capto/models/TaxInvoicePodo.dart';
import 'package:capto/models/app.dart';
import 'package:capto/models/outlets.dart';
import 'package:capto/routes/route_helper.dart';
import 'package:capto/screens/dialogLayout/TimeSlotDialog.dart';
import 'package:capto/screens/dialogLayout/emptyCart.dart';
import 'package:capto/screens/dialogLayout/variationDialog.dart';
import 'package:capto/ui/cartItemRow.dart';
import 'package:capto/ui/home_screen.dart';
import 'package:capto/ui/swiftoBtn.dart';
import 'package:capto/ui/swiftoCustom.dart';
import 'package:capto/ui/swiftoTxtField.dart';
import 'package:capto/util/constant.dart';
import 'package:capto/util/keyConstant.dart';
import 'package:capto/util/styles.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

String d_date = '', d_time = '', ddt = '', dtm = '';

class CartTab extends StatefulWidget {
  Outlet? currentOutlet;
  String deliveryType;

  CartTab({
    Key? key,
    required this.currentOutlet,
    required this.deliveryType,
  }) : super(key: key);

  @override
  _CartTabState createState() => _CartTabState();
}

class _CartTabState extends State<CartTab> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String service_charge = "", currency = '', packtype = 'none';
  int decimal_val = 2;
  bool isLogin = false;
  final TextEditingController _noteController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  List<String>? taxidlist, dtlist, dt_dlist, tmlist, tm_dlist;
  List<TaxInvoicePodo>? taxInvoiceList;
  double subtot = 0.0,
      grand = 0.0,
      rounding = 0.0,
      packval = 0.0,
      dis_per = 0.0,
      dis_amt = 0.0;
  String? rounding_id,
      interval_val,
      delivery_type = '',
      is_advance_order = 'no',
      packCharge = "0";
  bool isAdv = false, isLoading = true, applyCharge = false;
  int datelimit = 0;
  Outlet? selOutlet;
  List offerList = [];
  Map? offer;

  showToast(String msg) {
    KeyConstant().showToast(msg, context);
  }

  getData() async {
    isLoading = true;
    isLogin = await KeyConstant.isUserLogin();
    _getList();
  }

  _getList() async {
    int cnt = await DBProvider().getCount();
    if (cnt > 0) {
      await DBProvider().getOrderDetail();
      selOutlet = widget.currentOutlet;
      // selOutlet = await getOutletDetail(Constant.dishorederlist[0].rest_id!,
      //     Constant.dishorederlist[0].brandid!);
      if (selOutlet!.success == 1) {
        service_charge = selOutlet!.deliveryCharges!;
        decimal_val = int.parse(selOutlet!.decimalValue!);
        currency = selOutlet!.currency!;
        packtype = selOutlet!.pack_charge_type!;
        packval = selOutlet!.packaging_charge!;
        Constant.decimal_value = decimal_val.toString();
        Constant.service_charge = service_charge;
        if (selOutlet!.advance_order != null &&
            selOutlet!.advance_order! == 'true') {
          isAdv = true;
          if (selOutlet!.adv_order_duration != null) {
            datelimit = selOutlet!.adv_order_duration!;
          }
        } else {
          isAdv = false;
        }
        if (selOutlet!.cod != null && selOutlet!.cod!.isNotEmpty) {
          if (selOutlet!.cod == 'unavailable') {
            Constant.allowCOD = false;
          } else {
            Constant.allowCOD = true;
          }
        }
      }
    }
    isLoading = false;
    _cal();
  }

  void applyCoupon() async {
    offerList = [];
    Map resOffer = await getOffers(
        _codeController.text, Constant.dishorederlist[0].brandid!);
    if (resOffer.containsKey('success')) {
      if (resOffer['success'] == 1) {
        offerList = resOffer['data'];
        if (offerList.isNotEmpty) {
          bool isApply = false;
          for (Map o in offerList) {
            if (double.parse(o['bill_amt'].toString()) <= grand) {
              isApply = true;
            }
          }
          if (!isApply) {
            CustomUI.customAlert(
                'Sorry',
                'Coupon apply on minimum bill ' +
                    offerList[0]['bill_amt'].toString(),
                context);
            offerList = [];
          }
        }
        _cal();
      } else if (resOffer['success'] == Constant.api_no_net) {
        showToast(Constant.api_no_net_msg);
      } else if (resOffer['success'] == Constant.api_catch_error) {
        showToast(Constant.api_catch_error_msg);
      } else if (resOffer.containsKey('message')) {
        CustomUI.customAlert('Sorry', resOffer['message'], context);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    d_date = '';
    ddt = '';
    dtm = '';
    d_time = '';
    delivery_type = widget.deliveryType;
    Future.delayed(Duration(milliseconds: 10), () {
      getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Text("Cart"),
          elevation: 0.0,
          actions: <Widget>[
            InkWell(
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  'Clear Cart   ',
                  textAlign: TextAlign.center,
                ),
              ),
              onTap: () {
                CustomUI.customAlertDialog(
                    "Are you sure?", "You want to clear cart?", () async {
                  await DBProvider().deleteAll();
                  if (mounted) {
                    setState(() {
                      Constant.dishorederlist.clear();
                      Constant.selidlist.clear();
                      Get.back();
                      _cal();
                    });
                  }
                }, () {
                  Get.back();
                }, context);
              },
            ),
          ]),
      body: (Constant.dishorederlist.isNotEmpty)
          ? Column(
              children: <Widget>[
                (selOutlet != null && selOutlet!.success == Constant.api_no_net)
                    ? Container(
                        color: Theme.of(context).colorScheme.secondary,
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              selOutlet!.message!,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor),
                            ),
                            InkWell(
                              onTap: () {
                                _getList();
                              },
                              child: const Text(
                                'Retry',
                                style: TextStyle(color: Colors.red),
                              ),
                            )
                          ],
                        ),
                      )
                    : const SizedBox(),
                Flexible(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                      child: ListView(
                        children: <Widget>[
                          Container(
                            margin:
                                const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                            padding: const EdgeInsets.all(10),
                            color: Theme.of(context).cardColor,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  Constant.dishorederlist[0].rest_name!,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                (selOutlet != null &&
                                        selOutlet!.phno != null &&
                                        selOutlet!.phno!.isNotEmpty)
                                    ? Row(
                                        children: <Widget>[
                                          Image.asset(
                                            'assets/ic_phone.png',
                                            height: 18,
                                          ),
                                          const SizedBox(
                                            width: 3.0,
                                          ),
                                          InkWell(
                                            child: Text(
                                              selOutlet!.phno!,
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: defaultClr),
                                            ),
                                            onTap: () {
                                              launch(
                                                  "tel://" + selOutlet!.phno!);
                                            },
                                          )
                                        ],
                                      )
                                    : const SizedBox(),
                              ],
                            ),
                          ),
                          _setItems(),
                          const SizedBox(
                            height: 10.0,
                          ),
                          SwiftoTxtField(
                              _noteController,
                              "Any Instruction?",
                              TextInputAction.done,
                              TextInputType.text,
                              true,
                              null,
                              null),
                          const SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: SwiftoTxtField(
                                    _codeController,
                                    "Coupon Code",
                                    TextInputAction.done,
                                    TextInputType.text,
                                    true,
                                    null,
                                    null),
                              ),
                              SwiftoBtn1("Apply", () {
                                if (_codeController.text.isEmpty) {
                                  showToast("Coupon code required");
                                } else {
                                  applyCoupon();
                                }
                              })
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.all(10),
                            color: Theme.of(context).cardColor,
                            child: Column(
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text("Delivery Type"),
                                    Text((delivery_type ==
                                            KeyConstant.type_delivery)
                                        ? KeyConstant.type_delivery_txt
                                        : KeyConstant.type_takeaway_txt)
                                  ],
                                ),
                                const SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text("Sub Total"),
                                    Text(currency +
                                        subtot.toStringAsFixed(decimal_val))
                                  ],
                                ),
                                _setTax(),
                                const SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text("Packaging Charge"),
                                    (packval > 0)
                                        ? Text(currency +
                                            double.parse(packCharge!)
                                                .toStringAsFixed(decimal_val))
                                        : Text(currency + "0.00")
                                  ],
                                ),
                                const SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text("Delivery Charge"),
                                    (applyCharge)
                                        ? Text(currency +
                                            double.parse(service_charge)
                                                .toStringAsFixed(decimal_val))
                                        : Text(currency + "0.00")
                                  ],
                                ),
                                (selOutlet != null &&
                                        selOutlet!
                                                .minimum_bill_for_delivery_charge !=
                                            0 &&
                                        applyCharge)
                                    ? Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                            '*Apply Delivery charge if bill amount less than ' +
                                                currency +
                                                selOutlet!
                                                    .minimum_bill_for_delivery_charge
                                                    .toString(),
                                            textAlign: TextAlign.start,
                                            style: const TextStyle(
                                                color: Colors.red,
                                                fontSize: 12)),
                                      )
                                    : const SizedBox(),
                                const SizedBox(
                                  height: 7.0,
                                ),
                                (dis_amt > 0)
                                    ? Container(
                                        margin:
                                            const EdgeInsets.only(bottom: 7.0),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text("Discount\n(" +
                                                offer!['name'].toString() +
                                                ")"),
                                            Text(Constant.currency +
                                                dis_amt.toStringAsFixed(
                                                    decimal_val))
                                          ],
                                        ),
                                      )
                                    : const SizedBox(),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text("Bill Amount"),
                                    Text(currency +
                                        grand.toStringAsFixed(decimal_val))
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    flex: 1),
                (!isLoading)
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.fromLTRB(10.0, 25.0, 10.0, 25.0),
                        color: Theme.of(context).primaryColor,
                        child: (isLogin)
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  const Text(
                                    'Grand Total',
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    currency +
                                        grand.toStringAsFixed(decimal_val),
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  if (delivery_type ==
                                          KeyConstant.type_delivery &&
                                      grand < selOutlet!.minimum_total_bill!)
                                    Container(
                                      margin: const EdgeInsets.only(top: 3),
                                      child: Text(
                                          '*Minimum bill amount ' +
                                              currency +
                                              selOutlet!.minimum_total_bill!
                                                  .toString() +
                                              ' required for delivery.',
                                          style: const TextStyle(
                                              color: Colors.red, fontSize: 12)),
                                    )
                                ],
                              )
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Almost There',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  ),
                                  Text(
                                    'Login or signup to place your order',
                                    style: TextStyle(fontSize: 13),
                                  ),
                                ],
                              ),
                      )
                    : const SizedBox()
              ],
            )
          : (!isLoading)
              ? Center(child: EmptyCart())
              : const SizedBox(),
      floatingActionButton: (!isLogin &&
              selOutlet != null &&
              selOutlet!.success == 1 &&
              Constant.dishorederlist.isNotEmpty)
          ? FloatingActionButton(
              tooltip: "Login",
              backgroundColor: defaultClr,
              onPressed: () async{
                // Stripe.publishableKey = 'pk_test_fyXUDQ7Xmu9UtWH6ihGniIqc00L8ZM1FBr';
                // Stripe.merchantIdentifier = 'merchant.flutter.stripe.test';
                // Stripe.urlScheme = 'flutterstripe';
                // await Stripe.instance.applySettings();
                // showDialog(context: context, builder: (BuildContext context){
                //   return StripeScreen();
                // });
                Get.toNamed(RouteHelper.getSignInRoute());
              },
              child: Image.asset('assets/ic_login.png',
                  width: 20, color: BTNTXTCLR),
              heroTag: Object(),
            )
          : (isLogin &&
                  Constant.dishorederlist.isNotEmpty &&
                  ((selOutlet != null &&
                          delivery_type == KeyConstant.type_delivery &&
                          grand > selOutlet!.minimum_total_bill!) ||
                      delivery_type == KeyConstant.type_takeaway))
              ? FloatingActionButton(
                  tooltip: "Checkout",
                  backgroundColor: defaultClr,
                  onPressed: () {
                    checkOutClick();
                  },
                  child: Image.asset('assets/ic_down_right.png',
                      width: 20, color: BTNTXTCLR),
                  heroTag: Object(),
                )
              : const SizedBox(),
    );
  }

  _setDeliveryType(String title, String desc, String img, String val) {
    return Container(
      child: InkWell(
        child: Row(
          children: <Widget>[
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                image:
                    DecorationImage(image: AssetImage(img), fit: BoxFit.fill),
              ),
            ),
            const SizedBox(width: 15),
            Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    //    style:Theme.of(context).textTheme.subhead
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  Text(
                    desc,
                  ),
                ],
              ),
            ),
            Image.asset('assets/ic_next.png',height: 18,),
          ],
        ),
        onTap: () {
          if (delivery_type != val) {
            delivery_type = val;
          } else {
            delivery_type = '';
          }
          _cal();
        },
      ),
    );
  }

  _setItems() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: Constant.dishorederlist.length,
      itemBuilder: (BuildContext context, int index) {
        DishOrderPodo podo = Constant.dishorederlist[index];
        return CartItems(
          currency: currency,
          data: podo,
          isEditable: true,
          clickInc: () async {
            DishOrderPodo d = Constant.dishorederlist[index];
            int q = int.parse(d.qty.toString()) + 1;
            d.qty = q.toString();
            await DBProvider().updateQty(index, d.qty!, d.orderdetailid!);
            Constant.dishorederlist[index] = d;
            _cal();
          },
          clickDec: () async {
            DishOrderPodo d = Constant.dishorederlist[index];
            int q = int.parse(d.qty.toString()) - 1;
            if (q != 0) {
              d.qty = q.toString();
              await DBProvider().updateQty(index, d.qty!, d.orderdetailid!);
              Constant.dishorederlist[index] = d;
            } else {
              _removeItemAlert(d, index);
            }
            _cal();
          },
          clickRemove: () {
            DishOrderPodo d = Constant.dishorederlist[index];
            _removeItemAlert(d, index);
          },
          clickUpdate: () {
            if (podo.preflag != null &&
                podo.preflag!.isNotEmpty &&
                podo.preflag == 'true') {
              _getItemDetail(podo.dishid!, podo.brandid!, podo.rest_id!,
                  podo.rest_uniq_id!, index);
            }
          },
        );
      },
    );
  }

  _getItemDetail(
      String id, String brandid, String restid, String runiqid, int pos) async {
    Map data = await getItemDetail(restid, runiqid, id, brandid);
    if (data.containsKey("success") && data['success'] == 101) {
      CustomUI.customAlert('', "No Internet Connection", context);
    } else if (selOutlet!.success == 1) {
      dynamic d_result = await showDialog(
        context: context,
        builder: (_) => DialogVariation(
          item: data,
          currentOutlet: selOutlet!,
          isUpdate: true,
          updatePOS: pos,
        ),
      );

      if (d_result != null && d_result is bool) {
        if (d_result) Provider.of<AppModel>(context, listen: false).increment();
        if (mounted) {
          setState(() {});
        }
      }
    }
  }

  _removeItemAlert(DishOrderPodo d, int index) {
    CustomUI.customAlertDialog(
        "Remove Item?", "You want to remove " + d.dishname! + " from cart?",
        () async {
      await DBProvider().removeItemById(index, d.orderdetailid!);
      Constant.dishorederlist.removeAt(index);
      Constant.selidlist.removeAt(index);
      Get.back();
      _cal();
    }, () {
      Get.back();
    }, context);
  }

  _setTax() {
    if (taxInvoiceList == null) taxInvoiceList = [];
    taxInvoiceList!.clear();
    if (taxidlist == null) taxidlist = [];
    taxidlist!.clear();
    if (Constant.dishorederlist.isNotEmpty) {
      int dpos = 0;
      for (DishOrderPodo pojo in Constant.dishorederlist) {
        double tax_tot = 0;
        List? tlist = pojo.tax_data;
        tax_tot = double.parse(pojo.tot_tax!);
        int qty = int.parse(pojo.qty!);
        String? tt = pojo.tax_amt;
        if (tt == null || tt.trim().isEmpty) tt = "0";
        double diff = qty * (double.parse(tt));
        if (tlist != null && tlist.isNotEmpty) {
          int tpos = 0;
          for (Map tax in tlist) {
            TaxInvoicePodo tp = TaxInvoicePodo();
            double d = (diff * double.parse(tax['value'].toString())) / tax_tot;
            if (taxidlist!.contains(tax['id'].toString())) {
              int p = taxidlist!.indexOf(tax['id'].toString());
              tp = taxInvoiceList![p];
              double pt = double.parse(tp.per_total!) +
                  double.parse(tax['value'].toString());
              tp.per_total = pt.toString();
              double tdiff = double.parse(tp.amount_total!) + d;
              tax['tax_value'] = d;
              tp.amount_total = tdiff.toString();
              taxInvoiceList![p] = tp;
            } else {
              tp.id = tax['id'].toString();
              tp.name = tax['text'];
              tp.type = tax['tax_amount'].toString();
              tp.per = tax['value'].toString();
              tp.per_total = tax['value'].toString();
              tax['tax_value'] = d.toString();
              tp.amount_total = d.toString();
              taxInvoiceList!.add(tp);
              taxidlist!.add(tax['id'].toString());
            }
            tlist[tpos] = tax;
            tpos++;
          }
          DishOrderPodo dishOrderPojo = Constant.dishorederlist[dpos];
          dishOrderPojo.tax_data = tlist;
          Constant.dishorederlist[dpos] = dishOrderPojo;
        }
        dpos++;
      }

      if (taxInvoiceList != null && taxInvoiceList!.isNotEmpty) {
        return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: false,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: taxInvoiceList == null ? 0 : taxInvoiceList!.length,
            itemBuilder: (BuildContext context, int index) {
              TaxInvoicePodo p = taxInvoiceList![index];
              return Column(
                children: <Widget>[
                  const SizedBox(
                    height: 7.0,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(p.name! +
                          "(" +
                          double.parse(p.per!).toStringAsFixed(decimal_val) +
                          "%)"),
                      Text(currency +
                          double.parse(p.amount_total!)
                              .toStringAsFixed(decimal_val))
                    ],
                  )
                ],
              );
            });
      } else {
        return const SizedBox();
      }
    }
  }

  _cal() {
    Provider.of<AppModel>(context, listen: false).increment();
    rounding_id = "";
    interval_val = "";
    applyCharge = false;
    double s_tot = 0.0, g_tot = 0.0;
    int tot_qty = 0;
    dis_amt = 0.0;
    dis_per = 0.0;
    offer = null;
    if (Constant.dishorederlist != null && Constant.dishorederlist.isNotEmpty) {
      s_tot = Constant.dishorederlist
          .map<double>(
              (m) => int.parse(m.qty!) * double.parse(m.price_without_tax!))
          .reduce((a, b) => a + b);
      g_tot = Constant.dishorederlist
          .map<double>((m) => int.parse(m.qty!) * double.parse(m.price!))
          .reduce((a, b) => a + b);
      tot_qty = Constant.dishorederlist
          .map<int>((m) => int.parse(m.qty!))
          .reduce((a, b) => a + b);

      if (service_charge != null &&
          service_charge.trim().isNotEmpty &&
          delivery_type == KeyConstant.type_delivery) {
        if (selOutlet != null &&
            selOutlet!.minimum_bill_for_delivery_charge! > g_tot) {
          double sc = double.parse(service_charge);
          g_tot = g_tot + sc;
          applyCharge = true;
        }
      }

      if (packval > 0) {
        if (packtype == 'perorder') {
          g_tot = g_tot + packval;
          packCharge = packval.toString();
        } else if (packtype == 'peritem') {
          double pc = tot_qty * packval;
          g_tot = g_tot + pc;
          packCharge = pc.toString();
        }
      }

      for (Map o in offerList) {
        if (double.parse(o['bill_amt'].toString()) <= g_tot) {
          if (o['discount_type'].toString() == 'percentage') {
            offer = o;
            dis_per = double.parse(o['discount_percentage'].toString());
            dis_amt = (dis_per * g_tot) / 100;
          } else if (o['discount_type'] == 'amount') {
            offer = o;
            dis_amt = double.parse(o['discount_amt'].toString());
            dis_per = (dis_amt * 100) / g_tot;
          }
        }
      }

      subtot = s_tot;
      grand = g_tot;
      //_applyRoundFormat(g_tot);
    } else {
      subtot = 0.0;
      grand = 0.0;
      rounding = 0.0;
    }
    // if (delivery_type!.isNotEmpty) {
    //   checkOutClick();
    // } else {
    if (mounted) {
      setState(() {});
    }
    //}
  }

  checkOutClick() async {
    if (selOutlet!.outletCurrentStatus != null &&
        selOutlet!.outletCurrentStatus == 'close') {
      delivery_type = '';
      CustomUI.customAlert('Sorry, Restaurant is closed now.', '', context);
    } else {
      if (delivery_type == KeyConstant.type_takeaway && !isAdv) {
        String sel = await showDialog(
            context: context,
            builder: (context) {
              return TimeSlotDialog(
                parentScaffoldKey: _scaffoldKey,
              );
            });

        if (sel != null) {
          _redirectToConfirmScreen(grand, 0, 0, null, sel);
        } else {
          delivery_type = '';
        }
      } else {
        _redirectToConfirmScreen(grand, 0, 0, null, '');
      }
    }
  }

  Map _applyRoundFormat(double bill_amt) {
    String roundJSON = "";
    double p_amt = bill_amt, rounding;
    String rounding_id;
    try {
      double val_interval = 0; // 1, 0.50,0.05
      int rule_tag = 0; // 1,2,3,4
      if (selOutlet != null &&
          selOutlet!.intervalNumber != null &&
          selOutlet!.roundingTag != null) {
        Constant.showLog(selOutlet!.roundingTag.toString() +
            " " +
            selOutlet!.intervalNumber.toString() +
            " " +
            selOutlet!.roundingId.toString());
        String? rtag = (selOutlet!.roundingTag == null)
            ? ''
            : selOutlet!.roundingTag.toString();
        String? rval = selOutlet!.intervalNumber!;
        if (rtag != null && rtag.trim().isNotEmpty) rule_tag = int.parse(rtag);
        if (rval != null && rval.trim().isNotEmpty) {
          val_interval = double.parse(rval);
        }
        rounding_id = selOutlet!.roundingId!;
        interval_val = rval;

        double result = bill_amt;
        String txtbillamt = bill_amt.toString();
        if (!txtbillamt.contains('.'))
          txtbillamt = bill_amt.toStringAsFixed(decimal_val);
        String onlyint = txtbillamt.substring(0, txtbillamt.indexOf("."));
        double onlydecimal =
            double.parse("0" + txtbillamt.substring(txtbillamt.indexOf(".")));
        if (onlydecimal == 0) {
          result = bill_amt;
        } else if (decimal_val == 3) {
          result = bill_amt;
        } else if (val_interval == 1) {
          if (rule_tag == 1) {
            result = double.parse(bill_amt.round().toString());
          } else if (rule_tag == 3) {
            result = double.parse(bill_amt.ceil().toString());
          } else if (rule_tag == 4) {
            result = double.parse(bill_amt.floor().toString());
          } else if (rule_tag == 2) {
            if (onlydecimal <= 0.50) {
              result = double.parse(bill_amt.floor().toString());
            } else {
              result = double.parse(bill_amt.ceil().toString());
            }
          }
        } else if (val_interval == 0.50) {
          if (rule_tag == 4) {
            if (onlydecimal > 0 && onlydecimal < 0.50) {
              result = double.parse(onlyint);
            } else {
              result = double.parse(onlyint) + 0.50;
            }
          } else if (rule_tag == 3) {
            if (onlydecimal > 0 && onlydecimal <= 0.50) {
              result = double.parse(onlyint) + 0.50;
            } else {
              result = double.parse(onlyint) + 1;
            }
          } else if (rule_tag == 2) {
            if (onlydecimal > 0 && onlydecimal <= 0.25) {
              result = double.parse(onlyint);
            } else if (onlydecimal >= 0.26 && onlydecimal <= 0.75) {
              result = double.parse(onlyint) + 0.50;
            } else {
              result = double.parse(onlyint) + 1;
            }
          } else if (rule_tag == 1) {
            if (onlydecimal > 0 && onlydecimal <= 0.24) {
              result = double.parse(onlyint);
            } else if (onlydecimal >= 0.25 && onlydecimal <= 0.74) {
              result = double.parse(onlyint) + 0.50;
            } else {
              result = double.parse(onlyint) + 1;
            }
          }
        } else if (val_interval == 0.05) {
          String tofirstDecimal =
              txtbillamt.substring(0, txtbillamt.indexOf(".") + 2);
          double fd = double.parse(tofirstDecimal);
          double lastDecimal = double.parse(
              txtbillamt.replaceRange(0, txtbillamt.indexOf(".") + 2, "0.0"));
          if (lastDecimal == 0) {
            result = fd;
          } else if (lastDecimal == 0.05) {
            result = bill_amt;
          } else if (rule_tag == 1 || rule_tag == 2) {
            if (lastDecimal > 0 && lastDecimal < 0.03) {
              result = fd;
            } else if (lastDecimal >= 0.03 && lastDecimal <= 0.07) {
              result = fd + 0.05;
            } else if (lastDecimal >= 0.08 && lastDecimal <= 0.10) {
              result = fd + 0.10;
            }
          } else if (rule_tag == 3) {
            if (lastDecimal > 0 && lastDecimal <= 0.05) {
              result = fd + 0.05;
            } else {
              result = fd + 0.10;
            }
          } else if (rule_tag == 4) {
            if (lastDecimal > 0 && lastDecimal < 0.05) {
              result = fd;
            } else {
              result = fd + 0.05;
            }
          }
        }
        p_amt = result;
        rounding = p_amt - bill_amt;
        Map roundingJson = {
          "rounding_id": rounding_id,
          "cash_rounding": rounding,
          "interval_val": interval_val,
          "original_total": bill_amt
        };
        roundJSON = json.encode(roundingJson);
      }
    } on FormatException catch (e) {
      Constant.showLog("applyRoundFormat error:" + e.toString());
    } catch (e) {
      Constant.showLog("applyRoundFormat error11:" + e.toString());
    }
    Map res = {'round': roundJSON, 'pay_amt': p_amt};
    return res;
  }

  _redirectToConfirmScreen(double payamt, double r_point, double r_point_amt,
      String? per_point, String pickuptm) async {
    Map r = _applyRoundFormat(payamt);
    String roundJSON = r['round'], pointJson = "";
    if (r_point > 0) {
      Map pJson = {
        "redeem_points": r_point,
        "redeem_amount": r_point_amt.toStringAsFixed(decimal_val),
        "points_per_one_currency": per_point
      };
      pointJson = json.encode(pJson);
    }
    List ts = [];
    if (taxInvoiceList != null && taxInvoiceList!.isNotEmpty) {
      taxInvoiceList!.forEach((e) => ts.add(e.toJson()));
    }
    Map cartJSON = {
      'subtot': subtot.toStringAsFixed(decimal_val),
      'grand': grand.toStringAsFixed(decimal_val),
      'service_charge': (applyCharge) ? service_charge : '0.0',
      'packaging_charge': (packval > 0) ? packCharge : '0.0',
      'taxInvoiceList': ts, //taxInvoiceList,
      'points': 0.toStringAsFixed(decimal_val),
      'note': _noteController.text,
      'delivery_type': delivery_type,
      'roundingJSON': roundJSON,
      'pointJSON': pointJson,
      'pay': r['pay_amt'].toString(),
      'is_advanceorder': is_advance_order,
      'pick_dt': (is_advance_order == 'yes') ? ddt + " " + dtm : pickuptm,
      'currency': currency,
      'restNote': selOutlet!.note!,
      'dis_per': dis_per,
      'dis_amt': dis_amt,
      'offer': offer
    };
    try {
      if (delivery_type == KeyConstant.type_delivery) {
        Get.toNamed(RouteHelper.getSelectAddressRoute({
          'isChangeLocation': false,
          'outletData': selOutlet!.toJson(),
          'cartData': cartJSON
        }));
      } else {
        Get.toNamed(RouteHelper.getCheckoutRoute(
            {'outletData': selOutlet!.toJson(), 'cartData': cartJSON}));
      }
    } catch (e) {
      Constant.showLog('error');
      Constant.showLog(e);
    }
  }

  _createDateTime() {
    dt_dlist = [];
    dt_dlist!.clear();
    dtlist = [];
    dtlist!.clear();
    for (int i = 0; i <= datelimit; i++) {
      DateTime dt = DateTime.now();
      dt = dt.add(Duration(days: i));
      dt_dlist!.add(DateFormat("dd MMM yyyy").format(dt));
      dtlist!.add(DateFormat("yyyy-MM-dd").format(dt));
    }
    tm_dlist = [];
    tm_dlist!.clear();
    tmlist = [];
    tmlist!.clear();
    DateTime _selDateTime = DateTime.now();
    String stm = selOutlet!.outletOpeningTiming!;
    String etm = selOutlet!.outletClosingTiming!;
    DateTime sdt = DateTime(
        _selDateTime.year,
        _selDateTime.month,
        _selDateTime.day,
        int.parse(stm.split(":")[0].toString()),
        int.parse(stm.split(":")[1].toString()));
    DateTime edt = DateTime(
        _selDateTime.year,
        _selDateTime.month,
        _selDateTime.day,
        int.parse(etm.split(":")[0].toString()),
        int.parse(etm.split(":")[1].toString()));
    final dur = edt.difference(sdt);
    int m = (dur.inMinutes / 15).floor();
    tm_dlist!.add(DateFormat("hh:mm aaa").format(sdt));
    tmlist!.add(DateFormat("HH:mm:ss").format(sdt));
    DateTime dtm = sdt;
    for (int i = 0; i < m; i++) {
      dtm = dtm.add(const Duration(minutes: 15));
      tm_dlist!.add(DateFormat("hh:mm aaa").format(dtm));
      tmlist!.add(DateFormat("HH:mm:ss").format(dtm));
    }
    if (dtlist != null && dtlist!.isNotEmpty) {
      _settingModalBottomSheet();
    }
  }

  _settingModalBottomSheet() {
    if (d_date == null || d_date.isEmpty) {
      ddt = dtlist![0];
      d_date = dt_dlist![0];
    }
    showModalBottomSheet(
        context: context,
        builder: (_) => TimingList(
              dtlist1: dtlist!,
              dt_dlist1: dt_dlist!,
              tmlist1: tmlist!,
              tm_dlist1: tm_dlist!,
            )).whenComplete(_refreshLayout());
  }

  _refreshLayout() {
    if (mounted) {
      setState(() {});
    }
  }
}

class TimingList extends StatefulWidget {
  List<String> dtlist1, dt_dlist1, tmlist1, tm_dlist1;

  TimingList({
    Key? key,
    required this.dtlist1,
    required this.dt_dlist1,
    required this.tmlist1,
    required this.tm_dlist1,
  }) : super(key: key);

  @override
  _TimingListState createState() => _TimingListState();
}

class _TimingListState extends State<TimingList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      height: MediaQuery.of(context).size.height / 2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Select Delivery Date/Time',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
          ),
          Expanded(
            flex: 1,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: ListView.builder(
                      itemCount: widget.dt_dlist1.length,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            if (ddt != widget.dtlist1[index]) {
                              dtm = '';
                              d_time = '';
                              ddt = widget.dtlist1[index];
                              d_date = widget.dt_dlist1[index];
                            }
                            if (mounted) {
                              setState(() {});
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10, bottom: 5),
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  'assets/ic_success.png',
                                  width: 18,
                                  color: (d_date == widget.dt_dlist1[index])
                                      ? Colors.green
                                      : Theme.of(context).primaryColor,
                                ),
                                const SizedBox(
                                  width: 3,
                                ),
                                Text(
                                  widget.dt_dlist1[index],
                                  style: const TextStyle(fontSize: 16),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                ),
                Expanded(
                  flex: 1,
                  child: (d_date != null && d_date.trim().isNotEmpty)
                      ? ListView.builder(
                          itemCount: widget.tm_dlist1.length,
                          itemBuilder: (BuildContext context, int index) {
                            DateTime sdt = DateTime.now();
                            String stm = widget.tmlist1[index];
                            if (ddt != null && ddt.isNotEmpty) {
                              sdt = DateTime(
                                  int.parse(ddt.split("-")[0].toString()),
                                  int.parse(ddt.split("-")[1].toString()),
                                  int.parse(ddt.split("-")[2].toString()),
                                  int.parse(stm.split(":")[0].toString()),
                                  int.parse(stm.split(":")[1].toString()));
                            }
                            return (sdt.isAfter(DateTime.now()))
                                ? InkWell(
                                    onTap: () {
                                      if (mounted) {
                                        setState(() {
                                          if (d_time !=
                                              widget.tm_dlist1[index]) {
                                            d_time = widget.tm_dlist1[index];
                                            dtm = widget.tmlist1[index];
                                            Get.back();
                                          }
                                        });
                                      }
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 3, bottom: 5),
                                      child: Row(
                                        children: <Widget>[
                                          Image.asset(
                                            'assets/ic_success.png',
                                            width: 18,
                                            color: (d_time ==
                                                    widget.tm_dlist1[index])
                                                ? Colors.green
                                                : Theme.of(context)
                                                    .primaryColor,
                                          ),
                                          const SizedBox(
                                            width: 3,
                                          ),
                                          Text(
                                            widget.tm_dlist1[index],
                                            style:
                                                const TextStyle(fontSize: 16),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                : const SizedBox();
                          })
                      : const SizedBox(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
