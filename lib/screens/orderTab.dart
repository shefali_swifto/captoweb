import 'package:capto/screens/orderListScreen.dart';
import 'package:capto/util/firebaseHelper.dart';
import 'package:flutter/material.dart';
import 'package:capto/util/styles.dart';

class OrderTab extends StatefulWidget {
  FirebaseHelper parentObj;

  OrderTab({Key? key, required this.parentObj}) : super(key: key);

  @override
  _OrderTabState createState() => _OrderTabState();
}

class _OrderTabState extends State<OrderTab> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: const Text("Orders"),
            ),
            body: Column(
              children: [
                SizedBox(
                  height: 50,
                  child: TabBar(
                    labelColor: mainClr,
                    unselectedLabelColor:Theme.of(context).textTheme.bodySmall!.color,
                    indicatorColor: mainClr,
                    tabs: const [
                      Tab(
                        text: 'In process',
                      ),
                      Tab(
                        text: 'Past Orders',
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: TabBarView(
                    children: [
                      OrderList(
                        TAG: 'current',
                        parentObj: widget.parentObj,
                      ),
                      OrderList(
                        TAG: 'past',
                        parentObj: widget.parentObj,
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}